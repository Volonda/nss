<?php
namespace Nss\Bundle\ModerationBundle\Manager;

use Doctrine\ORM\EntityManager;

use ITSymfony\Bundle\BaseCollectionBundle\Formatter\CollectionFormatter;
use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrderBundle\Entity\View\OrderWorkflowActionView;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\UserBundle\Entity\Action;


class OrderManager
{
    /** @var EntityManager */
    private $em;

    /** @var array*/
    private $listFilters = [
        'id' => 'int',
        'c.fio' => 'like',
        'org.inn' => 'like',
        's.id' =>  [
            'options' => [ ],
            'type' => 'eq'
        ],
        'ot.id' => [
            'options' => [ ],
            'type' => 'eq'
        ],
        'o.createdAt' => 'date',
        'os.id' => [
            'options' => [],
            'type' => 'eq'
        ],
        'c.phone' => 'like',
        'c.email' => 'like',
    ];

    /** @var Paginator*/
    private $paginator;

    /** @var CollectionFormatter*/
    public $collectionFormatter;

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
     * @param CollectionFormatter $collectionFormatter
    */
    public function __construct(
        EntityManager $em,
        Paginator $paginator,
        CollectionFormatter $collectionFormatter 
    ) {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->collectionFormatter = $collectionFormatter;
    }

    /**
     * @return array
    */
    public function getFilters() : array
    {
        $filters = $this->listFilters;

        $filters['ot.id']['options'] = $this->collectionFormatter->toAssocArray(BaseOrderType::class,'id','name');
        $filters['s.id']['options'] = $this->collectionFormatter->toAssocArray(Sro::class,'id','name');
        $filters['os.id']['options'] = $this->collectionFormatter->toAssocArray(BaseOrderStatus::class,'id','name');

        return $filters;
    }


    /**
     * @param int $page
     * @param int $limit
     * @param Filter $filter
     * @return PaginationInterface
     */
    public function getPaginate($page, $limit, Filter $filter) : PaginationInterface
    {
        $action = $this->em->getRepository(Action::class)->findOneBy(['code' => 'moderation_view_all']);

        $subQb = $this->em->createQueryBuilder()
            ->select('owa')
            ->from(OrderWorkflowActionView::class,'owa')
            ->join('owa.actions','a')
            ->where('owa.orders = o')
            ->andWhere('a =:action')
        ;

        $qb = $this->em->createQueryBuilder();
        $qb->select('o')
            ->from(Order::class,'o')
            ->join('o.type','ot')
            ->join('o.sro','s')
            ->join('o.status','os')
            ->join('o.contact','c')
            ->join('c.organization','org')
            ->where($qb->expr()->exists($subQb))
            ->setParameter('action', $action)
        ;

        $filter->addFiltersToQB($qb);

        return $this->paginator->paginate($qb, $page, $limit,
            ['defaultSortFieldName' => 'o.id', 'defaultSortDirection' => 'ASC']
        );
    }
}