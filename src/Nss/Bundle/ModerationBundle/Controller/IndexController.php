<?php

namespace Nss\Bundle\ModerationBundle\Controller;

use CheckerBundle\Factory\CheckerFactory;
use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use Nss\Bundle\ContactBundle\Manager\ContactManager;
use Nss\Bundle\ModerationBundle\Manager\OrderManager;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrderBundle\Form\Type\OrderType;
use Nss\Bundle\WorkflowBundle\Checker\WorkflowChecker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class IndexController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/", name="moderation")
     * @Method("GET")
     * @Template()
     * @Security("is_granted('allow', 'action:moderation_view_all')")
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Заявления','moderation');

        /** @var OrderManager $manager*/
        $manager = $this->get('moderation.manager.order_manager');

        /* @var $filter Filter */
        $filter = $this->get('it_symfony.data_table.filter')->getFilter($request, $manager->getFilters());

        $pagination = $manager->getPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @Route("/view/{id}", requirements={"id": "\d+"}, name="moderation_view")
     * @Method("GET")
     * @Security("is_granted('allow', 'action:moderation_view_all')")
     * @Template()
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @return array
     */
    public function viewAction(Order $order) : array
    {
        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if (!$checker->isWorkflowActionAllowed($order, 'moderation_view_all'))
        {
            throw new NotFoundHttpException();
        }

        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Завяления', 'order_my');
        $breadcrumbs->addItem('#' . $order->getId());

        return [
            'order' => $order
        ];
    }

    /**
     * @Route("/edit/{id}", name="moderation_edit",
     *     requirements={"id": "\d+"})
     * @Template()
     * @Security("is_granted('allow', 'action:moderation')")
     * @ParamConverter("order", class="Nss\Bundle\OrderBundle\Entity\Order")
     * @param Request $request
     * @param Order $order
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Order $order)
    {
        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if (!$checker->isWorkflowActionAllowed($order, 'moderation'))
        {
            throw new NotFoundHttpException();
        }

        $orderModel = $order->getOrderModel();

        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Завяления','order_my');
        $breadcrumbs->addRouteItem( '#' . $order->getId(), 'order_view', [
            'id' => $order->getId()
        ]);
        $breadcrumbs->addItem('Редактирование');

        /** @var \Nss\Bundle\OrderBundle\Manager\OrderManager $orderManager*/
        $orderManager = $this->get('order.manager.order_manager');

        /** @var ContactManager $contactManager*/
        //$contactManager = $this->get('contact.manager.contact_manager');

        $form = $this->createForm(OrderType::class, null, [
            'serviceModels' => $orderModel->getActualServices(),
            'services' => $order->getServices(),
            'contact' => $order->getContact(),
            'user' => $order->getOwner()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->get('contact')->getData();

            if (!$contact){

                throw new NotFoundHttpException("Contact undefined");

                /*
                $contact = $form->get('orderContact')->getData();
                $contactManager->create($contact, $this->getUser(),  $order->getContact()->getOrganization());
                */
            }

            $serviceModel = $form->get('services')->getData();

            $orderManager->update($order, $serviceModel, $this->getUser());

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('notice','EDIT_SUCCESS');

            if ($form->get('submit_and_document')->isClicked()){
                return $this->redirectToRoute('moderation_document_output_edit', [
                    'id' => $order->getId()
                ]);
            } else {
                return $this->redirectToRoute('moderation_view', [
                    'id' => $order->getId()
                ]);
            }
        }

        return [
            'form' => $form->createView(),
            'organization' =>  $order->getContact()->getOrganization(),
            'sro' =>  $order->getSro(),
            'order' => $order
        ];
    }
}
