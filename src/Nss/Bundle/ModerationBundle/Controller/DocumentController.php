<?php

namespace Nss\Bundle\ModerationBundle\Controller;

use CheckerBundle\Factory\CheckerFactory;
use Nss\Bundle\DocumentBundle\Form\Type\BundleType;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\WorkflowBundle\Checker\WorkflowChecker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;


/**
 * @Route("/documents")
*/
class DocumentController extends Controller
{
    /**
     * @Route("/output/edit/{id}", requirements={"id": "\d+"}, name="moderation_document_output_edit")
     * @Method("GET")
     * @Template("@NssModeration/Document/edit.html.twig")
     * @Security("is_granted('allow', 'action:moderation')")
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function outputEditAction(Order $order)
    {
        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if (!$checker->isActionAllowed($order, 'moderation'))
        {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(BundleType::class,null,[
            'documentBundle' => $order->getOutputDocumentBundle(),
            'order' => $order,
            'type' => Order::DOCUMENT_BUNDLE_OUTPUT
        ]);

        return [
            'order' => $order,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/inbox/view/{id}", requirements={"id": "\d+"}, name="moderation_document_input_view")
     * @Security("is_granted('allow', 'action:moderation_view_all')")
     * @Template("@NssModeration/Document/view.html.twig")
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function inputViewAction(Order $order)
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Заявки','order_my');
        $breadcrumbs->addRouteItem('Заявка #' . $order->getId(), 'order_view', ['id' => $order->getId()]);
        $breadcrumbs->addItem('Документы');

        return [
            'order' => $order,
            'documents' => ($order->getInputDocumentBundle())?$order->getInputDocumentBundle()->getDocuments():[],
            'title'=> 'Исходящие документы',
            'type' => Order::DOCUMENT_BUNDLE_INPUT
        ];
    }

    /**
     * @Route("/outbox/view/{id}", requirements={"id": "\d+"}, name="moderation_document_output_view")
     * @Security("is_granted('allow', 'action:moderation_view_all')")
     * @Template("@NssModeration/Document/view.html.twig")
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function outputViewAction(Order $order)
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Заявки','order_my');
        $breadcrumbs->addRouteItem('Заявка #' . $order->getId(), 'order_view', ['id' => $order->getId()]);
        $breadcrumbs->addItem('Документы');

        return [
            'order' => $order,
            'documents' => ($order->getOutputDocumentBundle())?$order->getOutputDocumentBundle()->getDocuments():[],
            'title'=> 'Входящие документы',
            'type' => Order::DOCUMENT_BUNDLE_OUTPUT
        ];
    }
}
