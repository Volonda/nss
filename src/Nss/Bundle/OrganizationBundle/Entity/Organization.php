<?php
namespace Nss\Bundle\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\BaseCollectionBundle\Entity\BaseOrganizationType;

/**
 * @ORM\Entity(repositoryClass="Nss\Bundle\OrganizationBundle\Repository\OrganizationRepository")
 * @ORM\Table(name="app_organizations", schema="public")
 */
class Organization
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="text", nullable=false)
     */
    private $inn;

    /**
     * @var string|null
     * @ORM\Column(name="short_name", type="text", nullable=true)
     */
    private $shortName;

    /**
     * @var string|null
     * @ORM\Column(name="full_name", type="text", nullable=true)
     */
    private $fullName;

    /**
     * @var BaseOrganizationType
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\BaseCollectionBundle\Entity\BaseOrganizationType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     */
    private $type;

    /**
     * @var object
     * @ORM\Column(name="seldon_data", type="jsonb", nullable=true)
     */
    private $seldonData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $seldonDataUpdatedAt;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getInn(): ?string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return null|string
     */
    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    /**
     * @param null|string $shortName
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @return null|string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param null|string $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return BaseOrganizationType
     */
    public function getType() : BaseOrganizationType
    {
        return $this->type;
    }

    /**
     * @param BaseOrganizationType $type
     */
    public function setType(BaseOrganizationType $type)
    {
        $this->type = $type;
    }

    /**
     * @return object
     */
    public function getSeldonData()
    {
        return $this->seldonData;
    }

    /**
     * @param object $seldonData
     */
    public function setSeldonData($seldonData)
    {
        $this->seldonData = $seldonData;
    }

    /**
     * @return \DateTime
     */
    public function getSeldonDataUpdatedAt(): \DateTime
    {
        return $this->seldonDataUpdatedAt;
    }

    /**
     * @Gedmo\Timestampable(on="update", field="seldonDataUpdatedAt")
     */
    public function setSeldonDataUpdatedAt()
    {
        $this->seldonDataUpdatedAt = new \DateTime('now');
    }
}