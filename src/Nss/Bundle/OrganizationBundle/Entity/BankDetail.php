<?php
namespace Nss\Bundle\OrganizationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="bank_details", schema="nss_user")
 */
class BankDetail
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="bic", type="text", nullable=false)
     * @Assert\NotNull()
     */
    private $bic;

    /**
     * @var string
     *
     * @ORM\Column(name="correspondent_account", type="text", nullable=false)
     * @Assert\NotNull()
     */
    private $correspondentAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="checking_account", type="text", nullable=false)
     * @Assert\NotNull()
     */
    private $checkingAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=false)
     * @Assert\NotNull()
     */
    private $title;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrganizationBundle\Entity\Organization")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    private $organization;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBic(): ?string
    {
        return $this->bic;
    }

    /**
     * @param string $bic
     */
    public function setBic(string $bic)
    {
        $this->bic = $bic;
    }

    /**
     * @return string
     */
    public function getCorrespondentAccount(): ?string
    {
        return $this->correspondentAccount;
    }

    /**
     * @param string $correspondentAccount
     */
    public function setCorrespondentAccount(string $correspondentAccount)
    {
        $this->correspondentAccount = $correspondentAccount;
    }

    /**
     * @return string
     */
    public function getCheckingAccount(): ?string
    {
        return $this->checkingAccount;
    }

    /**
     * @param string $checkingAccount
     */
    public function setCheckingAccount(string $checkingAccount)
    {
        $this->checkingAccount = $checkingAccount;
    }

    /**
     * @return Organization
     */
    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return User
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }


}