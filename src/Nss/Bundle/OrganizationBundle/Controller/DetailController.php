<?php

namespace Nss\Bundle\OrganizationBundle\Controller;

use Nss\Bundle\OrganizationBundle\Entity\BankDetail;
use Nss\Bundle\OrganizationBundle\Form\Type\BankDetailType;
use Nss\Bundle\OrganizationBundle\Manager\DetailManager;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @Route("/detail")
*/
class DetailController extends Controller
{

    /**
     * @Route("/{id}", requirements={"id":"\d+"}, name="organization_detail")
     * @Template()
     * @ParamConverter("organization", class="NssOrganizationBundle:Organization")
     * @Method(methods={"GET"})
     * @Security("is_granted('allow', 'action:contact_my')")
     * @param Organization $organization
     * @return array
     */
    public function indexAction(Organization $organization) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Клиенты','organization_my');
        $breadcrumbs->addRouteItem($organization->getInn(),'organization_view', ['id' => $organization->getId()]);
        $breadcrumbs->addItem('Реквизиты');

        /** @var DetailManager $detailManager*/
        $detailManager = $this->get('organization.manager.detail_manager');

        return [
            'organization' => $organization,
            'details' => $detailManager->getByOrganizationAndUser($organization, $this->getUser())
        ];
    }

    /**
     * @Route("/create/{id}", requirements={"organizationId":"\d+"}, name="organization_detail_create")
     * @Template()
     * @ParamConverter("organization", class="NssOrganizationBundle:Organization")
     * @Security("is_granted('allow', 'action:contact_my')")
     * @param Request $request
     * @param Organization $organization
     * @return array|RedirectResponse
     */
    public function createAction(Request $request, Organization $organization)
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Клиенты','organization_my');
        $breadcrumbs->addRouteItem($organization->getInn(),'organization_view', ['id' => $organization->getId()]);
        $breadcrumbs->addRouteItem('Реквизиты', 'organization_detail', ['id' => $organization->getId()]);
        $breadcrumbs->addItem('Создание');

        $form = $this->createForm(BankDetailType::class);
        $form->handleRequest($request);

        /** @var DetailManager $detailManager*/
        $detailManager = $this->get('organization.manager.detail_manager');

        if ($form->isSubmitted() && $form->isValid()) {
            $detailManager->save($form->getData(), $organization, $this->getUser());

            $this->addFlash('notice','EDIT_SUCCESS');
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('organization_detail', [
                'id' => $organization->getId()
            ]);
        }

        return [
            'form' => $form->createView()
        ];
    }


    /**
     * @Route("/edit/{id}", requirements={"id":"\d+"}, name="organization_detail_edit")
     * @Template("@NssOrganization/Detail/create.html.twig")
     * @ParamConverter("detail", class="NssOrganizationBundle:BankDetail")
     * @Security("is_granted('allow', 'action:contact_my')")
     * @param Request $request
     * @param BankDetail $detail
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, BankDetail $detail)
    {
        $organization = $detail->getOrganization();

        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Клиенты','organization_my');
        $breadcrumbs->addRouteItem($organization->getInn(),'organization_view', ['id' => $organization->getId()]);
        $breadcrumbs->addRouteItem('Реквизиты', 'organization_detail', ['id' => $organization->getId()]);
        $breadcrumbs->addItem('Создание');

        $form = $this->createForm(BankDetailType::class, $detail);
        $form->handleRequest($request);

        /** @var DetailManager $detailManager*/
        $detailManager = $this->get('organization.manager.detail_manager');

        if ($form->isSubmitted() && $form->isValid()) {
            $detailManager->save($form->getData(), $organization, $this->getUser());

            $this->addFlash('notice','EDIT_SUCCESS');
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('organization_detail', [
                'id' => $organization->getId()
            ]);
        }

        return [
            'form' => $form->createView()
        ];
    }
}