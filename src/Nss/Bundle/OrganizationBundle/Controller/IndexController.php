<?php

namespace Nss\Bundle\OrganizationBundle\Controller;

use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use Nss\Bundle\OrganizationBundle\Manager\ContactManager;
use Nss\Bundle\OrganizationBundle\Manager\DetailManager;
use Nss\Bundle\OrganizationBundle\Manager\OrderManager;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\OrganizationBundle\Manager\OrganizationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class IndexController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/", name="organization_my")
     * @Template()
     * @param Request $request
     * @Method(methods={"GET"})
     * @Security("is_granted('allow', 'action:contact_my')")
     * @return array
     */
    public function myAction(Request $request) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Клиенты');

        /** @var OrganizationManager $manager*/
        $manager = $this->get('organization.manager.organization_manager');

        /* @var $filter Filter */
        $filter = $this->get('it_symfony.data_table.filter')->getFilter($request, $manager->getFilters());

        $pagination = $manager->getMyPaginated(
            $this->getUser(),
            $request->query->get('page', 1),
            self::PAGE_LIMIT,
            $filter
        );

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }


    /**
     * @Route("/{id}/view", requirements={"id":"\d+"}, name="organization_view")
     * @Template()
     * @ParamConverter("organization", class="NssOrganizationBundle:Organization")
     * @param Organization $organization
     * @Method(methods={"GET"})
     * @Security("is_granted('allow', 'action:contact_my')")
     * @return array
     */
    public function viewAction(Organization $organization) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Клиенты','organization_my');
        $breadcrumbs->addItem($organization->getInn());

        return [
            'organization' => $organization
        ];
    }

    /**
     * @Route("/{id}/orders", requirements={"id":"\d+"}, name="organization_order")
     * @Template()
     * @ParamConverter("organization", class="NssOrganizationBundle:Organization")
     * @Method(methods={"GET"})
     * @Security("is_granted('allow', 'action:contact_my')")
     * @return array
     */
    public function orderAction(Request $request, Organization $organization) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Клиенты','organization_my');
        $breadcrumbs->addRouteItem($organization->getInn(),'organization_view', ['id' => $organization->getId()]);
        $breadcrumbs->addItem('Заявки');

        /** @var OrderManager $orderManager*/
        $orderManager = $this->get('organization.manager.order_manager');

        $orders = $orderManager->getPaginated(
            $this->getUser(),
            $organization,
            $request->query->get('page', 1),
            self::PAGE_LIMIT
        );

        return [
            'organization' => $organization,
            'orders' => $orders
        ];
    }

    /**
     * @Route("/{id}/contacts", requirements={"id":"\d+"}, name="organization_contact")
     * @Template()
     * @ParamConverter("organization", class="NssOrganizationBundle:Organization")
     * @Method(methods={"GET"})
     * @Security("is_granted('allow', 'action:contact_my')")
     * @return array
     */
    public function contactAction(Request $request, Organization $organization) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Клиенты','organization_my');
        $breadcrumbs->addRouteItem($organization->getInn(),'organization_view', ['id' => $organization->getId()]);
        $breadcrumbs->addItem('Контакты');

        /** @var ContactManager $contactManager*/
        $contactManager = $this->get('organization.manager.contact_manager');

        $contacts = $contactManager->getPaginated(
            $this->getUser(),
            $organization,
            $request->query->get('page', 1),
            self::PAGE_LIMIT
        );

        return [
            'organization' => $organization,
            'contacts' => $contacts
        ];
    }
}