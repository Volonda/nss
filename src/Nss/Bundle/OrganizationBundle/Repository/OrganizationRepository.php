<?php

namespace Nss\Bundle\OrganizationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nss\Bundle\OrganizationBundle\Entity\Organization;

class OrganizationRepository extends EntityRepository
{
    /**
     * @param string $inn
     * @return null|Organization
    */
    public function finByOneInn(string $inn)
    {
        /** @var Organization $organization*/
        $organization = $this->findOneBy(['inn' => $inn]);

        return $organization;
    }
}
