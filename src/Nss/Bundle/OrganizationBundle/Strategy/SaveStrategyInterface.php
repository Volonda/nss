<?php
namespace Nss\Bundle\OrganizationBundle\Strategy;

use Nss\Bundle\OrganizationBundle\Entity\Organization;

interface SaveStrategyInterface
{
    public function setData(Organization $organization, $data);
}