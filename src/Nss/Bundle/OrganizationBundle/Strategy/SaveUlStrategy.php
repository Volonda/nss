<?php
namespace Nss\Bundle\OrganizationBundle\Strategy;

use Nss\Bundle\OrganizationBundle\Entity\Organization;

class SaveUlStrategy implements SaveStrategyInterface
{
    public function setData(Organization $organization, $data)
    {
        $organization->setSeldonData($data);
        $organization->setShortName($data->company_card->basic->shortName);
        $organization->setFullName($data->company_card->basic->fullName);
    }
}