<?php
namespace Nss\Bundle\OrganizationBundle\Strategy;

use Nss\Bundle\OrganizationBundle\Entity\Organization;

class SaveIpStrategy implements SaveStrategyInterface
{
    public function setData(Organization $organization, $data)
    {
        $organization->setSeldonData($data); 
        $organization->setFullName($data->basic->person->fullName);
    }
}