<?php
namespace Nss\Bundle\OrganizationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class InnValid extends Constraint
{
    public $invalidValue = 'Неверное значение ИНН';
}