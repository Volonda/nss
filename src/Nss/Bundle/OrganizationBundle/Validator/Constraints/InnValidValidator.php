<?php
namespace Nss\Bundle\OrganizationBundle\Validator\Constraints;

use Nss\Bundle\OrganizationBundle\Resolver\TypeResolver;
use Nss\Bundle\OrganizationBundle\Validator\Constraints\OrganizationExists;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class InnValidValidator extends ConstraintValidator
{
    /**
     * @var CompanyTypeSeldonResolver
    */
    private $resolver;

    public function __construct(TypeResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * @param string $inn
     * @param Constraint $constraint
     * @return bool
    */
    public function validate($inn, Constraint $constraint) : bool
    {
        /** @var $constraint InnValid */

        if (!empty($inn)) {

            $type = $this->resolver->getType($inn);

            if (!$type) {

                $this->context
                    ->buildViolation($constraint->invalidValue)
                    ->addViolation();

                return false;
            }
        }

        return true;
    }
}