<?php
namespace Nss\Bundle\OrganizationBundle\Resolver;

use Doctrine\ORM\EntityManager;
use ITSymfony\Bundle\SeldonBundle\Model\CompanyTypeResolverModel;

class TypeResolver implements CompanyTypeResolverModel
{
    public function getType($inn)
    {
        if(strlen($inn) == 10) {

            return CompanyTypeResolverModel::UL_TYPE;
        } elseif(strlen($inn) == 12) {

            return CompanyTypeResolverModel::IP_TYPE;
        } else {

            return false;
        }
    }
}