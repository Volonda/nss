<?php
namespace Nss\Bundle\OrganizationBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use ITSymfony\Bundle\SeldonBundle\Api\SeldonApi;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Nss\Bundle\BaseCollectionBundle\Entity\BaseOrganizationType;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\OrganizationBundle\Resolver\TypeResolver;
use Nss\Bundle\OrganizationBundle\Strategy\SaveIpStrategy;
use Nss\Bundle\OrganizationBundle\Strategy\SaveStrategyInterface;
use Nss\Bundle\OrganizationBundle\Strategy\SaveUlStrategy;
use Knp\Component\Pager\Paginator;
use Nss\Bundle\UserBundle\Entity\User;

class ContactManager
{
    /** @var EntityManager*/
    private $em;

    /** @var Paginator*/
    private $paginator;

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
    */
    public function __construct(
        EntityManager $em,
        Paginator $paginator
    ) {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @param User $user
     * @param Organization $organization
     * @param int $page
     * @param int $limit
     * @return PaginationInterface
     */
    public function getPaginated(User $user, Organization $organization, int $page, int $limit) : PaginationInterface
    {
        $qb = $this->em->createQueryBuilder()
            ->select('c')
            ->from(Contact::class, 'c')
            ->join('c.owner','own')
            ->join('c.organization','o')
            ->where('o = :organization')
            ->setParameter('organization', $organization->getId())
            ->andWhere('own = :owner')
            ->setParameter('owner', $user)
        ;

        return $this->paginator->paginate($qb, $page, $limit,
            ['defaultSortFieldName' => 'o.id', 'defaultSortDirection' => 'ASC']
        );
    }
}
