<?php
namespace Nss\Bundle\OrganizationBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use ITSymfony\Bundle\SeldonBundle\Api\SeldonApi;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Nss\Bundle\BaseCollectionBundle\Entity\BaseOrganizationType;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\OrganizationBundle\Resolver\TypeResolver;
use Nss\Bundle\OrganizationBundle\Strategy\SaveIpStrategy;
use Nss\Bundle\OrganizationBundle\Strategy\SaveStrategyInterface;
use Nss\Bundle\OrganizationBundle\Strategy\SaveUlStrategy;
use Knp\Component\Pager\Paginator;
use Nss\Bundle\UserBundle\Entity\User;

class OrganizationManager
{
    /** @var EntityManager*/
    private $em;

    /** @var TypeResolver*/
    private $typeResolver;

    /**  @var SeldonApi $seldon*/
    private $seldon;

    /** @var array*/
    private $filters = [
        'id' => 'int',
        'o.inn' => 'like',
        'o.fullName' => 'like'
    ];

    /** @var Paginator*/
    private $paginator;

    /**
     * @param EntityManager $em
     * @param SeldonApi $seldon
     * @param TypeResolver $typeResolver
     * @param Paginator $paginator
    */
    public function __construct(
        EntityManager $em,
        SeldonApi $seldon,
        TypeResolver $typeResolver,
        Paginator $paginator
    ) {
        $this->em = $em;
        $this->seldon = $seldon;
        $this->typeResolver = $typeResolver;
        $this->paginator = $paginator;
    }

    /**
     * @return array
    */
    public function getFilters()
    {
        $filters = $this->filters;

        return $filters;
    }

    /**
     * @param string $inn
     * @return Organization
     * @throws \Exception
    */
    public function getByInnOrCreate(string $inn) : Organization
    {
        $organization = $this->em->getRepository(Organization::class)->findOneBy(['inn' => $inn]);
        if (!$organization instanceof Organization) {
            $organization = new Organization();
            $organization->setInn($inn);

            $type = null;
            /** @var EntityRepository $typeRepo*/
            $typeRepo = $this->em->getRepository(BaseOrganizationType::class);

            $saveStrategy = null;

            switch ($this->typeResolver->getType($inn)){
                case TypeResolver::IP_TYPE:
                    $type = $typeRepo->findOneBy(['code' => BaseOrganizationType::TYPE_IP]);
                    $saveStrategy = new SaveIpStrategy();
                    break;
                case TypeResolver::UL_TYPE:
                    $type = $typeRepo->findOneBy(['code' => BaseOrganizationType::TYPE_UL]);
                    $saveStrategy = new SaveUlStrategy();
                    break;
                default:
                    throw new \Exception('Invalid inn type');
            }

            if (!$type instanceof BaseOrganizationType) {
                throw  new \Exception('undefined organization type');
            }

            $organization->setType($type);

            //заполнение из селдона
            if (
                !$organization->getSeldonData()
                && $saveStrategy instanceof SaveStrategyInterface
                &&  $seldonData = $this->seldon->getDataByInnOrNull($inn)
            ) {

                $saveStrategy->setData($organization, $seldonData);
            }

            $this->em->persist($organization);
        }

        return $organization;
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @param Filter $filter
     * @return PaginationInterface
     */
    public function getMyPaginated(User $user, int $page, int $limit, Filter $filter) : PaginationInterface
    {
        $qb = $this->em->createQueryBuilder()
            ->select('o')
            ->from(Organization::class,'o')
            ->join(Contact::class,'c','WITH','c.organization = o')
            ->leftJoin('c.orders','ord')
            ->leftJoin('ord.workflow','w')
            ->where('c.owner = :_user')
            ->setParameter('_user', $user)
        ;

        $filter->addFiltersToQB($qb);

        return $this->paginator->paginate($qb, $page, $limit,
            ['defaultSortFieldName' => 'o.id', 'defaultSortDirection' => 'ASC']
        );
    }
}
