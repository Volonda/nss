<?php
namespace Nss\Bundle\OrganizationBundle\Manager;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\OrganizationBundle\Entity\BankDetail;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\UserBundle\Entity\User;

class DetailManager
{
    /** @var EntityManager*/
    private $em;

    /**
     * @param EntityManager $em
    */
    public function __construct(
        EntityManager $em
    ) {
        $this->em = $em;
    }

    /**
     * @param User $user
     * @param Organization $organization
     * @return array
     */
    public function getByOrganizationAndUser(Organization $organization, User $user) : array
    {
        $qb = $this->em->createQueryBuilder()
            ->select('d')
            ->from(BankDetail::class, 'd')
            ->join('d.owner','own')
            ->join('d.organization','o')
            ->where('o = :organization')
            ->setParameter('organization', $organization->getId())
            ->andWhere('own = :owner')
            ->setParameter('owner', $user)
        ;

        return $qb->getQuery()->getResult();
    }

    public function save(BankDetail $detail, Organization $organization, User $user)
    {
        $detail->setOrganization($organization);
        $detail->setOwner($user);

        $this->em->persist($detail);
    }
}
