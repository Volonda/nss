<?php
namespace Nss\Bundle\OrganizationBundle\Form\Type;

use Nss\Bundle\OrganizationBundle\Entity\BankDetail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BankDetailType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('bic', TextType::class, [
            'label' => 'БИК'
        ])->add('correspondentAccount', TextType::class, [
            'label' => 'Корреспондентский счет',
            'required' => false
        ])->add('checkingAccount', TextType::class, [
            'label' => 'Расчетный счет',
            'required' => false
        ])->add('title', TextType::class, [
            'label' => 'Наименование',
            'required' => false
        ])->add('submit', SubmitType::class, [
            'label' => 'FORM_SUBMIT_SEND'
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BankDetail::class,
            'label' => false
        ]);
    }
}