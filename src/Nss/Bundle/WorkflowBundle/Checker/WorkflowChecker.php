<?php
namespace Nss\Bundle\WorkflowBundle\Checker;

use CheckerBundle\Model\CheckerInterface;
use Doctrine\ORM\EntityManager;
use ITSymfony\Bundle\ActionRolePermissionBundle\Security\ActionVoter;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\UserBundle\Entity\Action;
use Nss\Bundle\UserBundle\Entity\User;
use Nss\Bundle\WorkflowBundle\Entity\Workflow;
use Nss\Bundle\WorkflowBundle\Entity\Transition;
use Nss\Bundle\WorkflowBundle\Entity\WorkflowAction;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class WorkflowChecker implements CheckerInterface
{
    const NAME = 'workflow';

    /** @var EntityManager*/
    private $em;

    /** @var AuthorizationCheckerInterface*/
    private $authorizationChecker;

    public function __construct(EntityManager $em, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $em;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param Order $order
     * @param Transition $transition
     * @return bool
     */
    public function isTransitional(Order $order, Transition $transition) : bool
    {
        if ($transition->getCurrentStatus()->getId() == $order->getStatus()->getId()) {

            return true;
        }

        return false;
    }

    /**
     * @param Order $order
     * @param string $action
     * @return boolean
     */
    public function isActionAllowed(Order $order, string $action) : bool
    {
        return $this->isWorkflowActionAllowed($order, $action)
            && $this->authorizationChecker->isGranted(ActionVoter::ALLOW, 'action:' . $action);
    }

    /**
     * @param Order $order
     * @param string $action
     * @return boolean
    */
    public function isWorkflowActionAllowed(Order $order, string $action) : bool
    {
        $action = $this->em->getRepository(Action::class)->findOneBy(['code' => $action]);

        if ($action) {

            $query = $this->em->createQueryBuilder()
                ->select('wa')
                ->from(WorkflowAction::class, 'wa')
                ->join('wa.workflow','w')
                ->join('wa.actions','a')
                ->join('wa.currentStatus','s')
                ->where('w = :workflow')
                ->setParameter('workflow', $order->getWorkflow())
                ->andWhere('a = :action')
                ->setParameter('action', $action)
                ->andWhere('s =:currentStatus')
                ->setParameter('currentStatus', $order->getStatus())
                ->setMaxResults(1)
                ->getQuery()
                ->useQueryCache(true)

            ;
            /** @var Workflow $result*/
            $workflowAction = $query->getOneOrNullResult();

            if ($workflowAction) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return static::NAME;
    }
}