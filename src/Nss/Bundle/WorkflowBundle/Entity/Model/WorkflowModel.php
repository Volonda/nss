<?php
namespace Nss\Bundle\WorkflowBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\OrderType;
use Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow;


/**
 * @ORM\Entity()
 * @ORM\Table(name="model_workflows", schema="workflow")
 */
class WorkflowModel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BaseWorkflow
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow")
     * @ORM\JoinColumn(name="base_workflow_id", referencedColumnName="id")
     */
    private $baseWorkflow;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel", mappedBy="workflowModel")
     */
    private $orderModels;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowActionModel", mappedBy="workflowModel",cascade={"persist"})
     */
    private $workflowActions;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Model\TransitionModel", mappedBy="workflowModel",cascade={"persist"})
     */
    private $workflowTransitions;

    public function __construct()
    {
        $this->workflowActions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return BaseWorkflow
     */
    public function getBaseWorkflow() :? BaseWorkflow
    {
        return $this->baseWorkflow;
    }

    /**
     * @param BaseWorkflow $baseWorkflow
     */
    public function setBaseWorkflow(BaseWorkflow $baseWorkflow)
    {
        $this->baseWorkflow = $baseWorkflow;
    }

    /**
     * @return Collection
     */
    public function getWorkflowActions() : Collection
    {
        return $this->workflowActions;
    }

    /**
     * @param Collection $workflowActions
     */
    public function setWorkflowActions($workflowActions)
    {
        $this->workflowActions = $workflowActions;
    }

    /**
     * @return Collection
     */
    public function getOrderModels() : Collection
    {
        return $this->orderModels;
    }

    /**
     * @param Collection $orderModels
     */
    public function setOrderModels(Collection $orderModels)
    {
        $this->orderModels = $orderModels;
    }

    /**
     * @return Collection
     */
    public function getWorkflowTransitions(): Collection
    {
        return $this->workflowTransitions;
    }

    /**
     * @param Collection $workflowTransitions
     */
    public function setWorkflowTransitions(Collection $workflowTransitions)
    {
        $this->workflowTransitions = $workflowTransitions;
    }

}