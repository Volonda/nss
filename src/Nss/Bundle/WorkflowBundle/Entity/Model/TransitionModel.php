<?php
namespace Nss\Bundle\WorkflowBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\UserBundle\Entity\Action;
use Nss\Bundle\UserBundle\Entity\Group;
use Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow;

/**
 * @ORM\Entity()
 * @ORM\Table(name="model_transitions", schema="workflow")
 */
class TransitionModel
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @var BaseOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderStatus")
     * @ORM\JoinColumn(name="current_status_id", referencedColumnName="id")
     */
    private $currentStatus;

    /**
     * @var BaseOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderStatus")
     * @ORM\JoinColumn(name="transition_status_id", referencedColumnName="id")
     */
    private $transitionStatus;

    /**
     * @var WorkflowModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel", inversedBy="workflowTransitions")
     * @ORM\JoinColumn(name="workflow_model_id", referencedColumnName="id")
     */
    private $workflowModel;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Nss\Bundle\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="workflow.model_transition_groups",
     *      joinColumns={@ORM\JoinColumn(name="transition_model_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     */
    private $groups;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return BaseOrderStatus
     */
    public function getCurrentStatus() : ?BaseOrderStatus
    {
        return $this->currentStatus;
    }

    /**
     * @param BaseOrderStatus $currentStatus
     */
    public function setCurrentStatus(BaseOrderStatus $currentStatus)
    {
        $this->currentStatus = $currentStatus;
    }

    /**
     * @return BaseOrderStatus
     */
    public function getTransitionStatus(): ?BaseOrderStatus
    {
        return $this->transitionStatus;
    }

    /**
     * @param BaseOrderStatus $transitionStatus
     */
    public function setTransitionStatus(BaseOrderStatus $transitionStatus)
    {
        $this->transitionStatus = $transitionStatus;
    }

    /**
     * @return Collection
     */
    public function getGroups(): ?Collection
    {
        return $this->groups;
    }

    /**
     * @param Collection $groups
     */
    public function setGroups(Collection $groups)
    {
        $this->groups = $groups;
    }

    public function addGroup(Group $group)
    {
        $this->groups->add($group);
    }

    /**
     * @return WorkflowModel
     */
    public function getWorkflowModel(): ?WorkflowModel
    {
        return $this->workflowModel;
    }

    /**
     * @param WorkflowModel $workflowModel
     */
    public function setWorkflowModel(WorkflowModel $workflowModel)
    {
        $this->workflowModel = $workflowModel;
    }
}