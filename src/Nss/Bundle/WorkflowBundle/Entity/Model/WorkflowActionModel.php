<?php
namespace Nss\Bundle\WorkflowBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\UserBundle\Entity\Action;
use Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow;

/**
 * @ORM\Entity()
 * @ORM\Table(name="model_workflow_actions", schema="workflow")
 */
class WorkflowActionModel
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BaseOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderStatus")
     * @ORM\JoinColumn(name="current_status_id", referencedColumnName="id")
     */
    private $currentStatus;

    /**
     * @var WorkflowModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel", inversedBy="workflowActions")
     * @ORM\JoinColumn(name="workflow_model_id", referencedColumnName="id")
     */
    private $workflowModel;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Nss\Bundle\UserBundle\Entity\Action")
     * @ORM\JoinTable(name="workflow.model_workflow_action_actions",
     *      joinColumns={@ORM\JoinColumn(name="workflow_model_action_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="action_id", referencedColumnName="id")}
     *      )
     */
    private $actions;

    public function __construct()
    {
        $this->actions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return BaseOrderStatus
     */
    public function getCurrentStatus(): ?BaseOrderStatus
    {
        return $this->currentStatus;
    }

    /**
     * @param BaseOrderStatus $currentStatus
     */
    public function setCurrentStatus(BaseOrderStatus $currentStatus)
    {
        $this->currentStatus = $currentStatus;
    }


    /**
     * @return Collection
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    /**
     * @param Collection $actions
     */
    public function setActions(Collection $actions)
    {
        $this->actions = $actions;
    }

    public function addAction(Action $action)
    {
        $this->actions->add($action);
    }

    /**
     * @return WorkflowModel
     */
    public function getWorkflowModel(): ?WorkflowModel
    {
        return $this->workflowModel;
    }

    /**
     * @param WorkflowModel $workflowModel
     */
    public function setWorkflowModel(WorkflowModel $workflowModel)
    {
        $this->workflowModel = $workflowModel;
    }

}