<?php
namespace Nss\Bundle\WorkflowBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\Order;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_workflows", schema="workflow")
 */
class Workflow
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BaseWorkflow
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow")
     * @ORM\JoinColumn(name="base_workflow_id", referencedColumnName="id")
     */
    private $baseWorkflow;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Transition", mappedBy="workflow")
     */
    private $transitions;

    /**
     * @var Order
     *
     * @ORM\OneToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Order", inversedBy="workflow")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\WorkflowBundle\Entity\WorkflowAction", mappedBy="workflow")
     */
    private $workflowActions;

    public function __construct()
    {
        $this->transitions = new ArrayCollection();
        $this->workflowActions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return BaseWorkflow
     */
    public function getBaseWorkflow(): BaseWorkflow
    {
        return $this->baseWorkflow;
    }

    /**
     * @param BaseWorkflow $baseWorkflow
     */
    public function setBaseWorkflow(BaseWorkflow $baseWorkflow)
    {
        $this->baseWorkflow = $baseWorkflow;
    }

    /**
     * @return Collection
     */
    public function getTransitions(): Collection
    {
        return $this->transitions;
    }

    /**
     * @param Collection $transitions
     */
    public function setTransitions(Collection $transitions)
    {
        $this->transitions = $transitions;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param Transition $transition
    */
    public function addTransition(Transition $transition)
    {
        $this->transitions->add($transition);
    }

    /**
     * @return Collection
     */
    public function getWorkflowActions(): Collection
    {
        return $this->workflowActions;
    }

    /**
     * @param Collection $workflowActions
     */
    public function setWorkflowActions(Collection $workflowActions)
    {
        $this->workflowActions = $workflowActions;
    }

    public function addWorkflowAction(WorkflowAction $workflowAction)
    {
        $this->workflowActions->add($workflowAction);
    }
}