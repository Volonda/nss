<?php
namespace Nss\Bundle\WorkflowBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_transitions", schema="workflow")
 */
class Transition
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BaseOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderStatus")
     * @ORM\JoinColumn(name="current_status_id", referencedColumnName="id")
     */
    private $currentStatus;

    /**
     * @var BaseOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderStatus")
     * @ORM\JoinColumn(name="transition_status_id", referencedColumnName="id")
     */
    private $transitionStatus;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Workflow", inversedBy="transitions")
     * @ORM\JoinColumn(name="order_workflow_id", referencedColumnName="id")
     */
    private $workflow;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Nss\Bundle\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="workflow.app_transition_groups",
     *      joinColumns={@ORM\JoinColumn(name="transition_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     */
    private $groups;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return BaseOrderStatus
     */
    public function getCurrentStatus() : BaseOrderStatus
    {
        return $this->currentStatus;
    }

    /**
     * @param BaseOrderStatus $currentStatus
     */
    public function setCurrentStatus(BaseOrderStatus $currentStatus)
    {
        $this->currentStatus = $currentStatus;
    }

    /**
     * @return BaseOrderStatus
     */
    public function getTransitionStatus(): BaseOrderStatus
    {
        return $this->transitionStatus;
    }

    /**
     * @param BaseOrderStatus $transitionStatus
     */
    public function setTransitionStatus(BaseOrderStatus $transitionStatus)
    {
        $this->transitionStatus = $transitionStatus;
    }

    /**
     * @return Workflow
     */
    public function getWorkflow() :Workflow
    {
        return $this->workflow;
    }

    /**
     * @param Workflow $workflow
     */
    public function setWorkflow(Workflow $workflow)
    {
        $this->workflow = $workflow;
    }

    /**
     * @return Collection
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * @param Collection $groups
     */
    public function setGroups(Collection $groups)
    {
        $this->groups = $groups;
    }
}