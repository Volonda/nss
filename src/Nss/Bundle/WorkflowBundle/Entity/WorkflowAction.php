<?php
namespace Nss\Bundle\WorkflowBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\UserBundle\Entity\Action;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_workflow_actions", schema="workflow")
 */
class WorkflowAction
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BaseOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderStatus")
     * @ORM\JoinColumn(name="current_status_id", referencedColumnName="id")
     */
    private $currentStatus;

    /**
     * @var Workflow
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Workflow", inversedBy="workflowActions")
     * @ORM\JoinColumn(name="workflow_id", referencedColumnName="id")
     */
    private $workflow;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Nss\Bundle\UserBundle\Entity\Action")
     * @ORM\JoinTable(name="workflow.app_workflow_action_actions",
     *      joinColumns={@ORM\JoinColumn(name="workflow_action_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="action_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $actions;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return BaseOrderStatus
     */
    public function getCurrentStatus(): ?BaseOrderStatus
    {
        return $this->currentStatus;
    }

    /**
     * @param BaseOrderStatus $currentStatus
     */
    public function setCurrentStatus(BaseOrderStatus $currentStatus)
    {
        $this->currentStatus = $currentStatus;
    }

    /**
     * @return Workflow
     */
    public function getWorkflow(): ?Workflow
    {
        return $this->workflow;
    }

    /**
     * @param Workflow $workflow
     */
    public function setWorkflow(Workflow $workflow)
    {
        $this->workflow = $workflow;
    }


    /**
     * @return Collection
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    /**
     * @param Collection $actions
     */
    public function setActions(Collection $actions)
    {
        $this->actions = $actions;
    }

    public function addAction(Action $action)
    {
        $this->actions->add($action);
    }
}