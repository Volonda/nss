<?php
namespace Nss\Bundle\WorkflowBundle\Lead;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\UserBundle\Entity\Group;
use Nss\Bundle\UserBundle\Entity\User;
use Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow;
use Nss\Bundle\WorkflowBundle\Entity\Model\ActionModel;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel;
use Nss\Bundle\WorkflowBundle\Entity\Model\TransitionModel;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModelAction;
use Nss\Bundle\WorkflowBundle\Entity\Workflow;
use Nss\Bundle\WorkflowBundle\Entity\Transition;
use Nss\Bundle\WorkflowBundle\Entity\WorkflowAction;
use Nss\Bundle\WorkflowBundle\Event\TransitionEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class WorkflowLead
{
    /** @var EntityManager */
    private $em;

    /** @var EventDispatcherInterface $dispatcher */
    private $dispatcher;

    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Order $order
     * @param User $user
     * @return array
    */
    public function getTransitions(Order $order, User $user) : array
    {
        $qb = $this->em->createQueryBuilder();

        $transitions = [];
        $groups = [];

        /** @var Group $group*/
        foreach ($user->getGroups() as $group) {
            $groups = $group->getId();
        }

        if (count($groups)) {
            $query = $qb
                ->select('t')
                ->from(Transition::class, 't')
                ->join('t.workflow', 'ow')
                ->join('t.groups', 'g')
                ->where('ow.order = :order')
                ->setParameter('order', $order)
                ->andWhere('t.currentStatus = :currentStatus')
                ->setParameter('currentStatus', $order->getStatus())
                ->andWhere($qb->expr()->in('g', $groups))
                ->getQuery();

            $transitions = $query->getResult();
        }
        return $transitions;
    }

    /**
     * @param Order $order
     * @param Transition $transition
     */
    public function transition(Order $order, Transition $transition)
    {
        $order->setStatus($transition->getTransitionStatus());
        $this->dispatcher->dispatch('workflow_transition', new TransitionEvent($order, $transition));
    }

    public function create(SroOrderModel $orderModel, Order $order)
    {
        /** @var BaseWorkflow $baseWorkflow */
        $workflowModel = $orderModel->getWorkflowModel();
        $transitionModels = $orderModel->getWorkflowModel()->getWorkflowTransitions();

        $workflow = new Workflow();
        $workflow->setOrder($order);
        $workflow->setBaseWorkflow($workflowModel->getBaseWorkflow());

        /** @var TransitionModel $transitionModel*/
        foreach ($transitionModels as $transitionModel) {
            $transition = new Transition();
            $transition->setCurrentStatus($transitionModel->getCurrentStatus());
            $transition->setTransitionStatus($transitionModel->getTransitionStatus());
            $transition->setWorkflow($workflow);
            $transition->setGroups($transitionModel->getGroups());
            $this->em->persist($transition);
        }
        /** @var ActionModel $workflowModelAction*/
        foreach ( $workflowModel->getWorkflowActions() as $workflowModelAction) {
            $workflowAction = new WorkflowAction();
            $workflowAction->setActions($workflowModelAction->getActions());
            $workflowAction->setCurrentStatus($workflowModelAction->getCurrentStatus());

            $workflow->addWorkflowAction($workflowAction);
            $workflowAction->setWorkflow($workflow);

            $this->em->persist($workflowAction);
        }


        $this->em->persist($workflow);
    }
}