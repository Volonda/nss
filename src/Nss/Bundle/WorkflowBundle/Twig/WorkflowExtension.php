<?php
namespace Nss\Bundle\WorkflowBundle\Twig;

use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\UserBundle\Entity\User;
use Nss\Bundle\WorkflowBundle\Lead\WorkflowLead;

class WorkflowExtension extends \Twig_Extension
{
    /** @var WorkflowLead*/
    private $lead;

    /**
     * @param WorkflowLead $lead
    */
    public function  __construct(WorkflowLead $lead)
    {
        $this->lead = $lead;
    }

    public function getFunctions()
    {
        return [ new \Twig_SimpleFunction('workflowTransitions', [$this, 'getWorkflowTransitions'] ) ];
    }

    /**
     * @param Order $order
     * @param User $user
     * @return array
     */
    public function getWorkflowTransitions(Order $order, User $user) : array
    {
        return $this->lead->getTransitions($order, $user);
    }

    public function getName()
    {
        return 'workflow_extension';
    }
}