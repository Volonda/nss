<?php

namespace Nss\Bundle\WorkflowBundle\EventListener;

use Nss\Bundle\OrderBundle\Event\OrderCreatedEvent;
use Nss\Bundle\WorkflowBundle\Lead\WorkflowLead;


class OrderListener
{
    /**
     * @var WorkflowLead
     */
    private $workflowLead;

    /**
     * @param WorkflowLead $workflowLead
     */
    public function __construct(WorkflowLead $workflowLead)
    {
        $this->workflowLead = $workflowLead;
    }

    /**
     * @param OrderCreatedEvent $event
     */
    public function onCreate(OrderCreatedEvent $event)
    {
        $this->workflowLead->create($event->getOrderModel(), $event->getOrder());
    }
}