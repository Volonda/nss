<?php
namespace Nss\Bundle\WorkflowBundle\Event;

use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\WorkflowBundle\Entity\Transition;
use Symfony\Component\EventDispatcher\Event;

class TransitionEvent extends Event
{
    /** @var Order*/
    private $order;

    /** @var Transition */
    private $transition;

    /**
     * @param Order $order
     * @param Transition $transition
     */
    public function __construct(Order $order, Transition $transition)
    {
        $this->order = $order;
        $this->transition = $transition;
    }

    /**
     * @return Order
     */
    public function getOrder() : Order
    {
        return $this->order;
    }

    /**
     * @return Transition
     */
    public function getTransition() : Transition
    {
        return $this->transition;
    }
}