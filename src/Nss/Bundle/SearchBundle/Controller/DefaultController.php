<?php

namespace Nss\Bundle\SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NssSearchBundle:Default:index.html.twig');
    }
}
