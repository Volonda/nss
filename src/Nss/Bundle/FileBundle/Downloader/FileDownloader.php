<?php
namespace Nss\Bundle\FileBundle\Downloader;

use Nss\Bundle\FileBundle\Entity\File;
use Nss\Bundle\FileBundle\Strategy\PathStrategyInterface;
use Symfony\Component\HttpFoundation\Response;

class FileDownloader
{
    /** @var PathStrategyInterface $strategy*/
    private $strategy;

    /**
     * @var PathStrategyInterface $strategy
    */
    public function __construct(PathStrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @param File $file
     * @param int $attachment
     * @throws \Exception
     * @return Response
    */
    public function createResponse(File $file, $attachment = 0) : Response
    {
        $response = new Response();

        $this->strategy->loadFile($file);

        $fullPath =  $this->strategy->getFullPath();

        if (!file_exists($fullPath)){
            throw  new \Exception('File not found');
        }

        $response->setContent(file_get_contents($fullPath));
        $response->setStatusCode(200);
        $response->headers->add([
            'Content-type' => $file->getMimeType(),
            'Content-Disposition' => (($attachment) ?
                    "attachment;"
                    :""
                ) . "filename='" . $file->getClientOriginalName() . "'"
        ]);

        return $response;
    }
}
