<?php
namespace Nss\Bundle\FileBundle\Listener;

use Nss\Bundle\FileBundle\Entity\File;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Nss\Bundle\FileBundle\Strategy\PathStrategyInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileListener {

    /** @var PathStrategyInterface*/
    private $strategy;

    public function __construct(PathStrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($file)
    {
        if (!$file instanceof File) {
            return;
        }

        $uploadedFile = $file->getUploadedFile();
        if ($uploadedFile instanceof UploadedFile) {

            $file->setMimeType($uploadedFile->getMimeType());
            $file->setSize($uploadedFile->getSize());
            $file->setClientOriginalName($uploadedFile->getClientOriginalName());
            $file->setClientOriginalExtension($uploadedFile->getClientOriginalExtension());

            $this->strategy->createFile($file);
            $this->strategy->prepareDir();

            $file->setPath($this->strategy->getPath());

            $uploadedFile->move($this->strategy->getFullDir(), $this->strategy->getName());
        }

    }
}