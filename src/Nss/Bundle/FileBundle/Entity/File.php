<?php
namespace  Nss\Bundle\FileBundle\Entity;

use Nss\Bundle\AppBundle\Entity\Traits\TimestampableEntityString;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * File
 *
 * @ORM\Table(name="files", schema="file")
 * @ORM\Entity
 */
class File {

    use TimestampableEntityString;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Название
     * @var string

     * @ORM\Column(name="client_original_name", type="text", nullable=false)
     */
    private $clientOriginalName;

    /**
     * Расширение
     * @var string

     * @ORM\Column(name="client_original_extension", type="text", nullable=false)
     */
    private $clientOriginalExtension;

    /**
     * Тип
     * @var string
     *
     * @ORM\Column(name="mime_type", type="text", nullable=false)
     */
    private $mimeType;

    /**
     * Размер
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=false)
     */
    private $size;

    /**
     * Описание
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Путь
     * @var string
     *
     * @ORM\Column(name="path", type="text", nullable=true)
     */
    private $path;

    private $uploadedFile;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     */
    public function setMimeType(string $mimeType)
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function setUploadedFile(UploadedFile $uploadedFile)
    {
        $this->uploadedFile = $uploadedFile;
    }

    public function getUploadedFile() {

        return $this->uploadedFile;
    }

    /**
     * @return string
     */
    public function getClientOriginalName()
    {
        return $this->clientOriginalName;
    }

    /**
     * @param string $clientOriginalName
     */
    public function setClientOriginalName(string $clientOriginalName)
    {
        $this->clientOriginalName = $clientOriginalName;
    }

    /**
     * @return string
     */
    public function getClientOriginalExtension()
    {
        return $this->clientOriginalExtension;
    }

    /**
     * @param string $clientOriginalExtension
     */
    public function setClientOriginalExtension(string $clientOriginalExtension)
    {
        $this->clientOriginalExtension = $clientOriginalExtension;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

}