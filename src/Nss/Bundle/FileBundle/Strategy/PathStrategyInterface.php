<?php
namespace Nss\Bundle\FileBundle\Strategy;

use Nss\Bundle\FileBundle\Entity\File;

interface PathStrategyInterface
{
    public function createFile(File $file) : PathStrategyInterface;

    public function loadFile(File $file) : PathStrategyInterface;

    public function getDir() : string;

    public function getName() : string;

    public function getPath() : string;

    public function getFullPath() : string;

    public function getFullDir() : string;

    public function prepareDir();
}