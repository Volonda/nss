<?php
namespace Nss\Bundle\FileBundle\Strategy;

use Nss\Bundle\FileBundle\Entity\File;

class DefaultPathStrategy implements PathStrategyInterface {

    /** @var string*/
    private $uploadsDir;

    /** @var string*/
    private $name;

    /** @var string*/
    private $dir;

    /**
     * @param string $uploadsDir
    */
    public function __construct(String $uploadsDir)
    {
        $this->uploadsDir = $uploadsDir;
    }

    /**
     * @param File $file
     * @throws \Exception
     * @return PathStrategyInterface
     */
    public function createFile(File $file) : PathStrategyInterface
    {
        $now = new \DateTime('now');

        $this->dir = $now->format('Y_m_d');

        $extension = $file->getClientOriginalExtension();
        if (empty($extension)) {
            throw new \Exception('you need set ClientOriginalExtension before using DefaultNameStrategy');
        }

        $this->name = uniqid() . '.' . $file->getClientOriginalExtension();

        return $this;
    }

    /**
     * File $file
     * @return PathStrategyInterface
    */
    public function loadFile(File $file) : PathStrategyInterface
    {
        $path = explode('/', $file->getPath());

        $this->dir = $path[0];
        $this->name = $path[1];

        return $this;
    }

    /**
     * @return string
    */
    public function getDir() : string
    {
        return $this->dir;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath() : string
    {
        return $this->dir . '/' . $this->getName();
    }

    public function getFullPath() : string
    {
        return $this->uploadsDir . '/' . $this->getPath();
    }

    public function getFullDir() : string
    {
        return $this->uploadsDir . '/' . $this->getDir();
    }

    /**
     * @throws \Exception
    */
    public function prepareDir()
    {
        if (!is_dir($this->uploadsDir) || !is_writable($this->uploadsDir)) {

            throw new \Exception($this->uploadsDir . ' is not writable or not exists');
        }

        if (!is_dir($this->getFullDir())) {

            $oldmask = umask(0);
            if (!mkdir($this->getFullDir(), 0774)) {
                throw new \Exception('Unable create path ' . $this->getFullDir());
            }
            umask($oldmask);
        }
    }
}