<?php
namespace Nss\Bundle\DocumentBundle\Form\Type;

use Nss\Bundle\DocumentBundle\Entity\Document;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $document = $options['document'];

        if (!$document instanceof Document) {
            throw new \Exception('Unsupported document type');
        }

        $builder->add('description', TextareaType::class, [
            'label' => 'Описание',
            'required' => false,
        ]);

    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['document_row'] = true;
        $view->vars['document'] = $options['document'];
        $view->vars['order'] = $options['order'];
        $view->vars['type'] = $options['type'];
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('document');
        $resolver->setRequired('order');
        $resolver->setRequired('type');
    }
}