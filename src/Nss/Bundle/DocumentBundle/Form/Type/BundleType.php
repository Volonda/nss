<?php
namespace Nss\Bundle\DocumentBundle\Form\Type;

use Nss\Bundle\DocumentBundle\Entity\Document;
use Nss\Bundle\DocumentBundle\Entity\DocumentBundle;
use Nss\Bundle\OrderBundle\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class BundleType extends AbstractType {

    /** @var Router*/
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $documentBundle = $options['documentBundle'];
        $order = $options['order'];

        if (!$order instanceof Order) {
            throw new \Exception('Unsupported order type');
        }

        if ($documentBundle){
            if (!$documentBundle instanceof DocumentBundle) {
                throw new \Exception('Unsupported document bundle type');
            }

            /** @var Document $document*/
            foreach ($documentBundle->getDocuments() as $document) {

                $builder->add($document->getTemplate()->getCode(), DocumentType::class, [
                    'document' => $document,
                    'order' => $options['order'],
                    'label' => $document->getTemplate()->getName(),
                    'type' => $options['type']
                ]);
            }
        }


    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('documentBundle');
        $resolver->setRequired('order');
        $resolver->setRequired('type');

    }

}