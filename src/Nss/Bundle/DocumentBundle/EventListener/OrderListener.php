<?php

namespace Nss\Bundle\DocumentBundle\EventListener;


use Nss\Bundle\DocumentBundle\Manager\BundleManager;
use Nss\Bundle\OrderBundle\Event\OrderCreatedEvent;

class OrderListener
{
    /**
     * @var BundleManager
     */
    private $manager;

    /**
     * @param BundleManager $manager
     */
    public function __construct(BundleManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param OrderCreatedEvent $event
     */
    public function onCreate(OrderCreatedEvent $event)
    {
        $this->manager->createForOrder($event->getOrder(), $event->getOrderModel());
    }
}