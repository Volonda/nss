(function( $ ) {
    $.fn.documentDropzone = function(option) {

        if (option == undefined) {
            option = {}
        }
        var $element = $(this);

        var defaultOptions = {
            maxFiles: 2000,
            dictDefaultMessage: 'Перетащите сюда файл',
            dictFileTooBig: 'Файл слишком большой ({{filesize}} мб). Максимальный размер файла: {{maxFilesize}} мб.',
            dictInvalidFileType: 'Недопустимый форматфайла',
            dictResponseError: 'Сервер ответил со статусом {{statusCode}}',
            dictMaxFilesExceeded: 'Вы не можете отправить за раз уккболее, чем {{maxFiles}} файлов (файл)',
            clickable: $element.data('clickable'),
            uploadMultiple: true,
            fallback: function () {
                alert('Ваш браузер не поддерживает технологию drag and drop')
            },
            error:function () {
                alert('Произошла ошибка, обратитесь в техподдержку сайта');
            },
            success: function (file, response) {
                window.location.reload();
            },
            sending: function (file, xhr, formData) {
                var i = 1;
                var key;

                while ($element.data('target-param-' + i) != undefined) {
                    key = $element.data('target-param-' + i);
                    var field = $(key);
                    formData.append(field.attr('name'), field.val());

                    i++;
                }
            }
        };

        var mergedOptions = $.extend(defaultOptions, option);
        $element.dropzone(mergedOptions);

    };
})(jQuery);



