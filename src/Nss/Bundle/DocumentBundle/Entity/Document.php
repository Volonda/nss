<?php

namespace Nss\Bundle\DocumentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Document
 *
 * @ORM\Table(name="documents", schema="doc")
 * @ORM\Entity
 */
class Document
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean", nullable=true)
     */
    private $required;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_original", type="boolean", nullable=true)
     */
    private $isOriginal = false;

    /**
     * @var Template
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\Template")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * @var DocumentBundle
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\DocumentBundle", inversedBy="documents")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundle_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $bundle;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\DocumentBundle\Entity\DocumentVersion", mappedBy="document", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $versions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set required
     *
     * @param boolean $required
     *
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }

    /**
     * Get required
     *
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set isOriginal
     *
     * @param boolean $isOriginal
     */
    public function setIsOriginal($isOriginal)
    {
        $this->isOriginal = $isOriginal;
    }

    /**
     * Get isOriginal
     *
     * @return boolean
     */
    public function getIsOriginal()
    {
        return $this->isOriginal;
    }

    /**
     * Set template
     *
     * @param DocumentTemplates $template
     */
    public function setTemplate(Template $template = null)
    {
        $this->template = $template;
    }

    /**
     * Get template
     *
     * @return Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set bundle
     *
     * @param DocumentBundle $bundle
     */
    public function setBundle(DocumentBundle $bundle)
    {
        $this->bundle = $bundle;
    }

    /**
     * Get bundle
     *
     * @return DocumentBundle
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * Add documentVersion
     *
     * @param DocumentVersion $version
     *
     */
    public function addVersion(DocumentVersion $version)
    {
        $this->versions->add($version);
    }

    /**
     * Remove documentVersion
     *
     * @param DocumentVersion $version
     */
    public function removeVersion(DocumentVersion $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get version
     *
     * @return Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
