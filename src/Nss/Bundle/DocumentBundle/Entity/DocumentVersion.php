<?php

namespace Nss\Bundle\DocumentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nss\Bundle\AppBundle\Entity\Traits\TimestampableEntityString;
use Gedmo\Mapping\Annotation as Gedmo;
use Nss\Bundle\FileBundle\Entity\File;

/**
 * DocumentVersion
 *
 * @ORM\Table(name="versions", schema="doc")
 * @ORM\Entity
 */
class DocumentVersion
{
    use TimestampableEntityString;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Document
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\Document", inversedBy="versions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="document_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $document;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Nss\Bundle\FileBundle\Entity\File", cascade={"persist"})
     * @ORM\JoinTable(name="doc.version_files",
     *      joinColumns={@ORM\JoinColumn(name="version_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $files;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * Set document
     *
     * @param Document $document
     */
    public function setDocument(Document $document)
    {
        $this->document = $document;
    }

    /**
     * @return Collection
     */
    public function getFiles() : Collection
    {
        return $this->files;
    }

    /**
     * @param Collection $files
     */
    public function setFiles(Collection $files)
    {
        $this->files = $files;
    }

    public function addFile(File $file)
    {
        $this->files->add($file);
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(?string $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }

}