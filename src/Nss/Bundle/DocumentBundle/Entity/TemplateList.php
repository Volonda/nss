<?php

namespace Nss\Bundle\DocumentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Nss\Bundle\AppBundle\Validator\Constraints as AppAssert;

/**
 * TemplateList
 *
 * @ORM\Table(name="template_lists", schema="doc")
 * @ORM\Entity
 */
class TemplateList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull()
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @Assert\Regex(pattern="#^[\w]+$#")
     * @Assert\NotNull()
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var Collection
     * @AppAssert\UniquePropertyCollection(property="template.id", message="Один и тот же документ выбран несколько раз")
     * @ORM\OneToMany(targetEntity="Nss\Bundle\DocumentBundle\Entity\TemplateInTemplateList", mappedBy="templateList", cascade={"persist"})
     */
    private $templateInTemplateLists;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->modified = new \DateTime('now');
        $this->templateInTemplateLists = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @return Collection
     */
    public function getTemplateInTemplateLists() : Collection
    {
        return $this->templateInTemplateLists;
    }

    /**
     * @param Collection $templateInTemplateLists
     */
    public function setTemplateInTemplateLists(Collection $templateInTemplateLists)
    {
        $templateList = $this;

        $templateInTemplateLists->forAll(function ($num, TemplateInTemplateList $templateInTemplateList) use ($templateList) {

            if (!$templateInTemplateList->getTemplateList()) {
                $templateInTemplateList->setTemplateList($templateList);
            }

            return true;
        });
        $this->templateInTemplateLists = $templateInTemplateLists;
    }

    public function addTemplateInTemplateList(TemplateInTemplateList $templateInTemplateList)
    {
        $templateInTemplateList->setTemplateList($this);
        $this->templateInTemplateLists->add($templateInTemplateList);
    }

    public function __toString()
    {
        return ($this->name)?$this->name: "Документ";
    }
}