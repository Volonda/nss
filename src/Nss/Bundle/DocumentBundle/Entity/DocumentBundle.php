<?php

namespace Nss\Bundle\DocumentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nss\Bundle\OrderBundle\Entity\Order;

/**
 * DocumentBundle
 *
 * @ORM\Table(name="bundles", schema="doc")
 * @ORM\Entity
 */
class DocumentBundle
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\DocumentBundle\Entity\Document", mappedBy="bundle", cascade={"persist"})
     */
    private $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add document
     *
     * @param Document $document
     */
    public function addDocument(Document $document)
    {
        $this->documents[] = $document;
    }

    /**
     * Remove document
     *
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return Collection|Documents[]
     */
    public function getDocuments()
    {
        return $this->documents;
    }

}
