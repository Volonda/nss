<?php

namespace Nss\Bundle\DocumentBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Template
 *
 * @ORM\Table(name="templates", schema="doc")
 * @ORM\Entity
 */
class Template
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @Assert\Regex(pattern="#^[\w]+$#")
     * @Assert\NotNull()
     * @ORM\Column(name="code", type="string", length=30, nullable=false)
     */
    private $code;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\DocumentBundle\Entity\TemplateInTemplateList", mappedBy="template")
     */
    private $templateInTemplateList;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return Collection
     */
    public function getTemplateInTemplateList(): Collection
    {
        return $this->templateInTemplateList;
    }

    /**
     * @param Collection $templateInTemplateList
     */
    public function setTemplateInTemplateList(Collection $templateInTemplateList)
    {
        $this->templateInTemplateList = $templateInTemplateList;
    }

    public function __toString()
    {
        return $this->name;
    }
}