<?php
namespace Nss\Bundle\DocumentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TemplateListTemplates
 *
 * @ORM\Entity
 * @ORM\Table(name="template_in_template_list", schema="doc")
 */
class TemplateInTemplateList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean", nullable=true)
     */
    private $required = true;

    /**
     * @var Template
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\Template", inversedBy="templateInTemplateList")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * @var TemplateList
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\TemplateList", inversedBy="templateInTemplateLists")
     * @ORM\JoinColumn(name="template_list_id", referencedColumnName="id")
     */
    private $templateList;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set required
     *
     * @param boolean $required
     *
     * @return DocsInList
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }

    /**
     * Get required
     *
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @return TemplateList
     */
    public function getTemplateList(): ?TemplateList
    {
        return $this->templateList;
    }

    /**
     * @param TemplateList $templateList
     */
    public function setTemplateList(TemplateList $templateList)
    {
        $this->templateList = $templateList;
    }

    /**
     * @return Template
     */
    public function getTemplate() : ?Template
    {
        return $this->template;
    }

    /**
     * @param Template $template
     */
    public function setTemplate(Template $template)
    {
        $this->template = $template;
    }
}
