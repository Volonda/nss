<?php
namespace Nss\Bundle\DocumentBundle\Manager;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\DocumentBundle\Entity\DocumentVersion;

class VersionManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param DocumentVersion $version
     */
    public function delete(DocumentVersion $version)
    {
        $this->em->remove($version);
    }
}