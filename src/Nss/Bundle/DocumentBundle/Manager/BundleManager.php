<?php
namespace Nss\Bundle\DocumentBundle\Manager;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\DocumentBundle\Entity\Document;
use Nss\Bundle\DocumentBundle\Entity\DocumentBundle;
use Nss\Bundle\DocumentBundle\Entity\TemplateInTemplateList;
use Nss\Bundle\DocumentBundle\Entity\TemplateList;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\OrderBundle\Entity\Order;

class BundleManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Order $order
     * @param SroOrderModel $orderModel
     */
    public function createForOrder(Order $order, SroOrderModel $orderModel)
    {
        // input docs
        $templateList = $orderModel->getActualInputDocumentTemplateList();
        if (
            $templateList &&
            $templateList->getTemplateInTemplateLists()->count()
        ) {
            $bundle = $this->createBundleForTemplateList($templateList);
            $order->setInputDocumentBundle($bundle);
        }

        // output docs
        $templateList = $orderModel->getActualOutputDocumentTemplateList();
        if (
            $templateList &&
            $templateList->getTemplateInTemplateLists()->count()
        ) {
            $bundle = $this->createBundleForTemplateList($templateList);
            $order->setOutputDocumentBundle($bundle);
        }
    }

    /**
     * @param TemplateList $templateList
     * @return DocumentBundle
    */
    private function createBundleForTemplateList(TemplateList $templateList) : DocumentBundle
    {
        $bundle = new DocumentBundle();
        /** @var TemplateInTemplateList $templateInTemplateList*/
        foreach ($templateList->getTemplateInTemplateLists() as $templateInTemplateList) {
            $template = $templateInTemplateList->getTemplate();
            $document = new Document();
            $document->setTemplate($template);
            $document->setRequired($templateInTemplateList->getRequired());

            $document->setBundle($bundle);
            $bundle->addDocument($document);

            $this->em->persist($document);
        }

        $this->em->persist($bundle);

        return $bundle;
    }
}