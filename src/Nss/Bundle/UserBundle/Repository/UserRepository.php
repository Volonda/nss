<?php
namespace Nss\Bundle\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nss\Bundle\AppBundle\Exception\OperatorGroupNotFoundException;
use Nss\Bundle\UserBundle\Entity\Group;
use Nss\Bundle\UserBundle\Entity\User;

class UserRepository extends EntityRepository
{
    const OPERATOR_GROUP_ID = 2; // TODO Перенести в конфигв БД

    /**
     * @throws OperatorGroupNotFoundException
     * @return array|User[]
     */
    public function findAllManagers()
    {
        $dql = <<<DQL
SELECT 
  u, g
FROM 
  NssUserBundle:User u
  LEFT JOIN u.groups g
  WHERE ?1 MEMBER OF u.groups
DQL;

        return $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter(1, $this->getOperatorGroup())
            ->getResult();
    }

    /**
     * @return Group
     * @throws OperatorGroupNotFoundException
     */
    public function getOperatorGroup()
    {
        $group =  $this->getEntityManager()->find(Group::class, self::OPERATOR_GROUP_ID);

        if (!$group) {
            throw new OperatorGroupNotFoundException();
        }

        return $group;
    }
}