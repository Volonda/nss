<?php
namespace Nss\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ITSymfony\Bundle\ActionRolePermissionBundle\Model\Action as BaseAction;

/**
 * @ORM\Entity()
 * @ORM\Table(name="actions", schema="nss_user")
 */
class Action extends BaseAction
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_workflow_list", type="boolean")
     *
     */
    private $workflowList;

    public function __construct()
    {
        $this->workflowList = false;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isWorkflowList(): bool
    {
        return $this->workflowList;
    }

    /**
     * @param bool $workflowList
     */
    public function setWorkflowList(bool $workflowList)
    {
        $this->workflowList = $workflowList;
    }

}