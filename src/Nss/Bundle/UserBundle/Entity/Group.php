<?php
namespace Nss\Bundle\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ITSymfony\Bundle\ActionRolePermissionBundle\Model\ActionInterface;
use ITSymfony\Bundle\ActionRolePermissionBundle\Model\Action as BaseAction ;

/**
 * @ORM\Entity()
 * @ORM\Table(name="groups", schema="nss_user")
 */
class Group extends \FOS\UserBundle\Model\Group implements ActionInterface
{

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Nss\Bundle\UserBundle\Entity\Action", cascade={"persist"})
     * @ORM\JoinTable(name="action_groups", schema="nss_user",
     *      joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="action_id", referencedColumnName="id")}
     * )
     */
    private $actions;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * Group constructor.
     * @param string $name
     * @param array $roles
     */
    public function __construct($name='', array $roles = array())
    {
        parent::__construct($name, $roles);
        $this->actions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Collection
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    /**
     * @param Action[] $actions
     */
    public function setActions(array $actions)
    {
        $this->actions = $actions;
    }

    /**
     * @param BaseAction $action
     * @return boolean
    */
    public function hasAction(BaseAction $action) : bool
    {
        return $this->actions->contains($action);
    }

    /**
     * @var Action $action
     */
    public function addAction(Action $action)
    {
        $this->actions->add($action);
    }

    /**
     * @var Action $action
     */
    public function removeAction(Action $action)
    {
        $this->actions->removeElement($action);
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }
}