<?php
namespace Nss\Bundle\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nss\Bundle\SroBundle\Entity\Sro;

/**
 * @ORM\Entity(repositoryClass="Nss\Bundle\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="users",schema="nss_user")
 *
 */
class User extends \FOS\UserBundle\Model\User
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var Sro
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\SroBundle\Entity\Sro", inversedBy="users")
     * @ORM\JoinColumn(name="sro_id", referencedColumnName="id")
     */
    private $sro;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\ContactBundle\Entity\Contact", mappedBy="owner")
     */
    private $contacts;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Nss\Bundle\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="nss_user.user_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $groups;

    public function __construct()
    {
        $this->groups = new ArrayCollection();

        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Sro
     */
    public function getSro(): ?Sro
    {
        return $this->sro;
    }

    /**
     * @param Sro $sro
     */
    public function setSro(?Sro $sro)
    {
        $this->sro = $sro;
    }

    /**
     * @return Collection
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    /**
     * @param Collection $contacts
     */
    public function setContacts(Collection $contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * @return Collection
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * @param Collection $groups
     */
    public function setGroups(Collection $groups)
    {
        $this->groups = $groups;
    }

    public function __toString()
    {
        return $this->username;
    }
}