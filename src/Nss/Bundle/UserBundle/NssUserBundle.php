<?php

namespace Nss\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NssUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
