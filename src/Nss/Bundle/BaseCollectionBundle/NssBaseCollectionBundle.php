<?php

namespace Nss\Bundle\BaseCollectionBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NssBaseCollectionBundle extends Bundle
{
    public function getParent()
    {
        return 'ITSymfonyBaseCollectionBundle';
    }
}
