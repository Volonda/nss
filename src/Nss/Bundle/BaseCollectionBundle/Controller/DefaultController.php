<?php

namespace Nss\Bundle\BaseCollectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NssBaseCollectionBundle:Default:index.html.twig');
    }
}
