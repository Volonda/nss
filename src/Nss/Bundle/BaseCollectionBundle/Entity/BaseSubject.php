<?php
namespace Nss\Bundle\BaseCollectionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BaseSubject
 *
 * @ORM\Entity
 * @ORM\Table(name="subjects", schema="base")
 */
class BaseSubject
{
    /**
     * Заголовок
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    private $name;

    /**
     * Код
     * @var string
     *
     * @ORM\Column(name="code", type="text", nullable=false)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Регионы
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="BaseFederalDistrict", inversedBy="subject")
     * @ORM\JoinColumn(name="federal_district_id", referencedColumnName="id")
     */
    private $federalDistrict;

    /**
     * Флаг иные территории
     * @var boolean
     *
     * @ORM\Column(name="is_foreign", type="boolean", nullable=false)
     */
    private $isForeign;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFederalDistrict()
    {
        return $this->federalDistrict;
    }

    /**
     * @param mixed $federalDistrict
     */
    public function setFederalDistrict($federalDistrict)
    {
        $this->federalDistrict = $federalDistrict;
    }

    /**
     * @return boolean
     */
    public function getIsForeign()
    {
        return $this->isForeign;
    }

    /**
     * @param boolean $isForeign
     */
    public function setIsForeign($isForeign)
    {
        $this->isForeign = $isForeign;
    }

    public function getLabelName()
    {
        return $this->name . ' (' . $this->code . ')';
    }
    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
}

