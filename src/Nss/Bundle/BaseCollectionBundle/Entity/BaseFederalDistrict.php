<?php

namespace Nss\Bundle\BaseCollectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * BaseFederalDistricts
 *
 * @ORM\Entity
 * @ORM\Table(name="federal_districts", schema="base")
 */
class BaseFederalDistrict
{
    /**
     * Заголовок
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    private $name;

    /**
     * Код
     * @var string
     *
     * @ORM\Column(name="code", type="text", nullable=false)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Субъекты
     * @var BaseSubject
     *
     * @ORM\OneToMany(targetEntity="BaseSubject", mappedBy="federalDistrict")
     */
    private $subject;

    /**
     * Флаг иные территории
     * @var boolean
     *
     * @ORM\Column(name="is_foreign", type="boolean", nullable=false)
     */
    private $isForeign;

    public function __construct()
    {
        $this->subject = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return BaseSubject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param BaseSubject $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return boolean
     */
    public function getIsForeign()
    {
        return $this->isForeign;
    }

    /**
     * @param boolean $isForeign
     */
    public function setIsForeign($isForeign)
    {
        $this->isForeign = $isForeign;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

}

