<?php

namespace Nss\Bundle\ContactBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nss\Bundle\UserBundle\Entity\User;

class ContactRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param $inn
     * @return array
    */
    public function getByUserAndInn(User $user, $inn) : array
    {
        return $this->createQueryBuilder('c')
            ->join('c.organization','o')
            ->where('o.inn = :_inn')
            ->andWhere('c.owner = :_owner')
            ->setParameter('_owner', $user->getId())
            ->setParameter('_inn', $inn)
            ->getQuery()
            ->getResult()
        ;
    }
}
