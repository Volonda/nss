<?php
namespace Nss\Bundle\ContactBundle\Form\Type;

use Nss\Bundle\ContactBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ContactType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('fio', TextType::class, [
            'label' => 'ФИО'
        ])->add('email', EmailType::class, [
            'label' => 'email',
            'required' => false
        ])->add('phone', TextType::class, [
            'label' => 'Телефон',
            'required' => false
        ])->add('description', TextareaType::class, [
            'label' => 'Комментарий',
            'required' => false
        ])->add('isAgent', CheckboxType::class, [
            'label' => 'Агент',
            'required' => false
        ])->add('position', TextType::class, [
            'label' => 'Должность',
            'required' => false
        ])->add('submit', SubmitType::class, [
                'label' => 'FORM_SUBMIT_SEND'
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'label' => false
        ]);
    }
}