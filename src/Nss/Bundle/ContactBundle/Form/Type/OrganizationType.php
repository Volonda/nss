<?php
namespace Nss\Bundle\ContactBundle\Form\Type;

use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\OrganizationBundle\Manager\OrganizationManager;
use Nss\Bundle\OrganizationBundle\Resolver\TypeResolver;
use Nss\Bundle\OrganizationBundle\Validator\Constraints\InnValid;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrganizationType extends AbstractType {

    /** @var OrganizationManager*/
    private $organizationManager;

    /** @var TypeResolver*/
    private $resolver;

    /**
     * @param OrganizationManager $organizationManager
     * @param TypeResolver $resolver
    */
    public function __construct(
        OrganizationManager $organizationManager,
        TypeResolver $resolver
    ) {
        $this->organizationManager = $organizationManager;
        $this->resolver = $resolver;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $manager = $this->organizationManager;
        $resolver = $this->resolver;

        $builder->add('inn', TextType::class, [
            'label' => 'ИНН Организации',
            'constraints' => [
                new InnValid()
            ]
        ])
        ->add('fullName', TextType::class, [
            'label' => 'Полное наименование'
        ])->add('shortName', TextType::class, [
            'label' => 'Краткое наименование'
        ])
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) use ($manager, $resolver) {
            $inn = $event->getData()['inn'];
            if ($resolver->getType($inn)) {
                $organization = $manager->getByInnOrCreate($inn);
                $event->getForm()->setData($organization);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organization::class,
            'label' => false
        ]);
    }
}