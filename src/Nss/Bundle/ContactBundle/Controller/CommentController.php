<?php

namespace Nss\Bundle\ContactBundle\Controller;

use Nss\Bundle\CommentBundle\Entity\ContactComment;
use Nss\Bundle\CommentBundle\Form\Type\ContactCommentType;
use Nss\Bundle\CommentBundle\Manager\CommentManager;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/comments")
 * @Security("is_granted('allow', 'action:contact_my')")
*/
class CommentController extends Controller
{

    /**
     * @Route("/create/{id}", name="contact_comment_create")
     * @param Contact $contact
     * @param Request $request
     * @ParamConverter("contact", class="NssContactBundle:Contact")
     * @return RedirectResponse
     */
    public function create(Request $request, Contact $contact)
    {
        /** @var CommentManager $commentManager*/
        $commentManager = $this->get('comment.manager.comment_manager');

        $form = $this->createForm(ContactCommentType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $commentManager->createContactComment($form->getData(), $contact, $this->getUser());

            $this->addFlash('notice', 'EDIT_SUCCESS');

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact_view', [
                'id' => $contact->getId()
            ]);
        }
    }

    /**
     * @Route("/delete/{id}", name="contact_comment_delete")
     * @ParamConverter("comment", class="NssCommentBundle:ContactComment")
     * @param ContactComment $comment
     * @return RedirectResponse
     */
    public function delete(ContactComment $comment) : RedirectResponse
    {
        /**@var CommentManager $commentManager*/
        $commentManager = $this->get('comment.manager.comment_manager');
        $commentManager->delete($comment);

        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('notice', 'EDIT_SUCCESS');

        return $this->redirectToRoute('contact_view',  [
            'id' => $comment->getContact()->getId()
        ]);
    }
}