<?php

namespace Nss\Bundle\ContactBundle\Controller;

use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\ContactBundle\Form\Type\OrganizationContactType;
use Nss\Bundle\ContactBundle\Manager\ContactManager;
use Nss\Bundle\ContactBundle\Manager\OrderManager;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class IndexController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/", name="contact_my")
     * @Template()
     * @param Request $request
     * @Method(methods={"GET"})
     * @Security("is_granted('allow', 'action:contact_my')")
     * @return array
     */
    public function myAction(Request $request) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Клиенты');

        /** @var ContactManager $manager*/
        $manager = $this->get('contact.manager.contact_manager');

        /* @var $filter Filter */
        $filter = $this->get('it_symfony.data_table.filter')->getFilter($request, $manager->getFilters());

        $pagination = $manager->getOrganizationContactPaginate(
            $this->getUser(),
            $request->query->get('page', 1),
            self::PAGE_LIMIT,
            $filter
        );

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @Route("/create", name="contact_create")
     * @Route("/edit/{id}", name="contact_edit", requirements={"id" : "\d+"})
     * @Template()
     * @param Request $request
     * @param Contact $contact
     * @Security("is_granted('allow', 'action:contact_my')")
     * @ParamConverter("contact", class="NssContactBundle:Contact", options={"id":"id"})
     * @return array|RedirectResponse
     */
    public function create(Request $request, Contact $contact = null)
    {
        /** @var ContactManager $manager*/
        $manager = $this->get('contact.manager.contact_manager');

        $organization = null;
        if ($orgid = $request->get('orgid')) {
            $organization = $this->getDoctrine()->getManager()->getRepository(Organization::class)->find($orgid);
        }

        $form = $this->createForm(OrganizationContactType::class, $contact, [
            'attr' => [
                'id' => 'create-contact-form'
            ],
            'organization' => $organization
        ]);

        $form->handleRequest($request);

        if (
            $form->isSubmitted()
            && $form->isValid()
        ) {

            $manager->create($form->getData(), $this->getUser());

            $this->addFlash('notice', 'EDIT_SUCCESS');

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact_my');
        }

        return [
            'form' => $form->createView()
        ];
    }


    /**
     * @Route("/{id}/add", name="contact_add")
     * @Template()
     * @param Request $request
     * @param Contact $contact
     * @Security("is_granted('allow', 'action:contact_my')")
     * @ParamConverter("organization", class="NssOrganizationBundle:Organization", options={"id":"id"})
     * @return array|RedirectResponse
     */
    public function add(Request $request, Organization $organization)
    {
        /** @var ContactManager $manager*/
        $manager = $this->get('contact.manager.contact_manager');

        $form = $this->createForm(OrganizationContactType::class, $contact, [
            'attr' => [
                'id' => 'create-contact-form'
            ]
        ]);

        $form->handleRequest($request);

        if (
            $form->isSubmitted()
            && $form->isValid()
        ) {

            $manager->create($form->getData(), $this->getUser());

            $this->addFlash('notice', 'EDIT_SUCCESS');

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact_my');
        }

        return [
            'form' => $form->createView()
        ];
    }



    /**
     * @Route("/view/{id}", name="contact_view")
     * @Template()
     * @param Contact $contact
     * @Method(methods={"GET"})
     * @Security("is_granted('allow', 'action:contact_my')")
     * @return array
     */
    public function view(Request $request, Contact $contact) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Клиенты','organization_my');
        $breadcrumbs->addRouteItem($contact->getOrganization()->getInn(),'organization_view', ['id' => $contact->getOrganization()->getId()]);
        $breadcrumbs->addRouteItem('Контакты организации','organization_contact',['id' => $contact->getOrganization()->getId()]);
        $breadcrumbs->addItem($contact->getFio());

        /** @var OrderManager $orderManager*/
        $orderManager = $this->get('contact.manager.order_manager');

         $pagination = $orderManager->getPaginated(
             $contact,
             $request->query->get('page', 1),
             self::PAGE_LIMIT
         );

        return [
            'pagination' => $pagination,
            'contact' => $contact
        ];
    }
}