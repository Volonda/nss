<?php

namespace Nss\Bundle\ContactBundle\Controller;

use Nss\Bundle\CommentBundle\Form\Type\ContactCommentType;
use Nss\Bundle\CommentBundle\Manager\CommentManager;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\OrganizationBundle\Repository\OrganizationRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ITSymfony\Bundle\SeldonBundle\Api\SeldonApi;
use Monolog\Logger;
use Nss\Bundle\ContactBundle\Repository\ContactRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/rest")
 */
class RestController extends Controller
{
    /**
     * @Route("/comment/{id}", name="contact_rest_comment")
     * @Template()
     * @param Contact $contact
     * @Method(methods={"GET"})
     * @ParamConverter("contact", class="NssContactBundle:Contact")
     * @Security("is_granted('allow', 'action:contact_my')")
     * @return array
     */
    public function comment(Contact $contact) : array
    {
        /** @var CommentManager $commentManager*/
        $commentManager = $this->get('comment.manager.comment_manager');

        $form = $this->createForm(ContactCommentType::class, null, [
            'action' =>  $this->generateUrl('contact_comment_create', ['id' => $contact->getId()])
        ]);

        $comments = $commentManager->getContactComment($contact, $this->getUser());

        return [
            'comments' => $comments,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/check-inn-toastr", name="contact_rest_check_inn_toastr")
     * @Method(methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function checkInnToastr(Request $request) : JsonResponse
    {
        $responseData = [
            'toastr' => null,
            'result' => null,
            'data' => [
                'fullName' => '',
                'shortName' => ''
            ]
        ];

        /** @var $api SeldonApi*/
        $api = $this->get('it_symfony_seldon.api.seldon_api');
        /** @var ContactRepository $contactRepo*/
        $contactRepo = $this->getDoctrine()->getRepository(Contact::class);

        /** @var OrganizationRepository $organizationRepo*/
        $organizationRepo = $this->getDoctrine()->getRepository(Organization::class);

        $inn = $request->get('inn', false);


        if ($inn){
            try {
                //Поиск в контактах
                $contacts = $contactRepo->getByUserAndInn($this->getUser(), $inn);
                if (sizeof($contacts)) {
                    $organization = $organizationRepo->finByOneInn($inn);

                    $responseData['toastr'] = 'toastr.success("ПО ИНН <strong>' . $inn . '</strong> найдено контактов:<strong>' . sizeof($contacts) . '</strong>")';
                    $responseData['result'] = true;
                    $responseData['data']['shortName'] = $organization->getShortName();
                    $responseData['data']['fullName'] = $organization->getFullName();

                    return new JsonResponse($responseData);
                }

                //Поиск в Seldon
                $card = $api->getDataByInnOrNull($inn);

                if ($card) {

                    if ( property_exists($card, 'company_card')) {
                        $orgname = $card->company_card->basic->shortName;
                        $responseData['data']['shortName'] = $card->company_card->basic->shortName;
                        $responseData['data']['fullName'] = $card->company_card->basic->fullName;
                    } else {
                        $orgname = $card->basic->person->fullName;
                        $responseData['data']['fullName'] = $card->basic->person->fullName;
                    }

                    $responseData['toastr'] = 'toastr.success("ПО ИНН <strong>' . $inn . '</strong> найдена организация <strong>' . addslashes($orgname) . '</strong>")';
                    $responseData['result'] = true;

                } else {
                    $responseData['toastr'] = 'toastr.warning("Организация не найдена")';
                    $responseData['result'] = false;
                }

            } catch (\Exception $e) {
                /** @var Logger $logger*/
                $logger = $this->get('monolog.logger.rest');

                $logger->addCritical($e->getMessage());
                $responseData['toastr'] = 'toastr.error("Возникла непредвиденная ошибка")';
                $responseData['result'] = false;
            }

        }

        return new JsonResponse($responseData);
    }
}