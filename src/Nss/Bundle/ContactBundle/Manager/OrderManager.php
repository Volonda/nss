<?php

namespace Nss\Bundle\ContactBundle\Manager;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\UserBundle\Entity\User;

class OrderManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /** @var Paginator*/
    private $paginator;


    public function __construct(EntityManager $em, Paginator $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getPaginated(Contact $contact, int $page, int $limit) : PaginationInterface
    {
        $qb = $this->em->createQueryBuilder()
            ->select('o')
            ->from(Order::class, 'o')
            ->join('o.sro',' s')
            ->join('o.status',' os')
            ->join('o.contact','c')
            ->where('c = :contact')
            ->setParameter('contact', $contact)
            ;

        return $this->paginator->paginate($qb, $page, $limit,
            ['defaultSortFieldName' => 'o.updatedAt', 'defaultSortDirection' => 'DESC']
        );
    }

}
