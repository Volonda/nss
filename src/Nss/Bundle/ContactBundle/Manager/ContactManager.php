<?php
namespace Nss\Bundle\ContactBundle\Manager;

use Doctrine\ORM\EntityManager;
use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\UserBundle\Entity\User;

class ContactManager
{
    /**
     * @var EntityManager
    */
    private $em;

    /** @var Paginator*/
    private $paginator;

    /** @var array*/
    private $filters = [
        'id' => 'int',
        'c.fio' => 'like',
        'o.inn' => 'like',
        'c.phone' => 'like',
        'c.email' => 'like',
        'o.fullName' => 'like'
    ];

    public function __construct(EntityManager $em, Paginator $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getFilters()
    {
        $filters = $this->filters;

        return $filters;
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @param Filter $filter
     * @return PaginationInterface
    */
    public function getOrganizationContactPaginate(User $user, int $page, int $limit, Filter $filter) : PaginationInterface
    {
        $qb = $this->em->createQueryBuilder()
            ->select('c,o,ord,w')
            ->from(Contact::class,'c')
            ->join('c.organization', 'o')
            ->leftJoin('c.orders','ord')
            ->leftJoin('ord.workflow','w')
            ->where('c.owner = :_user')
            ->setParameter('_user', $user)
        ;

        $filter->addFiltersToQB($qb);

        return $this->paginator->paginate($qb, $page, $limit,
            ['defaultSortFieldName' => 'c.id', 'defaultSortDirection' => 'ASC']
        );
    }

    public function getByOrganization(User $user, Organization $organization, $type)
    {
        $query = $this->em->createQueryBuilder()
            ->select('c')
            ->from($type,'c')
            ->where('c.owner = :_owner')
            ->setParameter('_owner', $user)
            ->andWhere('c.organization = :_organization')
            ->setParameter('_organization', $organization)
            ->getQuery()
        ;

        return $query->getResult();
    }

    public function createForOrganization(Contact $contact, User $owner, Organization $organization)
    {
        $contact->setOrganization($organization);
        $contact->setOwner($owner);

        $this->em->persist($contact);
    }

    public function create(Contact $contact, User $owner)
    {
        $contact->setOwner($owner);
        $this->em->persist($contact);
    }
}
