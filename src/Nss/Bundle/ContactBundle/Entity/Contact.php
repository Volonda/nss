<?php
namespace Nss\Bundle\ContactBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Nss\Bundle\ContactBundle\Repository\ContactRepository")
 * @ORM\Table(name="contacts", schema="nss_user")
 */
class Contact
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="text", nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="text", nullable=false)
     * @Assert\NotNull(groups={"CreateContact"})
     */
    private $fio;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrganizationBundle\Entity\Organization")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    private $organization;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\UserBundle\Entity\User", inversedBy="contacts")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\OrderBundle\Entity\Order", mappedBy="contact")
     */

    private $orders;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_agent", type="boolean", nullable=false)
     */
    private $isAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="text", nullable=false)
     */
    private $position;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\CommentBundle\Entity\ContactComment", mappedBy="contact")
     */
    private $comments;


    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->isAgent = false;
        $this->comments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFio(): ?string
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     */
    public function setFio(string $fio)
    {
        $this->fio = $fio;
    }

    /**
     * @return Organization
     */
    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return Collection
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @param Collection $orders
     */
    public function setOrders(Collection $orders)
    {
        $this->orders = $orders;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isIsAgent(): bool
    {
        return $this->isAgent;
    }

    /**
     * @param bool $isAgent
     */
    public function setIsAgent(bool $isAgent)
    {
        $this->isAgent = $isAgent;
    }

    /**
     * @return string
     */
    public function getPosition(): ?string
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition(?string $position)
    {
        $this->position = $position;
    }

    public function labelName()
    {
        return $this->fio . (!empty($this->position)? '('. $this->position . ')':'');
    }

    public function getLastOrders($limit = 1)
    {
        $criteria = Criteria::create();
        $criteria->orderBy(['createdAt' => 'DESC']);
        $criteria->setMaxResults($limit);

        return $this->orders->matching($criteria);
    }

    /**
     * @return Collection
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @param Collection $comments
     */
    public function setComments(Collection $comments)
    {
        $this->comments = $comments;
    }
}