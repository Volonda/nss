<?php
namespace Nss\Bundle\AdminBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait SortableEntity
{
    /**
     * @var integer
     * @ORM\Column(name="sort", type="integer", nullable=false)
     */
    private $sort;

    /**
     * @return int
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort)
    {
        $this->sort = $sort;
    }
}