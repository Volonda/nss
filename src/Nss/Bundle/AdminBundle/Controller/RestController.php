<?php

namespace Nss\Bundle\AdminBundle\Controller;

use Monolog\Logger;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel;
use Nss\Bundle\OrderBundle\Manager\OrderManager;
use Nss\Bundle\SroBundle\Entity\Sro;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/rest")
*/
class RestController extends Controller
{
    /**
     * @Route("/template-order-model/{template_id}/enable/{sro_id}",
     *     requirements={"sro_id":"\d+","template_id":"\d+"},
     *     name="admin_rest_enable_template_order_model"
     * )
     * @param Sro $sro
     * @param TemplateOrderModel $templateOrderModel
     * @ParamConverter("sro", class="NssSroBundle:Sro", options={"id":"sro_id"})
     * @ParamConverter("templateOrderModel", class="Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel", options={"id":"template_id"})
     * @Template()
     * @return JsonResponse
     */
    public function enableTemplateOrderModelAction(Sro $sro, TemplateOrderModel $templateOrderModel) : JsonResponse
    {
        /** @var Logger $logger*/
        $logger = $this->get('logger');

        $data = [
            'error' => 0
        ];

        try {
            /** @var OrderManager $manager */
            $manager = $this->get('order.manager.order_manager');

            $manager->createOrEnableSroOrderModel($sro, $templateOrderModel);

            $this->getDoctrine()->getManager()->flush();
        } catch (\Exception $e) {
            $data = [
                'error' => 1
            ];

            $logger->addCritical($e->getMessage());
        }

        $response = new JsonResponse($data);

        return $response;
    }

    /**
     * @Route("/template-order-model/{template_id}/disable/{sro_id}",
     *     requirements={"sro_id":"\d+","template_id":"\d+"},
     *     name="admin_rest_disable_template_order_model"
     * )
     * @param Sro $sro
     * @param TemplateOrderModel $templateOrderModel
     * @ParamConverter("sro", class="NssSroBundle:Sro", options={"id":"sro_id"})
     * @ParamConverter("templateOrderModel", class="Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel", options={"id":"template_id"})
     * @Template()
     * @return JsonResponse
     */
    public function disableTemplateOrderModelAction(Sro $sro, TemplateOrderModel $templateOrderModel) : JsonResponse
    {
        /** @var Logger $logger*/
        $logger = $this->get('logger');

        $data = [
            'error' => 0
        ];

        try {
            /** @var OrderManager $manager */
            $manager = $this->get('order.manager.order_manager');

            $manager->disableSroOrderModel($sro, $templateOrderModel);

            $this->getDoctrine()->getManager()->flush();
        } catch (\Exception $e) {
            $data = [
                'error' => 1
            ];

            $logger->addCritical($e->getMessage());
        }

        $response = new JsonResponse($data);

        return $response;
    }
}
