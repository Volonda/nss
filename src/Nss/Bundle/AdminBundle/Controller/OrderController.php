<?php

namespace Nss\Bundle\AdminBundle\Controller;

use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel;
use Nss\Bundle\SroBundle\Entity\Sro;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OrderController extends Controller
{
    /**
     * @Route("/sro-order-model", name="admin_sro_order_model")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $sroList = $em->getRepository(Sro::class)->findAll();
        $templateOrderModelRepo = $em->getRepository(TemplateOrderModel::class);
        $orderTypes = $em->getRepository(BaseOrderType::class)->findAll();

        return [
            'sroList' => $sroList,
            'templateOrderModelRepo' => $templateOrderModelRepo,
            'orderTypes' => $orderTypes,
            'sroOrderModelRepo' => $em->getRepository(SroOrderModel::class)
        ];
    }
}
