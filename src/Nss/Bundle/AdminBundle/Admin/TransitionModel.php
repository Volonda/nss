<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class TransitionModel extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('workflowModel.baseWorkflow.name', null, ['label' => 'Процесс'])
            ->addIdentifier('currentStatus.name', null, ['label' => 'Текущий статус'])
            ->addIdentifier('transitionStatus.name', null, ['label' => 'Статус перехода'])
            ->addIdentifier('groups', null, [
                'label' => 'Группы',
                'associated_property' => 'title'
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Переходы процессов')
                ->add('workflowModel','sonata_type_model', [
                    'property' => 'baseWorkflow.name',
                    'label' => 'Бизнес процесс'
                ])
                ->add('currentStatus','sonata_type_model', [
                    'property' => 'name',
                    'label' => 'Текущий статус'
                ])
                ->add('transitionStatus','sonata_type_model', [
                    'property' => 'name',
                    'label' => 'Статус перехода'
                ])
                ->add('groups', 'sonata_type_model', [
                    'label' => 'Группы',
                    'property' => 'title',
                    'multiple' => true,
                    'expanded' => true
                ])
            ->end();
    }

}