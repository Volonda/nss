<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class ServiceModelFormType extends AbstractAdmin
{


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, ['label' => 'Заголовок'])
            ->addIdentifier('formType.name', null, ['label' => 'Тип формы'])
            ->addIdentifier('formType.options', null, ['label' => 'Опции'])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('sro_services_list_head')

            ->add('title', 'text',[
                'label' => 'Заголовок'
            ])
            ->add('formType', 'sonata_type_model', array(
                'class' => 'Nss\Bundle\OrderBundle\Entity\BaseFormType',
                'property' => 'name',
                'label' => 'Тип формы'
            ))
            ->add('code', 'text',[
                'label' => 'Код'
            ])
            ->add('options', 'textarea',[
                'label' => 'Опции (в формате YML)',
                'required' => false
            ])
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }
}