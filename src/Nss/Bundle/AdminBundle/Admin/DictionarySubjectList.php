<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class DictionarySubjectList extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, ['label' => 'dictionary_subject_title'])
            ->addIdentifier('code', null, ['label' => 'dictionary_subject_code'])
            ->addIdentifier('federalDistrict.name', null, ['label' => 'dictionary_subject_district']);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('dictionary_subject_head')
            ->add('name', 'text', ['label' => 'dictionary_subject_title'])
            ->add('code', 'text', ['label' => 'dictionary_subject_code'])
            ->add('federalDistrict', 'sonata_type_model', array(
                'class' => 'Nss\Bundle\BaseCollectionBundle\Entity\BaseFederalDistrict',
                'property' => 'name',
                'label' => 'dictionary_subject_district'
            ))
            ->end();
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, ['label' => 'dictionary_subject_title'])
            ->add('code', null, ['label' => 'dictionary_subject_code'])
            ->add('federalDistrict', null, array('label' => 'dictionary_subject_district'), 'entity', array(
                'class'    => 'Nss\Bundle\BaseCollectionBundle\Entity\BaseFederalDistrict',
                'choice_label' => 'name',
            ));
    }

}
