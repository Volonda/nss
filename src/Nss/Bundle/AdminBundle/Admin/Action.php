<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class Action extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title',null, ['label' => 'Название'])
            ->addIdentifier('code',null, ['label' => 'Код'])
            ->addIdentifier('workflowList',null, ['label' => 'Отображать в бизнесс процессах'])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $roles = $container->getParameter('security.role_hierarchy.roles');
        $rolesChoices = [];

        foreach ( array_keys($roles) as $role) {
            $rolesChoices[$role] = $role;
        }

        $formMapper
            ->with('Действия')
            ->add('title', 'text', [
                'label' => 'Название'
            ])->add('workflowList', 'checkbox', [
                'label' => 'Отображать в бизнесс процессах',
                'required' => false
            ])->add('code', 'text', [
                'label' => 'Код'
            ])->add('roles', 'choice', [
                'label' => 'Роли',
                'choices' => $rolesChoices,
                'multiple' => true
            ])
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('code')
        ;
    }
}