<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class ConditionModel extends AbstractAdmin
{


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('value', null, ['label' => 'Значение для рассчета'])
            ->addIdentifier('name', null, ['label' => 'Значение для отображения'])
            ->addIdentifier('type.name', null, ['label' => 'Тип'])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Условие')
            ->add('type', 'sonata_type_model', array(
                'class' => 'Nss\Bundle\OrderBundle\Entity\BaseConditionType',
                'property' => 'name',
                'label' => 'sro_conditions_type'
            ))
            ->add('value', 'number', ['label' => 'Значение для рассчета', 'scale' => 2])
            ->add('name', 'text', ['label' => 'Значение для отображения'])
            ->end();
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('type', null, array('label' => 'sro_conditions_type'), 'entity', array(
                'class'    => 'Nss\Bundle\OrderBundle\Entity\BaseConditionType',
                'choice_label' => 'name',
            ))
        ;
    }


}