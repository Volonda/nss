<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class Sro extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit', 'create'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('registrationNumber', null, ['label' => 'Рег. номер СРО'])
            ->add('sroType.name', null, ['label' => 'Тип'])
            ->add('subject.federalDistrict.name', null, ['label' => 'Субъект'])
            ->add('_action', 'actions', [
                'actions' => [
                    'edit' => []
                ]
            ] )
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Карточка СРО')
            ->add('name', 'text')
            ->add('shortName', 'text', ['label' => 'Краткое наименование'])
            ->add('registrationNumber', 'text', ['label' => 'Рег. номер'])
            ->add('sroType', 'sonata_type_model', [
                'property' => 'name',
                'label' => 'Тип',
                'btn_add' => false
            ])
            ->add('subject', 'sonata_type_model', [
                'property' => 'name',
                'label' => 'Субъект',
                'btn_add' => false
            ])
            ->end();
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }
}