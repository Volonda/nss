<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModel;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModelFormType;
use Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SroOrderModel extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit', ''));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, ['label' => 'Наименование'])
            ->add('sro.name', null, ['label' => 'СРО'])
            ->add('typeName', null, ['label' => 'Тип'])
            ->add('actualInputDocumentTemplateListName', null, ['label' => 'Список входящих документов'])
            ->add('actualOutputDocumentTemplateListName', null, ['label' => 'Список исходящих документов'])
            ->add('rewrite', null, ['label' => 'Уникальные настройки'])
            ->add('enabled', null, ['label' => 'Доступность'])
            ->add('_action', 'actions', [
                'actions' => [
                    'edit' => []
                ]
            ] )
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Заявление')
                ->with('Заявление')
                ->add('template','sonata_type_model', [
                    'property' => 'title',
                    'btn_add' => false,
                    'label' => 'Шаблон'
                ])
                ->add('rewrite','checkbox', [
                    'label' => 'Использовать уникальные настройки заявления СРО',
                    'required' => false
                ])
                ->add('enabled','checkbox',[
                    'label' => 'Доступность',
                    'required' => false
                ])->end()
            ->end()
            ->tab('Услуги')
                ->with('Услуги')
                ->add('services', 'sonata_type_collection', [
                    'by_reference' => false,
                    'type_options' => [
                        'btn_add' => false
                    ],
                    'btn_add' => 'Добавить услугу',
                    'label' => 'Услуги',
                    'required' => false
                ],[
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position'
                ])->end()
            ->end()
            ->tab('Входящие документы')
                ->with('Входящие документы')
                ->add('inputDocumentTemplateList', 'sonata_type_model', [
                    'property' => 'name',
                    'label' => 'Список входящих документов',
                    'required' => false,
                    'placeholder' => 'Нет',
                    'btn_add' => false,
                ])->end()
            ->end()
            ->tab('Исходящие документы')
                ->with('Исходящие документы')
                ->add('outputDocumentTemplateList', 'sonata_type_model', [
                    'property' => 'name',
                    'label' => 'Список исходящих документов',
                    'required' => false,
                    'btn_add' => false,
                    'placeholder' => 'Нет'
                ])->end()
            ->end()

        ;
    }

    public function preUpdate($object)
    {
        if (!$object instanceof \Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel ) {
            throw new \Exception('object is not instanceof \Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel');
        }

        /** @var EntityManager $em */
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.default_entity_manager');

        /** @var ServiceModelFormType $formType*/
        $formType = $em->getRepository(ServiceModelFormType::class)->findOneBy(['code' => 'simple_select']);

        if (!$formType instanceof ServiceModelFormType) {
            throw new \Exception("formType not instanceof ServiceModelFormType");
        }

        $object->getServices()->forAll(function ($num, ServiceModel $service) use ($object, $formType) {
            if (!$service->getFormType()) {
                $service->setFormType($formType);
            }
            return true;
        });

        parent::prePersist($object);
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('template.title', null, ['label' => 'Наименование'])
            ->add('sro.name', null, ['label' => 'СРО'])
            ->add('rewrite', null, ['label' => 'Уникальные настройки'])
            ->add('enabled', null, ['label' => 'Доступность'])
        ;
    }
}