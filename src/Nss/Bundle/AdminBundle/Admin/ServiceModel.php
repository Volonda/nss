<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class ServiceModel extends AbstractAdmin
{


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('order.title', null, ['label' => 'Заявление'])
            ->addIdentifier('title', null, ['label' => 'Услуга'])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('service', 'sonata_type_model', [
                'class' => 'Nss\Bundle\OrderBundle\Entity\BaseService',
                'property' => 'name',
                'label' => 'Услуга',
                'btn_add' => false
            ])
            ->add('options', 'sonata_type_collection', [
                'by_reference' => false,
                'btn_add' => 'Добавить опцию',
                'label' => 'Опции услуги'
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'btn_add' => true,
                'sortable' => 'position'
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }
}