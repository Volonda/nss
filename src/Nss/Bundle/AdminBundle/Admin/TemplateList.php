<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TemplateList extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit', 'create'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'Название'])
            ->add('code', null, ['label' => 'Код'])
            ->add('_action', 'actions', [
                'actions' => [
                    'edit' => []
                ]
            ] )
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Список документов')
                ->with('Список документов')
                ->add('name', 'text')
                ->add('code', 'text')->end()
            ->end()
            ->tab('Документы')
                ->with('Документы')
                ->add('templateInTemplateLists','sonata_type_collection',
                    [
                        'by_reference' => false,
                        'type_options' => [
                            'btn_add' => false
                        ],
                        'btn_add' => 'Добавить документ',
                        'label' => 'Документыв списке',
                        'required' => false
                    ],[
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position'
                    ]
                )
                ->end()
        ;
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('code')
        ;
    }


}