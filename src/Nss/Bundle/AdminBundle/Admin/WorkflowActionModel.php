<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Nss\Bundle\UserBundle\Entity\Action;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class WorkflowActionModel extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('workflowModel.baseWorkflow.name', null, ['label' => 'Процесс'])
            ->addIdentifier('currentStatus.name', null, ['label' => 'Текущий статус'])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Действия')
                ->add('workflowModel','sonata_type_model', [
                    'property' => 'baseWorkflow.name',
                    'label' => 'Процесс'
                ])
                ->add('currentStatus','sonata_type_model', [
                    'property' => 'name',
                    'label' => 'Текущий статус',
                    'btn_add' => false,
                ])
                ->add('actions', null, [
                    'label' => 'Действия',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'expanded' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->where('a.workflowList = TRUE')
                            ;
                    },
                ])
            ->end();
    }
}