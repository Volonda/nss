<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class DictionaryDisrictsList extends AbstractAdmin
{


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, ['label' => 'dictionary_district_title'])
            ->addIdentifier('code', null, ['label' => 'dictionary_district_code']);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('dictionary_district_head')
            ->add('name', 'text', ['label' => 'dictionary_district_title'])
            ->add('code', 'text', ['label' => 'dictionary_district_code'])
            ->end();
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, ['label' => 'dictionary_district_title'])
            ->add('code', null, ['label' => 'dictionary_district_code']);
    }


}