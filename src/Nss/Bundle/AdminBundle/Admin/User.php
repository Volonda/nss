<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Nss\Bundle\UserBundle\Entity\Group;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class User extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit', 'create'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username', null, [
                'label' => 'Логин'
            ])
            ->add('groups', null, [
                'associated_property' => 'title',
                'label' => 'Группы'
            ])
            ->add('_action', 'actions', [
                'actions' => [
                    'edit' => []
                ]
            ] )
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Пользователи')
            ->add('username', null, [
                'label' => 'Логин'
            ])
            ->add('email', null, [
                'label' => 'email'
            ])
            ->add('plainPassword', 'text', [
                'label' => 'Пароль'
            ])
            ->add('enabled', null, [
                'required' => false,
                'label' => 'Активность'
            ])
            ->add('sro','sonata_type_model', [
                'class' => 'Nss\Bundle\SroBundle\Entity\Sro',
                'property' => 'name',
                'placeholder' => 'Выберите СРО',
                'required' => false,
                'btn_add' => false,
                'label' => 'Принадлежность к СРО'
            ])
            ->add('groups','sonata_type_model',
                [
                    'label' => 'Группы',
                    'class' => Group::class,
                    'property' => 'title',
                    'multiple' => true,
                    'btn_add' => false
                ])
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
        ;
    }

    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;
        $options['validation_groups'] = (!$this->getSubject() || is_null($this->getSubject()->getId())) ? ['Registration'] : ['Profile'];
        $options['validation_groups'][] = 'Default';

        $formBuilder = $this->getFormContractor()->getFormBuilder( $this->getUniqid(), $options);

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

}