<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class BaseOption extends AbstractAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit', 'create'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('code', null, ['label' => 'Код'])
            ->add('description', null, ['label' => 'Пояснение'])
            ->add('_action', 'actions', [
                'actions' => [
                    'edit' => []
                ]
            ] )
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Карточка опции')
            ->add('name', 'text')
            ->add('code', 'text', [
                'label' => 'Код'
            ])
            ->add('description', 'textarea', [
                'required' => false,
                'label' => 'Пояснение'
            ])
            ->end();
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }
}