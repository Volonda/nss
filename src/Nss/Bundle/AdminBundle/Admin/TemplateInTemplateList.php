<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TemplateInTemplateList extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Документ в списке')
            ->add('template', 'sonata_type_model', array(
                'property' => 'name',
                'label' => 'Документ',
                'btn_add' => false
            ))
            ->add('required', 'checkbox',array(
                'label' => 'Обязательность'
            ))
            ->end();
    }
}