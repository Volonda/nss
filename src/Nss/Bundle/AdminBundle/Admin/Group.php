<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Nss\Bundle\UserBundle\Entity\Action;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class Group extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title',null, ['label' => 'Название'])
            ->addIdentifier('name',null, ['label' => 'Код'])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $roles = $container->getParameter('security.role_hierarchy.roles');
        $rolesChoices = [];

        foreach ( array_keys($roles) as $role) {
            $rolesChoices[$role] = $role;
        }

        $formMapper
            ->with('Группы')
            ->add('title','text', [
                'label' => 'Название'
            ])
            ->add('name', 'text', [
                'label' => 'Код'
            ])->add('roles', 'choice', [
                'label'=> 'Роли',
                'choices' => $rolesChoices,
                'multiple' => true
            ])->add('actions', 'sonata_type_model', [
                'label' => 'Действия',
                'property' => 'title',
                'multiple' => true,
                'expanded' => true
            ])
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('name')
        ;
    }

    protected static function flattenRoles($rolesHierarchy)
    {


       return $result;
    }
}