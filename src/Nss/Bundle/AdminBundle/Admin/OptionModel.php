<?php
namespace Nss\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class OptionModel extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('service.order.title', null, ['label' => 'Заявление'])
            ->addIdentifier('service.title', null, ['label' => 'ENTITY_MODEL_SERVICE_MODEL_TITLE'])
            ->addIdentifier('option.name', null, ['label' => 'ENTITY_BASE_OPTION'])
            ->addIdentifier('condition.value', null, ['label' => 'ENTITY_MODEL_CONDITION_MODEL_TITLE'])
            ->addIdentifier('reward.value', null, ['label' => 'ENTITY_MODEL_REWARD_MODEL_TITLE'])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('option', 'sonata_type_model', [
                'property' => 'name',
                'label' => 'Опция',
                'btn_add' => false
            ])
            ->add('condition', 'sonata_type_model', array(
                'property' => 'name',
                'label' => 'Условие',
                'btn_add' => false
            ))
            ->add('rewardModel', 'sonata_type_model', array(
                'property' => 'name',
                'label' => 'Вознаграждение',
                'btn_add' => false
            ))
            ;
    }
}