<?php

namespace Nss\Bundle\MessageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NssMessageBundle:Default:index.html.twig');
    }
}
