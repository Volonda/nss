<?php
namespace Nss\Bundle\MessageBundle\Form\Type;

use Nss\Bundle\MessageBundle\Entity\OrderMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class OrderMessageType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', TextareaType::class, [
            'label' => 'Сообщение',
            'attr' => ['rows' => 5]
        ])->add('submit', SubmitType::class, [
            'label' => 'FORM_SUBMIT_SEND'
        ])
        ;
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderMessage::class
        ]);
    }
}