<?php
namespace Nss\Bundle\MessageBundle\Manager;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\MessageBundle\Entity\Message;
use Nss\Bundle\MessageBundle\Entity\OrderMessage;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\MessageBundle\Event\MessageCreateEvent;
use Nss\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MessageManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param User $sender
     * @param OrderMessage $message
     * @param Order $order
    */
    public function createOrderMessage(User $sender, OrderMessage $message, Order $order)
    {
        $message->setSender($sender);
        $message->setOrder($order);

        $this->create($message);
    }

    /** @var Message $message*/
    private function create(Message $message)
    {
        $this->em->persist($message);
        $this->dispatcher->dispatch('message_created', new MessageCreateEvent($message));
    }

    public function getOrderMessages(Order $order)
    {
        return $this->em->getRepository(OrderMessage::class)->findBy(['order' => $order->getId()],['createdAt' => 'desc']);
    }
}