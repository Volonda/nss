<?php
namespace Nss\Bundle\MessageBundle\Event;

use Nss\Bundle\MessageBundle\Entity\Message;
use Symfony\Component\EventDispatcher\Event;

class MessageCreateEvent extends Event
{
    /** @var Message*/
    private $message;

    /**
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function getMessage(Message $message)
    {
        return $this->message;
    }
}