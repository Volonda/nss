<?php
namespace Nss\Bundle\HistoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nss\Bundle\AppBundle\Entity\Traits\TimestampableEntityString;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "order_created" = "OrderCreatedRecord",
 *     "order_updated" = "OrderUpdatedRecord",
 *     "transition" = "TransitionRecord"
 * })
 * @ORM\Table("app_history")
 */
abstract class Record
{
    const TYPE_NAME = 'record';
    const TYPE = 'record';

    use TimestampableEntityString;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    public function getTypeName()
    {
        return static::TYPE_NAME;
    }

    public function getType()
    {
        return static::TYPE;
    }
}
