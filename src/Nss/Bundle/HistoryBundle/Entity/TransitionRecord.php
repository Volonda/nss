<?php
namespace Nss\Bundle\HistoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\WorkflowBundle\Entity\Transition;

/**
 * @ORM\Entity
 */
class TransitionRecord extends Record
{
    const TYPE_NAME = 'Изменение статуса';
    const TYPE = 'transition';

    /**
     * @var Transition
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Transition")
     * @ORM\JoinColumn(name="transition_id", referencedColumnName="id")
     */
    private $transition;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @return Transition
     */
    public function getTransition(): Transition
    {
        return $this->transition;
    }

    /**
     * @param Transition $transition
     */
    public function setTransition(Transition $transition)
    {
        $this->transition = $transition;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }
}