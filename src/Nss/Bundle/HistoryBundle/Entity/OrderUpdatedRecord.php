<?php
namespace Nss\Bundle\HistoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nss\Bundle\OrderBundle\Entity\Order;

/**
 * @ORM\Entity
 */
class OrderUpdatedRecord extends Record
{
    const TYPE_NAME = 'Изменение заявления';
    const TYPE = 'order_updated';

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @return Order
     */
    public function getOrder() : Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }
}