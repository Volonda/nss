<?php
namespace Nss\Bundle\HistoryBundle\Factory;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\OrderBundle\Entity\Order;

class RecordFactory
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Order $order
    */
    public function create(Order $order)
    {

    }
}