<?php
namespace Nss\Bundle\HistoryBundle\Twig;

use Nss\Bundle\HistoryBundle\Entity\Record;
use Nss\Bundle\HistoryBundle\Manager\HistoryManager;
use Nss\Bundle\OrderBundle\Entity\Order;

class HistoryExtension extends \Twig_Extension
{
    /** @var HistoryManager*/
    private $manager;

    /**
     * @param HistoryManager $manager
    */
    public function  __construct(HistoryManager $manager)
    {
        $this->manager = $manager;
    }

    public function getFunctions()
    {
        return [ new \Twig_SimpleFunction('historyOrderRecords', [$this, 'getByOrder'] ) ];
    }

    /**
     * @param Order $order
     * @param $limit = null
     * @return array
    */
    public function getByOrder(Order $order, $limit = null) : array
    {
        return $this->manager->getByOrder($order, $limit);
    }

    public function getName()
    {
        return 'history_extension';
    }
}