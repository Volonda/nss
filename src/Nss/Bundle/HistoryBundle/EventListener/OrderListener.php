<?php

namespace Nss\Bundle\HistoryBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\HistoryBundle\Entity\OrderCreatedRecord;
use Nss\Bundle\HistoryBundle\Entity\OrderUpdatedRecord;
use Nss\Bundle\OrderBundle\Event\OrderCreatedEvent;
use Nss\Bundle\OrderBundle\Event\OrderUpdatedEvent;

class OrderListener
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param OrderCreatedEvent $event
     */
    public function onCreate(OrderCreatedEvent $event)
    {
        $record = new OrderCreatedRecord();
        $record->setOrder($event->getOrder());

        $this->em->persist($record);
    }

    /**
     * @param OrderUpdatedEvent $event
     */
    public function onUpdate(OrderUpdatedEvent $event)
    {
        $record = new OrderUpdatedRecord();
        $record->setOrder($event->getOrder());

        $this->em->persist($record);
    }
}