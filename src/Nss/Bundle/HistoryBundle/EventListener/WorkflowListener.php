<?php

namespace Nss\Bundle\HistoryBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\HistoryBundle\Entity\TransitionRecord;
use Nss\Bundle\WorkflowBundle\Event\TransitionEvent;

class WorkflowListener
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param TransitionEvent $event
     */
    public function onTransition(TransitionEvent $event)
    {
        $record = new TransitionRecord();
        $record->setOrder($event->getOrder());
        $record->setTransition($event->getTransition());

        $this->em->persist($record);
    }
}