<?php
namespace Nss\Bundle\HistoryBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Nss\Bundle\HistoryBundle\Entity\Record;
use Nss\Bundle\OrderBundle\Entity\Order;

class HistoryManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getByOrder(Order $order, $limit = null) : array
    {
        $qb = $this->em->getConnection()
            ->createQueryBuilder()
            ->select('id')
            ->from('public.app_history','h')
            ->where('order_id = :orderId')
            ->setParameter('orderId', $order->getId())
        ;

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $stmt =  $qb->execute();
        $result = $stmt->fetchAll();

        $data = [];
        if (count($result)) {

            $id = array_column($result,'id');
            $qb = $this->em->createQueryBuilder();
            $qb->select('h')
                ->from(Record::class,'h')
                ->where($qb->expr()->in('h', $id))
            ;

            $data = $qb->getQuery()->getResult();
        }


        return $data;
    }
}