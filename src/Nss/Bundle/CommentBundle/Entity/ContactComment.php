<?php
namespace Nss\Bundle\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class ContactComment extends Comment
{
    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\ContactBundle\Entity\Contact", inversedBy="comments")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @return Contact
     */
    public function getContact(): Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact)
    {
        $this->contact = $contact;
    }
}