<?php
namespace Nss\Bundle\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Nss\Bundle\OrderBundle\Entity\Order;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class OrderComment extends Comment
{
    /**
     * @var Order
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Order", inversedBy="comments")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @return Order
     */
    public function getOrder() : ?Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }
}