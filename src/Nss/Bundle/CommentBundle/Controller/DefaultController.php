<?php

namespace Nss\Bundle\CommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NssCommentBundle:Default:index.html.twig');
    }
}
