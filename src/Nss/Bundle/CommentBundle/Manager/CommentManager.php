<?php
namespace Nss\Bundle\CommentBundle\Manager;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\CommentBundle\Entity\Comment;
use Nss\Bundle\CommentBundle\Entity\ContactComment;
use Nss\Bundle\CommentBundle\Entity\OrderComment;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\UserBundle\Entity\User;

class CommentManager
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
    */
    public function __construct(
        EntityManager $em
    ) {
        $this->em = $em;
    }

    /**
     * @param OrderComment $comment
     * @param Order $order
     * @param User $owner
     */
    public function createOrderComment(OrderComment $comment, Order $order, User $owner)
    {
        $comment->setOwner($owner);
        $comment->setOrder($order);

        $this->em->persist($comment);
    }

    /**
     * @param ContactComment $comment
     * @param Contact $contact
     * @param User $owner
     */
    public function createContactComment(ContactComment $comment, Contact $contact,  User $owner)
    {
        $comment->setOwner($owner);
        $comment->setContact($contact);

        $this->em->persist($comment);
    }


    /**
     * @param User $owner
     * @param Order $order
     * @return  array
    */
    public function getOrderComment(Order $order, User $owner)
    {
        return $this->em->getRepository(OrderComment::class)->findBy([
            'owner' => $owner->getId(),
            'order' => $order->getId()
        ], [
            'createdAt' => 'DESC'
        ]);
    }

    /**
     * @param User $owner
     * @param Contact $contact
     * @return  array
     */
    public function getContactComment(Contact $contact, User $owner)
    {
        return $this->em->getRepository(ContactComment::class)->findBy([
            'contact' => $contact->getId(),
            'owner' => $owner->getId()
        ], [
            'createdAt' => 'DESC'
        ]);
    }

    /**
     * @param Comment $comment
    */
    public function delete(Comment $comment)
    {
        $this->em->remove($comment);
    }
}