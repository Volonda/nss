<?php
namespace Nss\Bundle\OrderBundle\Form\Type;

use Doctrine\ORM\Query\Expr\Base;
use Nss\Bundle\OrderBundle\Entity\BaseOption;
use Nss\Bundle\OrderBundle\Entity\BaseService;
use Nss\Bundle\OrderBundle\Entity\Model\OptionModel;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModel;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModelFormType;
use Nss\Bundle\OrderBundle\Entity\Service;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Yaml\Yaml;

class ServiceModelType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ServiceModel $serviceModel */
        $serviceModel = $options['serviceModel'];
        /** @var Service $service*/
        $service = $options['service'];

        /** @var ServiceModelFormType $serviceFormType*/
        $serviceFormType = $serviceModel->getFormType();

        $defaultOptions= [
            'attr' => [
                'class' => 'recalculation'
            ],
            'label' =>  $serviceModel->getService()->getName()
        ];

        $typeOptions = array_merge_recursive($defaultOptions, (array) Yaml::parse($serviceFormType->getOptions()));

        switch ($serviceFormType->getFormType()->getCode()) {
            case ChoiceType::class:

                /** @var OptionModel $optionModel*/
                foreach ($serviceModel->getOptions() as $optionModel) {

                    $typeOptions['choices'][$optionModel->getOption()->getName()] = $optionModel->getId();

                    if (
                        $service instanceof Service
                        && $service->getOption()->getBaseOption()->getId() == $optionModel->getOption()->getId()
                    ) {

                        $typeOptions['data'] = $optionModel->getId();
                    }
                }

                break;
        }

        $builder->add('optionModel', $serviceFormType->getFormType()->getCode(), $typeOptions);
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['serviceModel'] = $options['serviceModel'];
        $view->vars['user'] = $options['user'];
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('serviceModel');
        $resolver->setRequired('user');

        $resolver->setDefaults([
            'service' => null
        ]);
    }
}