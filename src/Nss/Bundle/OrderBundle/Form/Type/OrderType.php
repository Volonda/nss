<?php
namespace Nss\Bundle\OrderBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Nss\Bundle\ContactBundle\Form\Type\ContactType;
use Nss\Bundle\ContactBundle\Form\Type\OrganizationContactType;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\UserBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class OrderType extends AbstractType {

    const CONTACT_CREATE_OR_SELECT__SELECT = 1;
    const CONTACT_CREATE_OR_SELECT__CREATE = 2;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user*/
        $user = $options['user'];

        if (
            !$options['contact'] instanceof Contact
            &&  !$options['organization'] instanceof Organization
        ) {
            throw new \Exception('organization or contact option is required or passed invalid type');
        }

        if ($options['contact']) {
            $organization = $options['contact']->getOrganization();
            $contact = $options['contact'];
        } else{
            $contact = null;
            $organization = $options['organization'];
        }

        $builder->add('services', ServiceModelCollectionType::class, [
            'serviceModels' => $options['serviceModels'],
            'label'=> false,
            'services' => $options['services'],
            'user' => $options['user']
        ])
        ->add('contactSelectOrCreate', ChoiceType::class,
            [
                'label' => false,
                'expanded' => true,
                'choices' => [
                    'Выбрать контакт' => self::CONTACT_CREATE_OR_SELECT__SELECT,
                    'Указать контактную информацию' => self::CONTACT_CREATE_OR_SELECT__CREATE
                ],
                'data' => 1
        ])
        ->add('contact', EntityType::class, [
            'class' => Contact::class,
            'choice_label' => 'labelName',
            'label' => 'Контакт',
            'placeholder' => 'Выберите контакт',
            'required' => false,
            'data' => ($contact)
                ? $contact
                : null,
            'query_builder' => function (EntityRepository $er) use ($organization, $user) {
                return $er->createQueryBuilder('c')
                    ->where('c.owner = :_owner')
                    ->join('c.organization','o')
                    ->andWhere('o =:_organization')
                    ->setParameter('_owner', $user)
                    ->setParameter('_organization', $organization)
                ;
            },
            'constraints' => [
                new NotNull(['groups' => 'SelectContact'])
            ]
        ])
        ->add('orderContact', ContactType::class, [
            'label' => false
        ])
        ->add('submit_and_document', SubmitType::class,[
            'label' => 'Сохранить и загрузить документы',
            'attr' => ['class' => 'btn-promary order-send btn-block']
        ])
        ->add('submit', SubmitType::class,[
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn-success order-send btn-block']
        ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('serviceModels');
        $resolver->setRequired('user');

        $resolver->setDefaults([
            'contact' => null,
            'organization' => null,
            'services' => [],
            'attr' => [
                'novalidate' => 'novalidate'
            ],
            'validation_groups' => function(Form $form) {
                $groups = ['Default'];
                $contactSelectOrCreate = $form->get('contactSelectOrCreate')->getData();

                if ($contactSelectOrCreate ==  self::CONTACT_CREATE_OR_SELECT__SELECT) {
                    $groups[] = 'SelectContact';
                } else {
                    $groups[] = 'CreateContact';
                }

                return $groups;
            }
        ]);

    }
}