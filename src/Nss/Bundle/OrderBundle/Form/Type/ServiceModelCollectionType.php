<?php
namespace Nss\Bundle\OrderBundle\Form\Type;

use Doctrine\ORM\Query\Expr\Base;
use Nss\Bundle\OrderBundle\Entity\BaseOption;
use Nss\Bundle\OrderBundle\Entity\BaseService;
use Nss\Bundle\OrderBundle\Entity\Model\OptionModel;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModel;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModelFormType;
use Nss\Bundle\OrderBundle\Entity\Service;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Yaml\Yaml;

class ServiceModelCollectionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $services = $options['services'];

        /**
         * @var ServiceModel $serviceModel
         * @var BaseService $baseService
         * @var BaseOption $baseOption
         * @var OptionModel $optionModel
         */
        foreach ($options['serviceModels'] as $serviceModel) {
            $data = null;
            /** @var Service $service*/
            foreach ($services as $service) {
                if ($serviceModel->getService()->getId() == $service->getBaseService()->getId()) {
                   $data = $service;

                   break;
                }
            }

            $builder->add($serviceModel->getId(), ServiceModelType::class, [
                'serviceModel' => $serviceModel,
                'service' => $data,
                'user' => $options['user'],
                'label' => false
            ]);
        }
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('serviceModels');
        $resolver->setRequired('user');

        $resolver->setDefaults([
            'services' => []
        ]);
    }
}