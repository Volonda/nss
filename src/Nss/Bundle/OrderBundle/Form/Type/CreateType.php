<?php
namespace Nss\Bundle\OrderBundle\Form\Type;


use Doctrine\ORM\EntityRepository;
use Nss\Bundle\OrganizationBundle\Validator\Constraints\InnValid;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\SroBundle\Entity\BaseSroType;
use Nss\Bundle\SroBundle\Repository\SroRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CreateType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sroOptions = [
            'class' => Sro::class,
            'choice_label' => 'name',
            'placeholder' => 'Выберите СРО',
            'label' => 'СРО',
            'query_builder' => function (SroRepository $er) {

                return $er->getWithSroOrderModelQuery();
            }
        ];

        $builder
            ->add('inn', TextType::class, [
                'label' => 'ИНН строителя',
                'constraints' => [
                    new InnValid()
                ]
            ])
            ->add('sroType', EntityType::class, [
                'label' => 'Вид СРО',
                'class' => BaseSroType::class,
                'placeholder' => 'Выберите вид СРО',
                'choice_label' => 'name'
            ])
            ->add('sro', EntityType::class, $sroOptions)
        ;

        $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) use ($sroOptions){
            $form = $event->getForm();
            $data = $event->getData();

            $sroOptions['data'] = $data['sro'];
            $sroType = $data['sroType'];

            if ($sroType instanceof BaseSroType) {

                $sroOptions['query_builder'] = function (SroRepository $er) use ($sroType) {

                    return $er->getWithSroOrderModelByCodeQuery($sroType);
                };

                $form->add('sro', EntityType::class, $sroOptions);
            }

        });

        $builder->add('submit', SubmitType::class,[
            'label' => 'FORM_CREATE_CONTACT_SUBMIT_BUTTON',
        ]);
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'sroType' => null
        ]);
    }

}