<?php
namespace Nss\Bundle\OrderBundle\Event;

use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class OrderCreatedEvent extends Event
{
    /** @var Order*/
    private $order;

    /** @var User*/
    private $user;

    /**
     * @param Order $order
     * @param User $user
     */
    public function __construct(Order $order, User $user)
    {
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * @return Order
     */
    public function getOrder() : Order
    {
        return $this->order;
    }

    /**
     * @return OrderModel
    */
    public function getOrderModel() : OrderModel
    {
        return $this->getOrder()->getOrderModel();
    }

    public function getUser() : User
    {
        return $this->user;
    }
}