<?php

namespace Nss\Bundle\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;

class TemplateOrderModelRepository extends EntityRepository
{
    public function getEnabledByType(BaseOrderType $baseOrderType)
    {
        return $this->createQueryBuilder('tm')
            ->where('tm.type = :type')
            ->andWhere('tm.enabled = TRUE')
            ->setParameter('type', $baseOrderType->getId())
            ->getQuery()
            ->setCacheable(true)
            ->getResult()
            ;
    }
}
