<?php

namespace Nss\Bundle\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel;
use Nss\Bundle\SroBundle\Entity\Sro;

class SroOrderModelRepository extends EntityRepository
{
    public function getBySroAndTemplateOrderModel(Sro $sro, TemplateOrderModel $templateOrderModel)
    {
        return $this->createQueryBuilder('om')
            ->where('om.template = :template')
            ->andWhere('om.sro = :sro')
            ->setParameter('template', $templateOrderModel)
            ->setParameter('sro', $sro)
            ->getQuery()
            ->setMaxResults(1)
            ->setCacheable(true)
            ->getOneOrNullResult()
            ;
    }
}
