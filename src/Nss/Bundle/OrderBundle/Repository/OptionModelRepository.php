<?php

namespace Nss\Bundle\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;

class OptionModelRepository extends EntityRepository
{
     public function getRewardByIdList(array $idList) {

        $qb = $this->createQueryBuilder('o');

        return $qb->where($qb->expr()->in('o.id', $idList))
            ->getQuery()
            ->getResult()
            ;
     }

    public function getConditionByIdList(array $idList) {

        $qb = $this->createQueryBuilder('o');

        return $qb->where($qb->expr()->in('o.id', $idList))
            ->join('o.condition','c')
            ->getQuery()
            ->getResult()
            ;
    }




}
