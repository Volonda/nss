<?php

namespace Nss\Bundle\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;

class BaseOrderTypeRepository extends EntityRepository
{
    public function getFirst() : BaseOrderType
    {
        $type = $this->createQueryBuilder('bot')
            ->orderBy('bot.id')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()
        ;

        return $type;
    }
}
