<?php
namespace Nss\Bundle\OrderBundle\Manager;


use Doctrine\ORM\EntityManager;
use ITSymfony\Bundle\BaseCollectionBundle\Formatter\CollectionFormatter;
use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Nss\Bundle\OrderBundle\Calculator\ConditionCalculator;
use Nss\Bundle\OrderBundle\Calculator\RewardCalculator;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\OrderBundle\Entity\Condition;
use Nss\Bundle\OrderBundle\Entity\Model\OptionModel;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModel;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel;
use Nss\Bundle\OrderBundle\Entity\Option;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrderBundle\Entity\Reward;
use Nss\Bundle\OrderBundle\Entity\Service;
use Nss\Bundle\OrderBundle\Entity\UserReward;
use Nss\Bundle\OrderBundle\Entity\View\OrderWorkflowActionView;
use Nss\Bundle\OrderBundle\Event\OrderUpdatedEvent;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\OrderBundle\Event\OrderCreatedEvent;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\UserBundle\Entity\Action;
use Nss\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class OrderManager
{
    /** @var EntityManager */
    private $em;

    /** @var array*/
    private $listFilters = [
        'id' => 'int',
        'c.fio' => 'like',
        'org.inn' => 'like',
        's.id' =>  [
            'options' => [ ],
            'type' => 'eq'
        ],
        'ot.id' => [
            'options' => [ ],
            'type' => 'eq'
        ],
        'o.createdAt' => 'date',
        'os.id' => [
            'options' => [],
            'type' => 'eq'
        ],
        'c.phone' => 'like',
        'c.email' => 'like',

    ];

    /** @var Paginator*/
    private $paginator;

    /** @var CollectionFormatter*/
    public $collectionFormatter;

    /** @var EventDispatcherInterface*/
    public $dispatcher;
    /**
     * @param EntityManager $em
     * @param Paginator $paginator
     * @param CollectionFormatter $collectionFormatter
     * @param EventDispatcherInterface $dispatcher
    */
    public function __construct(
        EntityManager $em,
        Paginator $paginator,
        CollectionFormatter $collectionFormatter,
        EventDispatcherInterface $dispatcher
    ) {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->collectionFormatter = $collectionFormatter;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @return array
    */
    public function getFilters() : array
    {
        $filters = $this->listFilters;

        $filters['ot.id']['options'] = $this->collectionFormatter->toAssocArray(BaseOrderType::class,'id','name');
        $filters['s.id']['options'] = $this->collectionFormatter->toAssocArray(Sro::class,'id','name');
        $filters['os.id']['options'] = $this->collectionFormatter->toAssocArray(BaseOrderStatus::class,'id','name');

        return $filters;
    }

    /**
     * @param int $page
     * @param int $limit
     * @param Filter $filter
     * @param User $user
     * @return PaginationInterface
     */
    public function getMyPaginate(User $user, $page, $limit, Filter $filter) : PaginationInterface
    {

        $action = $this->em->getRepository(Action::class)->findOneBy(['code' => 'order_view_my']);

        $subQb = $this->em->createQueryBuilder()
            ->select('owa')
            ->from(OrderWorkflowActionView::class,'owa')
            ->join('owa.actions','a')
            ->where('owa.orders = o')
            ->andWhere('a =:action')
        ;

        $qb = $this->em->createQueryBuilder();
        $qb->select('o')
            ->from(Order::class,'o')
            ->join('o.type','ot')
            ->join('o.sro','s')
            ->join('o.status','os')
            ->join('o.contact','c')
            ->join('c.organization','org')
            ->where('o.owner = :owner')
            ->setParameter('owner', $user)
            ->andWhere($qb->expr()->exists($subQb))
            ->setParameter('action', $action)
        ;

        $filter->addFiltersToQB($qb);

        return $this->paginator->paginate($qb, $page, $limit, [
            'defaultSortFieldName' => 'o.updatedAt',
                'defaultSortDirection' => 'ASC']
        );

    }

    /**
     * @param SroOrderModel $orderModel
     * @param array $servicesModels
     * @param Sro $sro
     * @param Contact $contact
     * @param User $owner
     * @return Order
     * @throws \Exception
     * @return Order
    */
    public function create(SroOrderModel $orderModel, array $servicesModels, Sro $sro, Contact $contact, User $owner) : Order
    {
        if (!count($servicesModels)) {

            throw new \Exception('list of service model is empty');
        }

        $order = new Order();
        $order->setStatus($orderModel->getStatus());
        $order->setSro($sro);
        $order->setContact($contact);
        $order->setOwner($owner);
        $order->setOrderModel($orderModel);
        $order->setType($orderModel->getType());

        //create services
        foreach ($servicesModels as $serviceModelId => $serviceModelArray) {

            /** @var ServiceModel $serviceModel*/
            $serviceModel = $this->em->getRepository(ServiceModel::class)->find($serviceModelId);
            /** @var OptionModel $optionModel*/
            $optionModel = null;
            if ($serviceModelArray['optionModel']) {
                $optionModel = $this->em->getRepository(OptionModel::class)->find($serviceModelArray['optionModel']);
                $this->createOrUpdateService($order,$serviceModel, $optionModel);
            }
        }

        $this->recalculate($order);

        $this->em->persist($order);
        $this->em->flush();

        $this->dispatcher->dispatch('order_created', new OrderCreatedEvent($order, $owner));

        return $order;
    }

    public function update(Order $order, array $servicesModels, User $user)
    {
        $em = $this->em;

        foreach ($servicesModels as $serviceModelId => $serviceModelArray) {

            /** @var ServiceModel $serviceModel*/
            $serviceModel = $this->em->getRepository(ServiceModel::class)->find($serviceModelId);
            /** @var OptionModel $optionModel*/
            $optionModel = $this->em->getRepository(OptionModel::class)->find($serviceModelArray['optionModel']);

            //create or update services
           $this->createOrUpdateService($order,$serviceModel,$optionModel);
        }

        //remove services
        $order->getServices()->forAll(function ($num, Service $service) use ($em, $servicesModels, $order){

            if (!in_array($service->getServiceModel()->getId(), array_keys($servicesModels))) {

                $order->removeService($service);
            }

            return true;
        });

        $this->recalculate($order);

        $this->dispatcher->dispatch('order_updated', new OrderUpdatedEvent($order, $user));
    }

    private function createOrUpdateService(Order $order, ServiceModel $serviceModel, OptionModel $optionModel)
    {
        $serviceCollection = $order->getServices()->filter(function (Service $service) use ($serviceModel) {
            return $service->getServiceModel()->getId() == $serviceModel->getId();
        });

        if ($serviceCollection->count() == 1) {
            //update
            /** @var Service $service */
            $service = $serviceCollection->first();

        } elseif ($serviceCollection->count() == 0) {
            //create
            $service = new Service();
            $service->setTitle($serviceModel->getTitle());
            $service->setBaseService($serviceModel->getService());
            $service->setServiceModel($serviceModel);
            $service->setOrder($order);
            $order->addService($service);

            $this->em->persist($service);

        } else {
            throw new \Exception('multiple service model');
        }

        // create option if not exists
        if (!$service->getOption()) {
            $option = new Option();
            $option->setBaseOption($optionModel->getOption());
            $service->setOption($option);
            $option->setService($service);

            $this->em->persist($option);
        } else {
            $option = $service->getOption();
            $option->setBaseOption($optionModel->getOption());
        }

        // create reward if need
        if ($rewardModel = $optionModel->getActualRewardModel($order->getOwner())) {
            if(!$option->getReward()){
                $reward = new Reward();
                $option->setReward($reward);

                $this->em->persist($reward);
            } else {
                $reward = $option->getReward();
            }

            $reward->setValue($rewardModel->getValue());
            $reward->setType($rewardModel->getType());
            $reward->setName($rewardModel->getName());
        } else {
            $option->setReward(null);
        }

        if ($conditionModel = $optionModel->getCondition()) {

            if (!$option->getCondition()){
                $condition = new Condition();
                $option->setCondition($condition);

                $this->em->persist($condition);
            } else {
                $condition = $option->getCondition();
            }

            $condition->setType($conditionModel->getType());
            $condition->setValue($conditionModel->getValue());
            $condition->setName($conditionModel->getName());
        } else {
            $option->setCondition(null);
        }
    }

    private function recalculate(Order $order)
    {
        $rewardCalculator = new RewardCalculator();
        $conditionCalculator = new ConditionCalculator();

        $options = $order->getOptions();
        $order->setConditionTotal($conditionCalculator->getTotal($options));
        $order->setRewardTotal($rewardCalculator->getTotal($options, $order->getOwner()));
    }

    /**
     * @param Sro $sro
     * @param User $user
     * @return array
     */
    public function getByUserAndSro(User $user, Sro $sro) : array
    {
        $result = $this->em->getRepository(Order::class)->findBy([
            'sro' => $sro->getId(),
            'owner' => $user->getId()
        ]);

        return $result;
    }


    /**
     * @param Organization $organization
     * @param User $user
     * @param int $limit
     * @return array
     */
    public function getByUserAndOrganization(User $user, Organization $organization, int $limit = null) : array
    {
        $qb = $this->em->createQueryBuilder()
            ->select('o')
            ->from(Order::class, 'o')
            ->join('o.contact','c')
            ->join('o.owner','own')
            ->join('c.organization','org')
            ->where('org = :organization')
            ->setParameter('organization', $organization->getId())
            ->andWhere('own = :owner')
            ->setParameter('owner', $user)
            ;

        if ($limit){
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Sro $sro
     * @param TemplateOrderModel $templateOrderModel
    */
    public function createOrEnableSroOrderModel(Sro $sro, TemplateOrderModel $templateOrderModel)
    {
        $sroOrderModel = $this->em->getRepository(SroOrderModel::class)->findOneBy([
            'sro' => $sro->getId(),
            'template' => $templateOrderModel->getId()
        ]);

        if (!$sroOrderModel instanceof SroOrderModel) {
            $sroOrderModel= new SroOrderModel();
            $sroOrderModel->setTemplate($templateOrderModel);
            $sroOrderModel->setRewrite(false);
            $sroOrderModel->setSro($sro);

            $this->em->persist($sroOrderModel);
        }

        $sroOrderModel->setEnabled(true);
    }

    public function disableSroOrderModel(Sro $sro, TemplateOrderModel $templateOrderModel)
    {
        $sroOrderModel = $this->em->getRepository(SroOrderModel::class)->findOneBy([
            'sro' => $sro->getId(),
            'template' => $templateOrderModel->getId()
        ]);

        if (!$sroOrderModel instanceof SroOrderModel) {
            throw new \Exception('SroOrderModel not found for template ' . $templateOrderModel->getId());
        }

        $sroOrderModel->setEnabled(false);
    }
}