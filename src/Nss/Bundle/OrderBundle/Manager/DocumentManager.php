<?php
namespace Nss\Bundle\OrderBundle\Manager;

use Doctrine\ORM\EntityManager;

use Nss\Bundle\DocumentBundle\Entity\Document;
use Nss\Bundle\DocumentBundle\Entity\DocumentVersion;
use Nss\Bundle\FileBundle\Entity\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentManager
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     **/

    public function __construct(
        EntityManager $em
    ) {
        $this->em = $em;
    }

    /**
     * @param Document $document
     * @param array $uploadedFiles
     * @param string $comment
     * @throws \Exception
    */
   public function upload(Document $document, array $uploadedFiles, ?string $comment = null)
   {
       if (sizeof($uploadedFiles)) {
           $version = new DocumentVersion();
           $version->setDocument($document);
           $version->setComment($comment);
           $document->addVersion($version);

           foreach ($uploadedFiles as $uploadedFile) {

               if (!$uploadedFile instanceof UploadedFile) {
                   throw new \Exception('Unsupported uploaded file type');
               }

               $file = new File();
               $file->setUploadedFile($uploadedFile);
               $version->addFile($file);
           }

           $this->em->persist($document);
       }
   }
}