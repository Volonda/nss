<?php
namespace Nss\Bundle\OrderBundle\Manager;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;

class ModelManager
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
    */
    public function __construct(
        EntityManager $em
    ) {
        $this->em = $em;
    }

    /**
     * @param Sro $sro
     * @return OrderModel
    */
    public function getDefaultOrderModel(Sro $sro) : ?OrderModel
    {
        return $this->em->createQueryBuilder()
            ->select('om')
            ->from(SroOrderModel::class,'om')
            ->join('om.sro','s')
            ->where('s = :_sro')
            ->andWhere('om.enabled = TRUE')
            ->setParameter('_sro', $sro)
            ->orderBy('om.id','DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param Sro $sro
     * @return array
    */
    public function getEnabledBySro(Sro $sro) : array
    {
        $orderModels = $this->em->createQueryBuilder()
            ->select()
            ->select('om')
            ->from(SroOrderModel::class, 'om')
            ->join('om.sro','s')
            ->where('s = :sro')
            ->andWhere('om.enabled = TRUE')
            ->setParameter('sro', $sro)
            ->getQuery()
            ->getResult()
        ;

        return $orderModels;
    }
}