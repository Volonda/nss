<?php
namespace Nss\Bundle\OrderBundle\Twig;

use Nss\Bundle\OrderBundle\Manager\OrderManager;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\UserBundle\Entity\User;

class OrderExtension extends \Twig_Extension
{
    /** @var OrderManager*/
    private $orderManager;

    /**
     * @param OrderManager $orderManager
    */
    public function __construct(OrderManager $orderManager)
    {
        $this->orderManager = $orderManager;
    }

    public function getFilters()
    {
        return [ new \Twig_SimpleFilter('order_money_format', [$this, 'getOrderMoneyFormat'] ) ];
    }

    public function getFunctions()
    {
        return [ new \Twig_SimpleFunction('orderGetLastByUserOrganization', [$this, 'getLastByUserAndOrganization'] ) ];
    }


    /**
     * @param float $value
     * @return string
     */
    public function getOrderMoneyFormat($value = null)
    {
        return number_format($value, 0,'',' ') . ' руб';
    }

    /**
     * @param User $user
     * @param Organization $organization
     * @param int $limit
     * @return array
    */
    public function getLastByUserAndOrganization(User $user, Organization $organization, int $limit)
    {
        return $this->orderManager->getByUserAndOrganization($user, $organization, $limit);
    }

    public function getName()
    {
        return 'order_extension';
    }
}