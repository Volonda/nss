<?php
namespace Nss\Bundle\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Nss\Bundle\DocumentBundle\Entity\DocumentBundle;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\AppBundle\Entity\Traits\TimestampableEntityString;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\UserBundle\Entity\User;
use Nss\Bundle\WorkflowBundle\Entity\Workflow;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_orders", schema="public")
 */
class Order
{
    const DOCUMENT_BUNDLE_INPUT = 'input';
    const DOCUMENT_BUNDLE_OUTPUT = 'output';

    use TimestampableEntityString;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BaseOrderType
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     */
    private $type;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\OrderBundle\Entity\Service", mappedBy="order")
     */
    private $services;

    /**
     * @var Sro
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\SroBundle\Entity\Sro")
     * @ORM\JoinColumn(name="sro_id", referencedColumnName="id")
     */
    private $sro;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var Contact
     *
     * @Assert\Count(min="1")
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\ContactBundle\Entity\Contact", inversedBy="orders")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @var BaseOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $status;

    /**
     * @var float
     *
     * @ORM\Column(name="reward_total", type="decimal", scale=12, precision=2, nullable=false)
     */
    private $rewardTotal;

    /**
     * @var float
     *
     * @ORM\Column(name="condition_total", type="decimal", scale=12, precision=2, nullable=false)
     */
    private $conditionTotal;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\CommentBundle\Entity\OrderComment", mappedBy="order")
     */
    private $comments;

    /**
     * @var DocumentBundle
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\DocumentBundle")
     * @ORM\JoinColumn(name="input_document_bundle_id", referencedColumnName="id")
     */
    private $inputDocumentBundle;

    /**
     * @var DocumentBundle
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\DocumentBundle")
     * @ORM\JoinColumn(name="output_document_bundle_id", referencedColumnName="id")
     */
    private $outputDocumentBundle;

    /**
     * @var Workflow
     *
     * @ORM\OneToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Workflow", mappedBy="order")
     */
    private $workflow;

    /**
     * @var SroOrderModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel")
     * @ORM\JoinColumn(name="order_model_id", referencedColumnName="id")
     */
    private $orderModel;

    public function __construct()
    {
        $this->services = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection
     */
    public function getServices() : Collection
    {
        return $this->services;
    }

    /**
     * @param Collection $services
     */
    public function setServices(Collection $services)
    {
        $this->services = $services;
    }

    public function addService(Service $service)
    {
        $this->getServices()->add($service);
    }

    /**
     * @return Sro
     */
    public function getSro() : ?Sro
    {
        return $this->sro;
    }

    /**
     * @param Sro $sro
     */
    public function setSro(Sro $sro)
    {
        $this->sro = $sro;
    }

    /**
     * @return BaseOrderType
     */
    public function getType() : BaseOrderType
    {
        return $this->type;
    }

    /**
     * @param BaseOrderType $type
     */
    public function setType(BaseOrderType $type)
    {
        $this->type = $type;
    }

    /**
     * @return Contact
     */
    public function getContact() : ?Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return User
     */
    public function getOwner() : User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return BaseOrderStatus
     */
    public function getStatus() : BaseOrderStatus
    {
        return $this->status;
    }

    /**
     * @param BaseOrderStatus $status
     */
    public function setStatus(BaseOrderStatus $status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getRewardTotal(): float
    {
        return $this->rewardTotal;
    }

    /**
     * @param float $rewardTotal
     */
    public function setRewardTotal(float $rewardTotal)
    {
        $this->rewardTotal = $rewardTotal;
    }

    /**
     * @return float
     */
    public function getConditionTotal(): float
    {
        return $this->conditionTotal;
    }

    /**
     * @param float $conditionTotal
     */
    public function setConditionTotal(float $conditionTotal)
    {
        $this->conditionTotal = $conditionTotal;
    }

    public function getOptions() : Collection
    {
        $collection = new ArrayCollection();

        /** @var Service $service*/
        foreach ($this->getServices() as $service) {
            $collection->add($service->getOption());
        }

        return $collection;
    }

    /**
     * @return Collection
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @param Collection $comments
     */
    public function setComments(Collection $comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return DocumentBundle
     */
    public function getInputDocumentBundle(): ?DocumentBundle
    {
        return $this->inputDocumentBundle;
    }

    /**
     * @param DocumentBundle $inputDocumentBundle
     */
    public function setInputDocumentBundle(?DocumentBundle $inputDocumentBundle)
    {
        $this->inputDocumentBundle = $inputDocumentBundle;
    }

    /**
     * @return DocumentBundle
     */
    public function getOutputDocumentBundle(): ?DocumentBundle
    {
        return $this->outputDocumentBundle;
    }

    /**
     * @param DocumentBundle $outputDocumentBundle
     */
    public function setOutputDocumentBundle(?DocumentBundle $outputDocumentBundle)
    {
        $this->outputDocumentBundle = $outputDocumentBundle;
    }


    /**
     * @return Workflow
     */
    public function getWorkflow() : ?Workflow
    {
        return $this->workflow;
    }

    /**
     * @param Workflow $workflow
     */
    public function setWorkflow(Workflow $workflow)
    {
        $this->workflow = $workflow;
    }

    /**
     * @return SroOrderModel
     */
    public function getOrderModel()
    {
        return $this->orderModel;
    }

    /**
     * @param OrderModel $orderModel
     */
    public function setOrderModel(OrderModel $orderModel)
    {
        $this->orderModel = $orderModel;
    }

    public function removeService(Service $service)
    {
        $this->services->remove($service);
    }
}