<?php
namespace Nss\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModel;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_services", schema="public")
 */
class Service
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="services")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var BaseService
     *
     * @ORM\ManyToOne(targetEntity="BaseService")
     * @ORM\JoinColumn(name="base_service_id", referencedColumnName="id")
     */
    private $baseService;

    /**
     * @var ServiceModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\ServiceModel")
     * @ORM\JoinColumn(name="service_model_id", referencedColumnName="id")
     */
    private $serviceModel;

    /**
     * @var string
     *
     * @ORM\Column(type="text", name="title")
     */
    private $title;

    /**
     * @var Option
     * @ORM\OneToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Option", mappedBy="service")
     */
    private $option;

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Order
     */
    public function getOrder() : Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return BaseService
     */
    public function getBaseService(): BaseService
    {
        return $this->baseService;
    }

    /**
     * @param BaseService $baseService
     */
    public function setBaseService(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return Option
     */
    public function getOption() : ?Option
    {
        return $this->option;
    }

    /**
     * @param Option $option
     */
    public function setOption(Option $option)
    {
        $this->option = $option;
    }

    /**
     * @return ServiceModel
     */
    public function getServiceModel(): ServiceModel
    {
        return $this->serviceModel;
    }

    /**
     * @param ServiceModel $serviceModel
     */
    public function setServiceModel(ServiceModel $serviceModel)
    {
        $this->serviceModel = $serviceModel;
    }
}