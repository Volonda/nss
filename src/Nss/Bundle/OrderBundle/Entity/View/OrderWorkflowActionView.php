<?php
namespace Nss\Bundle\OrderBundle\Entity\View;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\UserBundle\Entity\Action;
use Nss\Bundle\WorkflowBundle\Entity\Workflow;
use Nss\Bundle\WorkflowBundle\Entity\WorkflowAction;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="vw_order_workflow_actions", schema="public")
 */
class OrderWorkflowActionView
{ 
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
  
    /**
     * @var Action
     * 
     * @ORM\OneToOne(targetEntity="Nss\Bundle\UserBundle\Entity\Action")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     */
    private $actions;

    /**
     * @var Workflow
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Workflow")
     * @ORM\JoinColumn(name="workflow_id", referencedColumnName="id", nullable=false)
     */
    private $workflows;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Workflow")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->workflows = new ArrayCollection();
        $this->actions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    } 
    

    /**
     * @return Workflow
     */
    public function getWorkflows(): Workflow
    {
        return $this->workflows;
    }

    /**
     * @param Workflow $workflows
     */
    public function setWorkflows(Workflow $workflows)
    {
        $this->workflows = $workflows;
    }

    /**
     * @return Order
     */
    public function getOrders(): Order
    {
        return $this->orders;
    }

    /**
     * @param Order $orders
     */
    public function setOrders(Order $orders)
    {
        $this->orders = $orders;
    }

    /**
     * @return Action
     */
    public function getActions(): Action
    {
        return $this->actions;
    }

    /**
     * @param Action $actions
     */
    public function setActions(Action $actions)
    {
        $this->actions = $actions;
    }
    
}