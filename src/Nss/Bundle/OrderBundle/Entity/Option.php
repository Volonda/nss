<?php
namespace Nss\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\Interfaces\ConditionAccessibleInterface;
use Nss\Bundle\OrderBundle\Entity\Interfaces\ConditionCalculableInterface;
use Nss\Bundle\OrderBundle\Entity\Interfaces\RewardAccessibleInterface;
use Nss\Bundle\OrderBundle\Entity\Interfaces\RewardCalculableInterface;
use Nss\Bundle\UserBundle\Entity\User;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_options", schema="public")
 */
class Option implements ConditionAccessibleInterface, RewardAccessibleInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Service
     *
     * @ORM\OneToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Service", inversedBy="option")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;

    /**
     * @var Reward
     *
     * @ORM\OneToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Reward")
     * @ORM\JoinColumn(name="reward_id", referencedColumnName="id")
     */
    private $reward;

    /**
     * @var Condition
     *
     * @ORM\OneToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Condition")
     * @ORM\JoinColumn(name="condition_id", referencedColumnName="id")
     */
    private $condition;

    /**
     * @var BaseOption
     *
     * @ORM\OneToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOption")
     * @ORM\JoinColumn(name="base_option_id", referencedColumnName="id")
     */
    private $baseOption;

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Service
     */
    public function getService() : Service
    {
        return $this->service;
    }

    /**
     * @param Service Service
     */
    public function setService(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @return Reward
     */
    public function getReward(): ?Reward
    {
        return $this->reward;
    }

    /**
     * @param Reward $reward
     */
    public function setReward(?Reward $reward)
    {
        $this->reward = $reward;
    }

    /**
     * @return Condition
     */
    public function getCondition(): ?Condition
    {
        return $this->condition;
    }

    /**
     * @param Condition $condition
     */
    public function setCondition(?Condition $condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return BaseOption
     */
    public function getBaseOption(): BaseOption
    {
        return $this->baseOption;
    }

    /**
     * @param BaseOption $baseOption
     */
    public function setBaseOption(BaseOption $baseOption)
    {
        $this->baseOption = $baseOption;
    }

    public function getConditionCalculable(): ?ConditionCalculableInterface
    {
        return $this->condition;
    }

    /**
     * @return RewardCalculableInterface
    */
    public function getRewardCalculable(User $user = null): ?RewardCalculableInterface
    {
        return $this->getReward();
    }
}