<?php
namespace Nss\Bundle\OrderBundle\Entity\Interfaces;

use Nss\Bundle\OrderBundle\Entity\BaseRewardType;

interface RewardCalculableInterface
{
    public function getType() : BaseRewardType;

    public function getValue() : float;
}