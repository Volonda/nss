<?php
namespace Nss\Bundle\OrderBundle\Entity\Interfaces;

use Nss\Bundle\OrderBundle\Entity\BaseConditionType;

interface ConditionCalculableInterface
{
    public function geCalculableType() : BaseConditionType;

    public function getValue() : float;
}