<?php
namespace Nss\Bundle\OrderBundle\Entity\Interfaces;

use Nss\Bundle\UserBundle\Entity\User;

interface RewardAccessibleInterface
{
    public function getRewardCalculable(User $user = null) : ?RewardCalculableInterface;
}