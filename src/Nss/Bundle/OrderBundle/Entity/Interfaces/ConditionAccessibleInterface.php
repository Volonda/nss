<?php
namespace Nss\Bundle\OrderBundle\Entity\Interfaces;

interface ConditionAccessibleInterface
{
    public function getConditionCalculable() : ?ConditionCalculableInterface;
}