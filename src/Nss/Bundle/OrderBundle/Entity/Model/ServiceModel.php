<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseService;
use Nss\Bundle\AppBundle\Validator\Constraints as AppAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="model_services", schema="public")
 */
class ServiceModel
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var BaseService
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseService")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", nullable=false)
     */
    private $service;

    /**
     * @var OrderModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\OrderModel", inversedBy="services")
     * @ORM\JoinColumn(name="order_model_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * @var ServiceModelFormType
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\ServiceModelFormType", inversedBy="serviceModels")
     * @ORM\JoinColumn(name="service_form_type_id", referencedColumnName="id")
     */
    private $formType;

    /**
     * @var Collection
     * @Assert\Valid()
     * @AppAssert\UniquePropertyCollection(property="option.id", message="Одна и та же опция выбрана несколько раз")
     * @ORM\OneToMany(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\OptionModel", mappedBy="service", cascade={"persist"})
     *
     */
    private $options;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return BaseService
     */
    public function getService() : ?BaseService
    {
        return $this->service;
    }

    /**
     * @param BaseService $service
     */
    public function setService(?BaseService $service)
    {
        $this->service = $service;
    }

    /**
     * @return OrderModel
     */
    public function getOrder() : ?OrderModel
    {
        return $this->order;
    }

    /**
     * @param OrderModel $order
     */
    public function setOrder(?OrderModel $order)
    {
        $this->order = $order;
    }

    /**
     * @return null|ServiceModelFormType
     */
    public function getFormType() : ?ServiceModelFormType
    {
        return $this->formType;
    }

    /**
     * @param ServiceModelFormType $formType
     */
    public function setFormType(ServiceModelFormType $formType)
    {
        $this->formType = $formType;
    }

    public function getTitle() : ?string
    {
        return $this->getService()->getName();
    }

    public function getFullTitle() : ?string
    {
        return $this->getService()->getName() . ' ('. $this->getOrder()->getTitle() .')';
    }

    /**
     * @return Collection
     */
    public function getOptions() : Collection
    {
        return $this->options;
    }

    /**
     * @param Collection $options
     */
    public function setOptions($options)
    {
        $service = $this;

        if (is_array($options)) {
            $options = new ArrayCollection($options);
        }

        $options->forAll(function ($num, OptionModel $option) use ($service){
            if (!$option->getService()) {
                $option->setService($service);
            }

            return true;
        });
        $this->options = $options;
    }

    public function addOption(OptionModel $option)
    {
        $option->setService($this);
        $this->options->add($option);
    }
}