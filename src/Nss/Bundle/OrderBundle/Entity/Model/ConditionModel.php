<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseConditionType;
use Nss\Bundle\OrderBundle\Entity\Interfaces\ConditionCalculableInterface;


/**
 * @ORM\Entity()
 * @ORM\Table(name="model_conditions", schema="public")
 */
class ConditionModel implements ConditionCalculableInterface
{

    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BaseConditionType
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseConditionType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     */
    private $type;


    /**
     * @var int
     *
     * @ORM\Column(name="value", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    private $name;

    public function __construct()
    {
        $this->value = 0;
        $this->name = 'нет';
    }

    /**
     * @return int
     */
    public function getId() :? int
    {
        return $this->id;
    }

    /**
     * @return BaseConditionType
     */
    public function getType() : ?BaseConditionType
    {
        return $this->type;
    }

    /**
     * @return BaseConditionType
     */
    public function geCalculableType() : BaseConditionType
    {
        return $this->type;
    }

    /**
     * @param BaseConditionType $type
     */
    public function setType(BaseConditionType $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getValue() : float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
}