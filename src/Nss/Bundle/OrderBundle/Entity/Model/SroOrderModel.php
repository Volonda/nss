<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;
use Nss\Bundle\DocumentBundle\Entity\TemplateList;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel;

/**
 * @ORM\Entity(repositoryClass="Nss\Bundle\OrderBundle\Repository\SroOrderModelRepository")
 * @ORM\Table(name="model_sro_orders", schema="public")
 */
class SroOrderModel extends OrderModel
{
    /**
     * @var Sro
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\SroBundle\Entity\Sro")
     * @ORM\JoinColumn(name="sro_id", referencedColumnName="id", nullable=true)
     */
    private $sro;

    /**
     * @var  TemplateOrderModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_rewrite", type="boolean")
     */
    private $rewrite;

    public function __construct()
    {
        parent::__construct();

        $this->rewrite = false;
    }

    /**
     * @return Sro
     */
    public function getSro() :? Sro
    {
        return $this->sro;
    }

    /**
     * @param Sro $sro
     */
    public function setSro(Sro $sro)
    {
        $this->sro = $sro;
    }

    /**
     * @return TemplateOrderModel
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param TemplateOrderModel $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return bool
     */
    public function isRewrite(): bool
    {
        return $this->rewrite;
    }

    /**
     * @param bool $rewrite
     */
    public function setRewrite(bool $rewrite)
    {
        $this->rewrite = $rewrite;
    }

    public function getTitle() :string
    {
        return $this->template->getTitle();
    }

    /**
     * @return WorkflowModel
     */
    public function getWorkflowModel(): ?WorkflowModel
    {
        return $this->template->getWorkflowModel();
    }

    /**
     * @return BaseOrderType
     */
    public function getType(): ?BaseOrderType
    {
        return $this->template->getType();
    }

    public function getTypeName() : string
    {
        return $this->getType()->getName();
    }

    public function getActualServices()
    {
        if (!$this->isRewrite()) {
            return $this->template->getServices();
        } else {
            return $this->getServices();
        }
    }

    /**
     * @return TemplateList
     */
    public function getActualInputDocumentTemplateList(): ?TemplateList
    {
        if (!$this->isRewrite()) {
            return $this->template->getInputDocumentTemplateList();
        } else {
            return $this->getInputDocumentTemplateList();
        }
    }

    /**
     * @return TemplateList
     */
    public function getActualOutputDocumentTemplateList(): ?TemplateList
    {
        if (!$this->isRewrite()) {
            return $this->template->getOutputDocumentTemplateList();
        } else {
            return $this->getOutputDocumentTemplateList();
        }
    }

    /**
     * @return String
    */
    public function getActualInputDocumentTemplateListName(): string
    {
        if ($this->getActualInputDocumentTemplateList()) {

            return $this->getActualInputDocumentTemplateList()->getName();
        } else {

            return '';
        }
    }

    /**
     * @return String
     */
    public function getActualOutputDocumentTemplateListName(): string
    {
        if($this->getActualOutputDocumentTemplateList()) {

            return $this->getActualOutputDocumentTemplateList()->getName();

        } else {
            return '';
        }
    }

    /**
     * @return BaseOrderStatus
    */
    public function getStatus(): ?BaseOrderStatus
    {
        return $this->getTemplate()->getStatus();
    }

    public function __toString()
    {
        return $this->getTitle();
    }
}