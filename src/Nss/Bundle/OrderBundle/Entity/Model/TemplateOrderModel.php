<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity( repositoryClass="Nss\Bundle\OrderBundle\Repository\TemplateOrderModelRepository")
 * @ORM\Table(name="model_template_orders", schema="public")
 */
class TemplateOrderModel extends OrderModel
{

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var BaseOrderType
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     */
    private $type;

    /**
     * @var WorkflowModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel", inversedBy="orderModels")
     * @ORM\JoinColumn(name="workflow_model_id", referencedColumnName="id")
     */
    private $workflowModel;

    /**
     * @var BaseOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOrderStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $status;

    /**
     * @return WorkflowModel
     */
    public function getWorkflowModel(): ?WorkflowModel
    {
        return $this->workflowModel;
    }

    /**
     * @param WorkflowModel $workflowModel
     */
    public function setWorkflowModel(WorkflowModel $workflowModel)
    {
        $this->workflowModel = $workflowModel;
    }

    /**
     * @return string
     */
    public function getTitle() : ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return BaseOrderType
     */
    public function getType() :? BaseOrderType
    {
        return $this->type;
    }

    /**
     * @param BaseOrderType $type
     */
    public function setType(BaseOrderType $type)
    {
        $this->type = $type;
    }

    /**
     * @return BaseOrderStatus
     */
    public function getStatus(): ?BaseOrderStatus
    {
        return $this->status;
    }

    /**
     * @param BaseOrderStatus $status
     */
    public function setStatus(BaseOrderStatus $status)
    {
        $this->status = $status;
    }

    public function __toString()
    {
        return (string)$this->title;
    }
}