<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseRewardType;
use Nss\Bundle\OrderBundle\Entity\Interfaces\RewardCalculableInterface;


/**
 * @ORM\Entity()
 * @ORM\Table(name="model_rewards", schema="public")
 */
class RewardModel implements RewardCalculableInterface
{

    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BaseRewardType
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseRewardType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     */
    private $type;


    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    private $name;


    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return BaseRewardType
     */
    public function getType() : BaseRewardType
    {
        return $this->type;
    }

    /**
     * @param BaseRewardType $type
     */
    public function setType(BaseRewardType $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getValue() :float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

}