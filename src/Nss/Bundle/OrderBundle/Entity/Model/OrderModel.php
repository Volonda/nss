<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\DocumentBundle\Entity\TemplateList;
use Nss\Bundle\OrderBundle\Entity\OrderType;
use Nss\Bundle\AppBundle\Validator\Constraints as AppAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="model_type", type="string")
 * @ORM\DiscriminatorMap({"template" = "TemplateOrderModel", "sro" = "SroOrderModel"})
 * @ORM\Table(name="model_orders", schema="public")
 */
abstract class OrderModel
{

    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Collection
     * @Assert\Valid()
     * @AppAssert\UniquePropertyCollection(property="service.id", message="Одна и та же услуга выбрана несколько раз")
     * @ORM\OneToMany(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\ServiceModel", mappedBy="order", cascade={"persist"})
     */
    private $services;

    /**
     * @var TemplateList
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\TemplateList")
     * @ORM\JoinColumn(name="input_document_template_list_id", referencedColumnName="id")
     */
    private $inputDocumentTemplateList;

    /**
     * @var TemplateList
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\DocumentBundle\Entity\TemplateList")
     * @ORM\JoinColumn(name="output_document_template_list_id", referencedColumnName="id")
     */
    private $outputDocumentTemplateList;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;

    public function __construct()
    {
        $this->services = new ArrayCollection();
        $this->enabled = true;
    }

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return Collection
     */
    public function getServices() : Collection
    {
        return $this->services;
    }

    /**
     * @param Collection $services
     */
    public function setServices(Collection $services)
    {
        $order = $this;

        $services->forAll(function ($num, ServiceModel $service) use ($order) {
            if(!$service->getOrder()) {
                $service-> setOrder($order);
            }
            return true;
        });

        $this->services = $services;
    }

    /**
     * @return TemplateList
     */
    public function getInputDocumentTemplateList(): ?TemplateList
    {
        return $this->inputDocumentTemplateList;
    }

    /**
     * @param TemplateList $inputDocumentTemplateList
     */
    public function setInputDocumentTemplateList(?TemplateList $inputDocumentTemplateList)
    {
        $this->inputDocumentTemplateList = $inputDocumentTemplateList;
    }

    /**
     * @return TemplateList
     */
    public function getOutputDocumentTemplateList(): ?TemplateList
    {
        return $this->outputDocumentTemplateList;
    }

    /**
     * @param TemplateList $outputDocumentTemplateList
     */
    public function setOutputDocumentTemplateList(?TemplateList $outputDocumentTemplateList)
    {
        $this->outputDocumentTemplateList = $outputDocumentTemplateList;
    }

    public function addService(ServiceModel $service)
    {
        $service->setOrder($this);
        $this->services->add($service);
    }
    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }



}