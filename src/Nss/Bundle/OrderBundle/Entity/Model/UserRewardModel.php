<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\UserBundle\Entity\User;

/**
 * @ORM\Entity()
 * @ORM\Table(name="model_user_rewards", schema="public")
 */
class UserRewardModel
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var RewardModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\RewardModel")
     * @ORM\JoinColumn(name="reward_model_id", referencedColumnName="id")
     */
    private $rewardModel;

    /**
     * @var OptionModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\OptionModel", inversedBy="userRewardModels")
     * @ORM\JoinColumn(name="option_model_id", referencedColumnName="id")
     */
    private $optionModel;


    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return RewardModel
     */
    public function getRewardModel(): RewardModel
    {
        return $this->rewardModel;
    }

    /**
     * @param RewardModel $rewardModel
     */
    public function setRewardModel(RewardModel $rewardModel)
    {
        $this->rewardModel = $rewardModel;
    }

    /**
     * @return OptionModel
     */
    public function getOptionModel() : ?OptionModel
    {
        return $this->optionModel;
    }

    /**
     * @param OptionModel $optionModel
     */
    public function setOptionModel(OptionModel $optionModel)
    {
        $this->optionModel = $optionModel;
    }
}