<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseOption;
use Nss\Bundle\OrderBundle\Entity\Interfaces\ConditionAccessibleInterface;
use Nss\Bundle\OrderBundle\Entity\Interfaces\ConditionCalculableInterface;
use Nss\Bundle\OrderBundle\Entity\Interfaces\RewardAccessibleInterface;
use Nss\Bundle\OrderBundle\Entity\Interfaces\RewardCalculableInterface;
use Nss\Bundle\UserBundle\Entity\User;


/**
 * @ORM\Entity(repositoryClass="Nss\Bundle\OrderBundle\Repository\OptionModelRepository")
 * @ORM\Table(name="model_options", schema="public")
 */
class OptionModel implements RewardAccessibleInterface, ConditionAccessibleInterface
{

    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ServiceModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\ServiceModel", inversedBy="options")
     * @ORM\JoinColumn(name="service_model_id", referencedColumnName="id", nullable=false)
     */
    private $service;

    /**
     * @var BaseOption
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseOption")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id", nullable=false)
     */
    private $option;

    /**
     * @var ConditionModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\ConditionModel")
     * @ORM\JoinColumn(name="condition_model_id", referencedColumnName="id", nullable=false)
     */
    private $condition;

    /**
     * @var RewardModel
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\RewardModel")
     * @ORM\JoinColumn(name="reward_model_id", referencedColumnName="id", nullable=false)
     */
    private $rewardModel;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\UserRewardModel", mappedBy="optionModel")
     */
    private $userRewardModels;

    public function __construct()
    {
        $this->userRewardModels = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return ServiceModel
     */
    public function getService() : ?ServiceModel
    {
        return $this->service;
    }

    /**
     * @param ServiceModel $service
     */
    public function setService(?ServiceModel $service)
    {
        $this->service = $service;
    }

    /**
     * @return BaseOption
     */
    public function getOption() : ?BaseOption
    {
        return $this->option;
    }

    /**
     * @param BaseOption $option
     */
    public function setOption(?BaseOption $option)
    {
        $this->option = $option;
    }

    /**
     * @return null|ConditionModel
     */
    public function getCondition(): ?ConditionModel
    {
        return $this->condition;
    }

    /**
     * @param ConditionModel $condition
     */
    public function setCondition(?ConditionModel $condition)
    {
        $this->condition = $condition;
    }

    public function getConditionCalculable(): ConditionCalculableInterface
    {
        return $this->condition;
    }

    /**
     * @return RewardModel
     */
    public function getRewardModel(): ?RewardModel
    {
        return $this->rewardModel;
    }

    /**
     * @param RewardModel $rewardModel
     */
    public function setRewardModel(?RewardModel $rewardModel)
    {
        $this->rewardModel = $rewardModel;
    }

    /**
     * @return ArrayCollection
     */
    public function getUserRewardModels(): ArrayCollection
    {
        return $this->userRewardModels;
    }

    /**
     * @param ArrayCollection $userRewardModels
     */
    public function setUserRewardModels(ArrayCollection $userRewardModels)
    {
        $this->userRewardModels = $userRewardModels;
    }

    /**
     * @param User $user
     * @return RewardModel
    */
    public function getActualRewardModel(User $user = null) : ?RewardModel
    {
        if ($user) {
            $criteria = Criteria::create();
            $expr = Criteria::expr();

            $criteria->where($expr->eq('user', $user));
            $criteria->andWhere($expr->eq('optionModel', $this));

            $criteria->setMaxResults(1);
            $rewardModels = clone $this->userRewardModels;

            $userRewardModelCollection = $rewardModels->matching($criteria);

            if ($userRewardModelCollection->count()) {
                /** @var UserRewardModel $userRewardModel */
                $userRewardModel = $userRewardModelCollection->first();

                return $userRewardModel->getRewardModel();
            } else {

                return $this->rewardModel;
            }
        } else {

            return $this->rewardModel;
        }
    }

    /**
     * @param User $user
     * @return RewardCalculableInterface
    */
    public function getRewardCalculable(User $user = null) : ?RewardCalculableInterface
    {
        return $this->getActualRewardModel($user);
    }
}