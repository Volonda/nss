<?php
namespace Nss\Bundle\OrderBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\OrderBundle\Entity\BaseFormType;

/**
 * @ORM\Entity()
 * @ORM\Table(name="model_service_form_types", schema="public")
 */
class ServiceModelFormType
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id; 

    /**
     * @var BaseFormType
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\OrderBundle\Entity\BaseFormType")
     * @ORM\JoinColumn(name="form_type_id", referencedColumnName="id", nullable=false)
     */
    private $formType;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\OrderBundle\Entity\Model\ServiceModel", mappedBy="formType")
     */
    private $serviceModels;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="text", nullable=true)
     */
    private $options;

    /**
     * Код
     * @var string
     *
     * @ORM\Column(name="code", type="text", nullable=false)
     */
    private $code;

    /**
     * Заголовок
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=false)
     */
    private $title;

    public function __construct()
    {
        $this->serviceModels = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return BaseFormType
     */
    public function getFormType(): BaseFormType
    {
        return $this->formType;
    }

    /**
     * @param BaseFormType $formType
     */
    public function setFormType(BaseFormType $formType)
    {
        $this->formType = $formType;
    }

    /**
     * @return Collection
     */
    public function getServiceModels(): Collection
    {
        return $this->serviceModels;
    }

    /**
     * @param Collection $serviceModels
     */
    public function setServiceModels(Collection $serviceModels)
    {
        $this->serviceModels = $serviceModels;
    }

    /**
     * @return string
     */
    public function getOptions() : ?string
    {
        return $this->options;
    }

    /**
     * @param string $options
     */
    public function setOptions(?string $options)
    {
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getCode() : ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getTitle() : ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }
}