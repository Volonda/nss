<?php

namespace Nss\Bundle\OrderBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Nss\Bundle\AdminBundle\Admin\Sro;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuBuilder
{
    /**
     * @var EntityManager
    */
    private $em;

    /** @var FactoryInterface*/
    private $factory;

    /** @var Request*/
    private $request;

    /**
     * @param EntityManager $em
     * @param FactoryInterface $factory
     * @param RequestStack $requestStack
    */
    public function __construct(EntityManager $em, FactoryInterface $factory, RequestStack $requestStack)
    {
        $this->factory = $factory;
        $this->em = $em;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @return ItemInterface
    */
    public function tabs() : ItemInterface
    {
        $orderModels = $this->em->createQueryBuilder()
            ->select()
            ->select('om')
            ->from(SroOrderModel::class, 'om')
            ->join('om.sro','s')
            ->where('s.id = :sroId')
            ->andWhere('om.enabled = TRUE')
            ->setParameter('sroId', $this->request->get('sro_id'))
            ->getQuery()
            ->getResult()
        ;

        $menu = $this->factory->createItem('root');
        $menu->isRoot();

        /** @var SroOrderModel $orderModel*/
        foreach ($orderModels as $orderModel) {


            $menuItem = $menu->addChild($orderModel->getTitle(), [
                'route' => 'order_create_type',
                'routeParameters' => [
                    'sro_id' => $this->request->get('sro_id'),
                    'organization_id' => $this->request->get('organization_id'),
                    'order_model_id' => $orderModel->getId()
                ]
            ]);

            if ($orderModel->getId() == $this->request->get('order_model_id')) {
                $menuItem
                    ->setAttribute('class','active')
                    ->isCurrent()
                ;
            }
        }

        return $menu;
    }
}