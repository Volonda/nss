<?php
namespace Nss\Bundle\OrderBundle\Controller;


use Nss\Bundle\CommentBundle\Form\Type\OrderCommentType;

use Nss\Bundle\HistoryBundle\Manager\HistoryManager;
use Nss\Bundle\MessageBundle\Form\Type\MessageType;
use Nss\Bundle\MessageBundle\Form\Type\OrderMessageType;
use Nss\Bundle\MessageBundle\Manager\MessageManager;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\CommentBundle\Manager\CommentManager;
use Nss\Bundle\OrderBundle\Repository\ConditionModelRepository;
use Nss\Bundle\OrderBundle\Repository\RewardModelRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/rest")
*/
class RestController extends Controller
{
    /**
     * @Route("/history/{id}", name="order_rest_history")
     * @Template()
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     *
     * @return array
     */
    public function history(Order $order) : array
    {
        /** @var HistoryManager $manager*/
        $manager = $this->get('history.manager.history_manager');

        return [
            'records' => $manager->getByOrder($order)
        ];
    }


    /**
     * @Route("/message/{id}", name="order_rest_message")
     * @Template()
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function message(Order $order) : array
    {
        $form = $this->createForm(OrderMessageType::class, null, [
            'action' => $this->generateUrl('order_message_create', [
                'id' => $order->getId()
            ])
        ]);

        /** @var MessageManager $manager*/
        $manager = $this->get('message.manager.message_manager');

        return [
            'messages' => $manager->getOrderMessages($order),
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/comment/{id}", name="order_rest_comment")
     * @Template()
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function comment(Order $order) : array
    {
        $form = $this->createForm(OrderCommentType::class, null, [
            'action' => $this->generateUrl('order_comment_create', [
                'id' => $order->getId()
            ])
        ]);

        /** @var CommentManager $manager*/
        $manager = $this->get('comment.manager.comment_manager');

        return [
            'comments' => $manager->getOrderComment($order, $this->getUser()),
            'form' => $form->createView()
        ];
    }
}