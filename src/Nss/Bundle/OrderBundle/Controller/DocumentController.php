<?php
namespace Nss\Bundle\OrderBundle\Controller;

use CheckerBundle\Factory\CheckerFactory;
use Monolog\Logger;

use Nss\Bundle\DocumentBundle\Entity\Document;
use Nss\Bundle\DocumentBundle\Entity\DocumentVersion;
use Nss\Bundle\DocumentBundle\Form\Type\BundleType;
use Nss\Bundle\DocumentBundle\Manager\VersionManager;
use Nss\Bundle\FileBundle\Downloader\FileDownloader;
use Nss\Bundle\FileBundle\Entity\File;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrderBundle\Manager\DocumentManager;
use Nss\Bundle\WorkflowBundle\Checker\WorkflowChecker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @Route("/documents")
 */
class DocumentController extends Controller
{
    /**
     * @Route("/input/view/{id}", requirements={"id": "\d+"}, name="order_document_input_view")
     * @Security("is_granted('allow', 'action:order_view_my')")
     * @Template("@NssOrder/Document/view.html.twig")
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function inputViewAction(Order $order)
    {
        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if (!$checker->isWorkflowActionAllowed($order, 'order_view_my'))
        {
            throw new NotFoundHttpException();
        }

        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Заявки','order_my');
        $breadcrumbs->addRouteItem('Заявка #' . $order->getId(), 'order_view', ['id' => $order->getId()]);
        $breadcrumbs->addItem('Документы');

        return [
            'order' => $order,
            'documents' => ($order->getInputDocumentBundle())?$order->getInputDocumentBundle()->getDocuments():[],
            'title' => 'Входящие документы заявления',
            'type' => Order::DOCUMENT_BUNDLE_INPUT
        ];
    }

    /**
     * @Route("/output/view/{id}", requirements={"id": "\d+"}, name="order_document_output_view")
     * @Security("is_granted('allow', 'action:order_view_my')")
     * @Template("@NssOrder/Document/view.html.twig")
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function outputViewAction(Order $order)
    {
        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if (!$checker->isWorkflowActionAllowed($order, 'order_view_my'))
        {
            throw new NotFoundHttpException();
        }

        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Заявки','order_my');
        $breadcrumbs->addRouteItem('Заявка #' . $order->getId(), 'order_view', ['id' => $order->getId()]);
        $breadcrumbs->addItem('Документы');

        return [
            'order' => $order,
            'documents' => ($order->getOutputDocumentBundle())?$order->getOutputDocumentBundle()->getDocuments():[],
            'title' => 'Исходящие документы заявления',
            'type' => Order::DOCUMENT_BUNDLE_OUTPUT
        ];
    }

    /**
     * @Route("/input/edit/{id}", requirements={"id": "\d+"}, name="order_document_input_edit")
     * @Security("is_granted('allow', 'action:order_edit_my')")
     * @Template("@NssOrder/Document/edit.html.twig")
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function inputEditAction(Order $order)
    {
        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if( !$checker->isWorkflowActionAllowed($order, 'order_edit_my')){
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(BundleType::class,null,[
            'documentBundle' => $order->getInputDocumentBundle(),
            'order' => $order,
            'type' => Order::DOCUMENT_BUNDLE_INPUT
        ]);

        return [
            'order' => $order,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/upload/{orderId}/{documentId}/{type}",
     *     requirements={
     *          "orderId": "\d+",
     *          "documentId": "\d+",
     *          "type": "(input|output)"
     *     },
     *     name="order_document_upload")
     * @Method("POST")
     * @Security("is_granted('allow', 'action:order_edit_my') or is_granted('allow', 'action:moderation')")
     * @ParamConverter("order", class="NssOrderBundle:Order", options={"id":"orderId"})
     * @ParamConverter("document", class="NssDocumentBundle:Document", options={"id":"documentId"})
     * @param Order $order
     * @param Request $request
     * @param Document $document
     * @param string $type
     * @return Response
     */
    public function uploadAction(Request $request, Order $order, Document $document, string $type)
    {
        if (
            $type == Order::DOCUMENT_BUNDLE_INPUT
            && $document->getBundle()->getId() != $order->getInputDocumentBundle()->getId()
        ) {
            throw new NotFoundHttpException();
        } elseif(
            $type == Order::DOCUMENT_BUNDLE_OUTPUT
            && $document->getBundle()->getId() != $order->getOutputDocumentBundle()->getId()
        ) {
            throw new NotFoundHttpException();
        }

        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if(
            !$checker->isActionAllowed($order, 'moderation')
            && !$checker->isActionAllowed($order, 'order_edit_my')
        ){
            throw new NotFoundHttpException();
        }

        /** @var Logger $logger*/
        $logger = $this->get('logger');

        try{
            $uploadedFiles = $request->files->get('file');

            /**
             * @var DocumentManager $documentManager
             */
            $documentManager = $this->get('order.manager.document_manager');

            $data = $request->get('bundle');
            $comment = (isset($data[$document->getTemplate()->getCode()]['description']))
                ? $data[$document->getTemplate()->getCode()]['description']
                : null
            ;

            $documentManager->upload($document, $uploadedFiles, $comment);

            $this->getDoctrine()->getManager()->flush();

            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent('Файлы загружены');

            $this->addFlash('notice', 'EDIT_SUCCESS');

        } catch (\Exception $e) {

            $response = new Response();
            $response->setStatusCode(500);
            $response->setContent('Во время загрузки файлов произошла ошибка');

            $logger->addCritical($e->getMessage(), [
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }

        return $response;
    }

    /**
     * @Route("/delete/{type}/{orderId}/{versionId}",
     *     requirements={
     *      "versionId": "\d+",
     *      "orderId": "\d+",
     *      "type": "(input|output)",
     *     },
     *     name="order_document_delete")
     * @Method("GET")
     * @Security("is_granted('allow', 'action:order_edit_my') or is_granted('allow', 'action:moderation')")
     * @ParamConverter("order", class="NssOrderBundle:Order", options={"id":"orderId"})
     * @ParamConverter("version", class="NssDocumentBundle:DocumentVersion", options={"id":"versionId"})
     * @param Order $order
     * @param DocumentVersion $version
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Order $order, DocumentVersion $version, $type) : RedirectResponse
    {
        $documentBundle = ($type == Order::DOCUMENT_BUNDLE_OUTPUT)
            ? $order->getOutputDocumentBundle()
            : $order->getInputDocumentBundle()
        ;

        if ( $version->getDocument()->getBundle()->getId() != $documentBundle->getId()) {
            throw new NotFoundHttpException();
        }


        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if(
            !$checker->isActionAllowed($order, 'moderation')
            && !$checker->isActionAllowed($order, 'order_edit_my')
        ){
            throw new NotFoundHttpException();
        }


        /**
         * @var VersionManager $manager
         */
        $manager = $this->get('document.manager.version_manager');
        $manager->delete($version);

        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('notice', 'EDIT_SUCCESS');


        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/download/{type}/{orderId}/{versionId}/{fileId}/{attachment}",
     *     requirements={
     *          "type": "(input|output)",
     *          "orderId": "\d+",
     *          "versionId": "\d+",
     *          "fileId": "\d+",
     *          "attachment": "(0|1)"
     *     },
     *     name="order_document_download")
     * @Security("is_granted('allow', 'action:order_view_my') or is_granted('allow', 'action:moderation_view_all')")
     * @Template()
     * @ParamConverter("order", class="NssOrderBundle:Order", options={"id":"orderId"})
     * @ParamConverter("version", class="NssDocumentBundle:DocumentVersion", options={"id":"versionId"})
     * @ParamConverter("file", class="NssFileBundle:File", options={"id":"fileId"})
     * @param Order $order
     * @param DocumentVersion $version
     * @param File $file
     * @param int $attachment
     * @throws NotFoundHttpException
     * @return Response
     */
    public function downloadAction(Order $order, DocumentVersion $version, File $file, int $attachment = 1, $type) : Response
    {
        $documentBundle = ($type == Order::DOCUMENT_BUNDLE_OUTPUT)
            ? $order->getOutputDocumentBundle()
            : $order->getInputDocumentBundle()
        ;

        if (
            !$version->getFiles()->contains($file)
            || $version->getDocument()->getBundle()->getId() != $documentBundle->getId()
        ) {
             throw new NotFoundHttpException();
        }


        /** @var CheckerFactory $checkerFactory*/
        $checkerFactory = $this->get('checker.factory.checker_factory');
        /** @var  WorkflowChecker $checker*/
        $checker = $checkerFactory->getByName('workflow');

        if(
            !$checker->isActionAllowed($order, 'moderation_view_all')
             && !$checker->isActionAllowed($order, 'order_view_my')
        ){
            throw new NotFoundHttpException();
        }

        /** @var FileDownloader $downloader*/
        $downloader = $this->get('file.downloader.file_downloader');

        return $downloader->createResponse($file, $attachment);
    }
}