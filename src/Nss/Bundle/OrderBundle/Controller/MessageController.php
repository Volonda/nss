<?php
namespace Nss\Bundle\OrderBundle\Controller;

use Nss\Bundle\MessageBundle\Form\Type\MessageType;
use Nss\Bundle\MessageBundle\Form\Type\OrderMessageType;
use Nss\Bundle\MessageBundle\Manager\MessageManager;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrderBundle\Repository\ConditionModelRepository;
use Nss\Bundle\OrderBundle\Repository\RewardModelRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @Route("/message")
*/
class MessageController extends Controller
{
    /**
     * @Route("/create/{id}", name="order_message_create")
     * @Template()
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request, Order $order) : RedirectResponse
    {
        $form = $this->createForm(OrderMessageType::class, null, [
            'action' => $this->generateUrl('order_message_create', [
                'id' => $order->getId()
            ])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**@var MessageManager $manager*/
            $manager = $this->get('message.manager.message_manager');

            $manager->createOrderMessage($this->getUser(), $form->getData(), $order);

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('notice', 'EDIT_SUCCESS');
        }

        if ($this->isGranted('allow','action:order_view_my')) {
            $route = 'order_view';
        } else {
            $route = 'moderation_view';
        }

        $url = $this->generateUrl($route, [
            'id' => $order->getId()
        ]);

        return $this->redirect($url . '#messages');
    }
}