<?php
namespace Nss\Bundle\OrderBundle\Controller;

use CheckerBundle\Factory\CheckerFactory;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\WorkflowBundle\Checker\WorkflowChecker;
use Nss\Bundle\WorkflowBundle\Entity\Transition;
use Nss\Bundle\WorkflowBundle\Lead\WorkflowLead;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/workflow")
*/
class WorkflowController extends Controller
{
    /**
     * @Route("/{order_id}/{transition_id}", name="order_workflow_transition",
     *     requirements={"transition_id": "\d+", "order_id": "\d+"})
     * @param Transition $transition
     * @param Order $order
     * @param Request $request
     * @ParamConverter("order", class="NssOrderBundle:Order", options={"id":"order_id"})
     * @ParamConverter("transition", class="NssWorkflowBundle:Transition", options={"id":"transition_id"})
     * @return RedirectResponse
     */
    public function transitionAction(Request $request, Transition $transition, Order $order )
    {
        /** @var CheckerFactory $factory*/
        $factory = $this->get('checker.factory.checker_factory');

        /** @var WorkflowLead $workflowLead*/
        $workflowLead = $this->get('workflow.lead.workflow_lead');

        if ($factory->getByName(WorkflowChecker::NAME)->isTransitional($order, $transition)) {
            $workflowLead->transition($order, $transition);

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('notice', 'EDIT_SUCCESS');
        }

        $url = $request->headers->get("referer");

        return $this->redirect($url);
    }
}