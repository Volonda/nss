<?php
namespace Nss\Bundle\OrderBundle\Controller;

use ITSymfony\Bundle\DataTableBundle\Filter\Filter;

use Nss\Bundle\DocumentBundle\Form\Type\BundleType;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrderBundle\Manager\OrderManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;


class IndexController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/my", name="order_my")
     * @Method("GET")
     * @Security("is_granted('allow', 'action:order_view_my')")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Заявки');

        /** @var OrderManager $manager*/
        $manager = $this->get('order.manager.order_manager');

        /* @var $filter Filter */
        $filter = $this->get('it_symfony.data_table.filter')->getFilter($request, $manager->getFilters());

        $pagination = $manager->getMyPaginate(
            $this->getUser(),
            $request->query->get('page', 1),
            self::PAGE_LIMIT, $filter
        );

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @Route("/view/{id}", requirements={"id": "\d+"}, name="order_view")
     * @Method("GET")
     * @Security("is_granted('allow', 'action:order_view_my')")
     * @Template()
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @return array
     */
    public function viewAction(Order $order) : array
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addRouteItem('Заявки', 'order_my');
        $breadcrumbs->addItem('Заявка #' . $order->getId());

        return [
            'order' => $order
        ];
    }
}
