<?php
namespace Nss\Bundle\OrderBundle\Controller;

use Nss\Bundle\OrderBundle\Calculator\ConditionCalculator;
use Nss\Bundle\OrderBundle\Calculator\RewardCalculator;
use Nss\Bundle\OrderBundle\Entity\Model\OptionModel;
use Nss\Bundle\OrderBundle\Repository\ConditionModelRepository;
use Nss\Bundle\OrderBundle\Repository\OptionModelRepository;
use Nss\Bundle\OrderBundle\Repository\RewardModelRepository;
use Nss\Bundle\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/calculator")
*/
class CalculatorController extends Controller
{
    /**
     * @Route("/option-model", name="calculator_option_model")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function optionModelAction(Request $request)
    {
        $em =  $this->getDoctrine()->getManager();

        $idList = (array)$request->get('id');
        $userId = $request->get('userId');
        $user = $em->getRepository(User::class)->find($userId);

        if (count($idList) && $user instanceof User) {

            /** @var  OptionModelRepository $optionModelRepo */
            $optionModelRepo = $this->getDoctrine()->getManager()->getRepository(OptionModel::class);
            $optionsWithCondition = $optionModelRepo->getConditionByIdList($idList);
            $optionsWithReward = $optionModelRepo->getRewardByIdList($idList);

            $rewardCalculator = new RewardCalculator();
            $conditionCalculator = new ConditionCalculator();

            $data = [
                'totalRewards' => $rewardCalculator->getTotal($optionsWithReward, $user),
                'totalCondition' => $conditionCalculator->getTotal($optionsWithCondition)
            ];
        } else {
            $data = [
                'totalRewards' => 0,
                'totalCondition' => 0
            ];
        }

        return JsonResponse::create($data);
    }
}