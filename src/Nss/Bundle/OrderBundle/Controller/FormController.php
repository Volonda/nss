<?php
namespace Nss\Bundle\OrderBundle\Controller;

use ITSymfony\Bundle\SeldonBundle\Api\SeldonApi;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\OrderBundle\Event\OrderCreatedEvent;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\OrganizationBundle\Manager\OrganizationManager;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\OrderBundle\Form\Type\CreateType;
use Nss\Bundle\OrderBundle\Manager\ModelManager;
use Nss\Bundle\OrderBundle\Manager\OrderManager;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\OrganizationBundle\Repository\OrganizationRepository;
use Nss\Bundle\ContactBundle\Manager\ContactManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Nss\Bundle\OrderBundle\Form\Type\OrderType;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @Route("/form")
*/
class FormController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/", name="order_create")
     * @Template()
     * @Security("is_granted('allow', 'action:order_create')")
     * @param Request $request
     * @return array| RedirectResponse
     */
    public function createAction(Request $request)
    {
        $data['inn'] = $request->get('inn', false);

        if ($sroId = $request->get('sro', false)){
            /** @var Sro $sro*/
            $sro = $this->getDoctrine()->getRepository(Sro::class)->find($sroId);
            $sroType = $sro->getSroType();

            $data = [
                'sro' => $sro,
                'sroType' => $sroType
            ];
        }

        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Завяления','order_my');
        $breadcrumbs->addItem('Новое');

        /** @var ModelManager $modelManager*/
        $modelManager = $this->get('order.manager.model_manager');

        $form = $this->createForm(CreateType::class, $data, [
            'attr' => [
                'id' => 'order-create'
            ]
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $form->get('submit')->isClicked()) {

            /** @var OrganizationManager $organizationManager*/
            $organizationManager = $this->get('organization.manager.organization_manager');
            $organization = $organizationManager->getByInnOrCreate($form->get('inn')->getData());

            /** @var Sro $sro*/
            $sro = $form->get('sro')->getData();


            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('order_create_type', [
                'organization_id' => $organization->getId(),
                'sro_id' => $sro->getId(),
                'order_model_id' => $modelManager->getDefaultOrderModel($sro)->getId()
            ]);
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/type/{sro_id}/{organization_id}/{order_model_id}", name="order_create_type",
     *     requirements={"sro_id":"\d+", "organization_id": "\d+", "order_model_id": "\d+"})
     * @Template()
     * @Security("is_granted('allow', 'action:order_create')")
     * @param Organization $organization
     * @param Sro $sro
     * @param SroOrderModel $sroOrderModel
     * @param Request $request
     * @ParamConverter("organization", class="NssOrganizationBundle:Organization", options={"id":"organization_id"})
     * @ParamConverter("sro", class="NssSroBundle:Sro", options={"id":"sro_id"})
     * @ParamConverter("sroOrderModel", class="Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel", options={"id":"order_model_id"})
     * @return array|RedirectResponse
     */
    public function typeAction(Request $request, Organization $organization, Sro $sro, SroOrderModel $sroOrderModel)
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Завяления','order_my');
        $breadcrumbs->addRouteItem('Новое','order_create');
        $breadcrumbs->addItem($sroOrderModel->getType()->getName());

        /** @var OrderManager $orderManager*/
        $orderManager = $this->get('order.manager.order_manager');

        /** @var ContactManager $contactManager*/
        $contactManager = $this->get('contact.manager.contact_manager');

        $form = $this->createForm(OrderType::class, null, [
            'serviceModels' => $sroOrderModel->getActualServices(),
            'organization' => $organization,
            'user' => $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->get('contact')->getData();

            if (!$contact){
                $contact = $form->get('orderContact')->getData();
                $contactManager->createForOrganization($contact, $this->getUser(), $organization);
            }

            $serviceModel = $form->get('services')->getData();

            $order = $orderManager->create($sroOrderModel, $serviceModel, $sro, $contact, $this->getUser());

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('notice','EDIT_SUCCESS');


            if ($form->get('submit_and_document')->isClicked()){
                return $this->redirectToRoute('order_document_input_edit', [
                    'id' => $order->getId()
                ]);
            } else {
                return $this->redirectToRoute('order_view', [
                    'id' => $order->getId()
                ]);
            }
        }

        return [
            'form' => $form->createView(),
            'organization' => $organization,
            'sro' => $sro
        ];
    }

    /**
     * @Route("/edit/{id}", name="order_form_edit",
     *     requirements={"id": "\d+"})
     * @Template()
     * @Security("is_granted('allow', 'action:order_edit_my')")
     * @ParamConverter("order", class="Nss\Bundle\OrderBundle\Entity\Order")
     * @param Request $request
     * @param Order $order
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Order $order)
    {
        $orderModel = $order->getOrderModel();

        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('Завяления','order_my');
        $breadcrumbs->addRouteItem( '#' . $order->getId(), 'order_view', [
            'id' => $order->getId()
        ]);
        $breadcrumbs->addItem('Редактирование');

        /** @var OrderManager $orderManager*/
        $orderManager = $this->get('order.manager.order_manager');

        /** @var ContactManager $contactManager*/
        $contactManager = $this->get('contact.manager.contact_manager');

        $form = $this->createForm(OrderType::class, null, [
            'serviceModels' => $orderModel->getActualServices(),
            'services' => $order->getServices(),
            'contact' => $order->getContact(),
            'user' => $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->get('contact')->getData();

            if (!$contact){
                $contact = $form->get('orderContact')->getData();
                $contactManager->create($contact, $this->getUser(),  $order->getContact()->getOrganization());
            }

            $serviceModel = $form->get('services')->getData();

            $orderManager->update($order, $serviceModel, $this->getUser());

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('notice','EDIT_SUCCESS');

            if ($form->get('submit_and_document')->isClicked()){
                return $this->redirectToRoute('order_document_input_edit', [
                    'id' => $order->getId()
                ]);
            } else {
                return $this->redirectToRoute('order_view', [
                    'id' => $order->getId()
                ]);
            }

        }

        return [
            'form' => $form->createView(),
            'organization' =>  $order->getContact()->getOrganization(),
            'sro' =>  $order->getSro(),
            'order' => $order
        ];
    }
}