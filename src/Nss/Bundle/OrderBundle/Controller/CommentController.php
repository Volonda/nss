<?php
namespace Nss\Bundle\OrderBundle\Controller;

use Nss\Bundle\CommentBundle\Entity\OrderComment;
use Nss\Bundle\CommentBundle\Form\Type\OrderCommentType;
use Nss\Bundle\CommentBundle\Manager\CommentManager;
use Nss\Bundle\OrderBundle\Entity\Order;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/comments")
*/
class CommentController extends Controller
{
    /**
     * @Route("/create/{id}", name="order_comment_create")
     * @ParamConverter("order", class="NssOrderBundle:Order")
     * @param Order $order
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request, Order $order) : RedirectResponse
    {
        $form = $this->createForm(OrderCommentType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**@var CommentManager $commentManager*/
            $commentManager = $this->get('comment.manager.comment_manager');

            $commentManager->createOrderComment($form->getData(), $order, $this->getUser());

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('notice', 'EDIT_SUCCESS');
        }

        if ($this->isGranted('allow','action:order_view_my')) {
            $route = 'order_view';
        } else {
            $route = 'moderation_view';
        }

        $url = $this->generateUrl($route, [
            'id' => $order->getId()
        ]);

        return $this->redirect($url . '#comments');
    }

    /**
     * @Route("/delete/{id}", name="order_comment_delete")
     * @ParamConverter("comment", class="NssCommentBundle:OrderComment")
     * @param OrderComment $comment
     * @return RedirectResponse
     */
    public function delete(OrderComment $comment) : RedirectResponse
    {
        /**@var CommentManager $commentManager*/
        $commentManager = $this->get('comment.manager.comment_manager');
        $commentManager->delete($comment);

        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('notice', 'EDIT_SUCCESS');

        if ($this->isGranted('allow','action:order_view_my')) {
            $route = 'order_view';
        } else {
            $route = 'moderation_view';
        }

        $url = $this->generateUrl($route, [
            'id' => $comment->getOrder()->getId()
        ]);

        return $this->redirect($url . '#comments');
    }
}