<?php
namespace Nss\Bundle\OrderBundle\Calculator;

use Nss\Bundle\OrderBundle\Entity\Interfaces\RewardAccessibleInterface;
use Nss\Bundle\UserBundle\Entity\User;

class RewardCalculator
{
    /**
     * @param $optionCollection
     * @param User $user
     * @return float
     * @throws \Exception
    */
    public function getTotal($optionCollection, User $user) : float
    {
        $totalValue = 0;

         foreach ($optionCollection as $option) {

             if (!$option instanceof RewardAccessibleInterface) {

                 throw new \Exception(get_class($option) . ' is not implementing RewardAccessibleInterface');
             }

             $reward = $option->getRewardCalculable($user);
             if ($reward) {
                 $value = $reward->getValue();

                 switch ($reward->getType()->getCode()) {
                     default:
                         $totalValue += (float)$value;
                         break;
                 }
             }
         }
         return $totalValue;
    }
}
