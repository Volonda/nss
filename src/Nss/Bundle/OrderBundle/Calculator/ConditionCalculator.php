<?php
namespace Nss\Bundle\OrderBundle\Calculator;

use Nss\Bundle\OrderBundle\Entity\Interfaces\ConditionAccessibleInterface;

class ConditionCalculator
{
    public function getTotal($optionCollection) : float
    {
        $totalValue = 0;

        foreach ($optionCollection as $option) {

            if (!$option instanceof ConditionAccessibleInterface) {
                throw new \Exception(get_class($option) . ' is not implementing RewardAccessibleInterface');
            }

            $condition = $option->getConditionCalculable();
            if ($condition) {
                $value = $condition->getValue();

                switch ($condition->geCalculableType()->getCode()) {
                    default:
                        $totalValue += (float)$value;
                        break;
                }
            }
        }
        return $totalValue;
    }
}
