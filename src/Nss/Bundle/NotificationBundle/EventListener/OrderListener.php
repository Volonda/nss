<?php
namespace Nss\Bundle\NotificationBundle\EventListener;

use Nss\Bundle\NotificationBundle\Manager\NotificationManager;
use Nss\Bundle\OrderBundle\Event\OrderCreatedEvent;
use Psr\Log\LoggerInterface;

class OrderListener
{

    /**
     * @var NotificationManager
     */
    private $notificationManager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param NotificationManager $notificationManager
     * @param LoggerInterface $logger
     */
    public function __construct(NotificationManager $notificationManager, LoggerInterface $logger)
    {
        $this->notificationManager = $notificationManager;
        $this->logger = $logger;
    }

    /**
     * @param OrderCreatedEvent $event
     */
    public function onCreate(OrderCreatedEvent $event)
    {
        try {
            $this->notificationManager->notifyOrderCreate($event->getOrder());
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), ['trace' => $e->getTrace()]);
        }

    }
}