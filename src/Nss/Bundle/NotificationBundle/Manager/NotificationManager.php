<?php
namespace Nss\Bundle\NotificationBundle\Manager;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\NotificationBundle\Entity\Notification;
use Nss\Bundle\NotificationBundle\Model\EmailNotification;
use Nss\Bundle\NotificationBundle\Model\Notifiable;
use Nss\Bundle\OrderBundle\Entity\Order;
use Nss\Bundle\UserBundle\Entity\User;
use Nss\Bundle\UserBundle\Repository\UserRepository;
use Symfony\Component\Templating\EngineInterface;

class NotificationManager
{
    const MAIL_CONTENT_TYPE = 'text/html';
    const ORDER_CREATE_TEMPLATE = '@NssNotification/Emails/notification.html.twig';

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var string
     */
    private $mailerFrom;
    /**
     * @var EngineInterface
     */
    private $engine;

    /**
     * @param EntityManager $entityManager
     * @param \Swift_Mailer $mailer
     * @param string $mailerFrom
     * @param EngineInterface $engine
     */
    public function __construct(
        EntityManager $entityManager,
        \Swift_Mailer $mailer,
        string $mailerFrom,
        EngineInterface $engine)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->mailerFrom = $mailerFrom;
        $this->engine = $engine;
    }

    /**
     * @param User $user
     * @return array|Notification[]
     */
    public function getUnreadNotifications(User $user) :array
    {
        $repository = $this->entityManager->getRepository(Notification::class);
        return $repository->findBy(['readAt' => null, 'user' => $user]);
    }

    /**
     * @param User $user
     * @return array|Notification[]
     */
    public function getNotifications(User $user) :array
    {
        $repository = $this->entityManager->getRepository(Notification::class);
        return $repository->findBy(['user' => $user]);
    }

    /**
     * @param User $user
     * @param string $title
     * @param string $text
     * @return Notification
     */
    public function createNotification(User $user, string $title, string $text) :Notification
    {
        $notification = new Notification($user, $title, $text);
        $this->entityManager->persist($notification);
        return $notification;
    }

    /**
     * @param Notification $notification
     * @return Notification
     */
    public function readNotification(Notification $notification) :Notification
    {
        $notification->setReadAt(new \DateTime());
        return $notification;
    }

    /**
     * @param Notifiable $notification
     * @param string $email
     */
    public function sendNotificationByEmail(Notifiable $notification, string $email)
    {
        $message = (new \Swift_Message($notification->getTitle()))
            ->setFrom($this->mailerFrom)
            ->setTo($email)
            ->setBody($notification->getBody(), self::MAIL_CONTENT_TYPE);

        $this->mailer->send($message);
    }

    /**
     * @param Order $order
     */
    public function notifyOrderCreate(Order $order)
    {
        $userRepository = $this->entityManager->getRepository(User::class);

        try {
            /** @var UserRepository $userRepository*/
            $operators = $userRepository->findAllManagers();
        } catch (OperatorGroupNotFoundException $e) {
            $operators = []; // TODO Логировать, но ситуация рабочая. Не задана группа операторов
        }

        $owner = $order->getOwner();
        $template = self::ORDER_CREATE_TEMPLATE;


        foreach ($operators as $operator) {
            $notification = $this->createNotification($operator, 'Создан новый заказ', 'Создан новый заказ');
            $notification->setSubjectId($order->getId());
            $params = [
                'title' => $notification->getTitle(),
                'body' => $notification->getBody(),
                'order_id' => $order->getId()
            ];
            $this->sendNotificationByEmail($this->buildEmailNotification($notification, $template, $params), $operator->getEmail());
        } 
    }

    /**
     * @param Notifiable $notifiable
     * @param string $template
     * @param array $params
     * @return EmailNotification
     */
    private function buildEmailNotification(Notifiable $notifiable, string $template, array $params)
    {
        $email = new EmailNotification($notifiable);
        $email->setParams($params);
        $email->setTemplate($template);
        $email->setRenderEngine($this->engine);

        return $email;
    }

}