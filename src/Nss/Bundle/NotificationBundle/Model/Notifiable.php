<?php
namespace Nss\Bundle\NotificationBundle\Model;

interface Notifiable
{

    public function getTitle() :string;

    public function getBody(): string;

}