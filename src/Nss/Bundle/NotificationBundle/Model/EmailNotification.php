<?php

namespace Nss\Bundle\NotificationBundle\Model;

use Symfony\Component\Templating\EngineInterface;

class EmailNotification implements Notifiable
{
    /**
     * @var Notifiable
     */
    private $notifiable;

    /**
     * @var EngineInterface
     */
    private $engine;

    /**
     * @var string
     */
    private $template;

    /**
     * @var array
     */
    private $params;

    /**
     * @param Notifiable $notifiable
     */
    public function __construct(Notifiable $notifiable)
    {
        $this->notifiable = $notifiable;
    }

    /**
     * @param EngineInterface $engine
     */
    public function setRenderEngine(EngineInterface $engine)
    {
        $this->engine = $engine;
    }

    /**
     * @param string $template
     */
    public function setTemplate(string $template)
    {
        $this->template = $template;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params = [])
    {
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->notifiable->getTitle();
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->engine->render($this->template, $this->params);
    }

}