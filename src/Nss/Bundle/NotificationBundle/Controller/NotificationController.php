<?php
namespace Nss\Bundle\NotificationBundle\Controller;

use Nss\Bundle\NotificationBundle\Entity\Notification;
use Nss\Bundle\NotificationBundle\Manager\NotificationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class NotificationController extends Controller
{

    /**
     * @Route("/unread", name="notification_unread")
     * @Template()
     * @Method({"GET"})
     * @return array
     */
    public function unreadAction()
    {
        $manager = $this->getNotificationManager();
        $user = $this->getUser();

        return [
            'notifications' => $manager->getUnreadNotifications($user)
        ];
    }

    /**
     * @Route("/read/{id}", name="notification_read")
     * @ParamConverter("notification", class="NssNotificationBundle:Notification")
     * @param Notification $notification
     * @return RedirectResponse
     */
    public function readAction(Notification $notification)
    {
        $manager = $this->getNotificationManager();
        $manager->readNotification($notification);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('notification');
    }

    /**
     * @Route("/", name="notification")
     * @Template()
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $manager = $this->getNotificationManager();
        $user = $this->getUser();

        return [
            'notifications' => $manager->getNotifications($user)
        ];
    }

    /**
     * @return NotificationManager
     */
    private function getNotificationManager() : NotificationManager
    {
        return $this->get('nss_bundle_notification.manager.notification_manager');
    }
}
