<?php
namespace Nss\Bundle\SroBundle\Controller;

use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use Nss\Bundle\OrderBundle\Manager\ModelManager;
use Nss\Bundle\SroBundle\Manager\OrderManager;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\SroBundle\Manager\SroManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class IndexController extends Controller
{

    const PAGE_LIMIT = 20;

    /**
     * @Route("/", name="sro")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('СРО','sro');

        /** @var SroManager $manager*/
        $manager = $this->get('sro.manager.sro_manager');

        /* @var $filter Filter */
        $filter = $this->get('it_symfony.data_table.filter')->getFilter($request, $manager->getFilters());

        $pagination = $manager->getPaginated(
            $request->query->get('page', 1),
            self::PAGE_LIMIT,
            $filter
        );

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @Route("/view/{id}", name="sro_view")
     * @Template()
     * @param Sro $sro
     * @ParamConverter("sro", class="NssSroBundle:Sro")
     * @return array
     */
    public function viewAction(Sro $sro)
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('СРО','sro');
        $breadcrumbs->addItem($sro->getRegistrationNumber());

        /** @var ModelManager $manager */
        $manager = $this->get('order.manager.model_manager');

        return [
            'sro' => $sro,
            'orderModels' => $manager->getEnabledBySro($sro)
        ];
    }

    /**
     * @Route("/orders/{id}", name="sro_my_orders")
     * @Template()
     * @param Sro $sro
     * @param Request $request
     * @ParamConverter("sro", class="NssSroBundle:Sro")
     * @return array
     */
    public function ordersAction(Request $request, Sro $sro)
    {
        /** @var Breadcrumbs $breadcrumbs*/
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addRouteItem('СРО','sro');
        $breadcrumbs->addItem($sro->getRegistrationNumber());

        /** @var OrderManager $manager */
        $manager = $this->get('sro.manager.order_manager');

        $pagination = $manager->getPaginated(
            $this->getUser(),
            $sro,
            $request->query->get('page', 1),
            self::PAGE_LIMIT
        );

        return [
            'sro' => $sro,
            'pagination' => $pagination
        ];
    }
}
