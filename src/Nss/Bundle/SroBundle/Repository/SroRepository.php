<?php

namespace Nss\Bundle\SroBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\SroBundle\Entity\BaseSroType;

class SroRepository extends EntityRepository
{
    /**
     * @param BaseSroType $sroType
     * @return QueryBuilder
    */
    public function getWithSroOrderModelByCodeQuery(BaseSroType $sroType) : QueryBuilder
    {
        $subQuery = $this->getEntityManager()->createQueryBuilder()
            ->select('som')
            ->from(SroOrderModel::class,'som')
            ->where('som.sro = s')
            ->andWhere('som.enabled = TRUE')
            ->setMaxResults(1)
            ;

        $qb = $this->createQueryBuilder('s');
        $query = $qb->join('s.sroType','st')
            ->where('st.code = :code')
            ->andWhere()
            ->setParameter('code', $sroType->getCode())
            ->andWhere($qb->expr()->exists($subQuery))
        ;

        return $query;
    }

    /**
     * @return QueryBuilder
    */
    public function getWithSroOrderModelQuery() : QueryBuilder
    {
        $subQuery = $this->getEntityManager()->createQueryBuilder()
            ->select('som')
            ->from(SroOrderModel::class,'som')
            ->where('som.sro = s')
            ->andWhere('som.enabled = TRUE')
            ->setMaxResults(1)
        ;

        $qb = $this->createQueryBuilder('s');
        $query = $qb->join('s.sroType','st')
            ->andWhere()
            ->andWhere($qb->expr()->exists($subQuery))
        ;

        return $query;
    }
}