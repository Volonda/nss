<?php
namespace Nss\Bundle\SroBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Nss\Bundle\SroBundle\Entity\BaseSroType;
use Nss\Bundle\BaseCollectionBundle\Entity\BaseSubject;

/**
 * @ORM\Entity(repositoryClass="Nss\Bundle\SroBundle\Repository\SroRepository")
 * @ORM\Table(name="app_sro", schema="public")
 */
class Sro
{

    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="short_name", type="text", nullable=false)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_number", type="string", nullable=false, length=100)
     */
    private $registrationNumber;

    /**
     * @var BaseSroType
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\SroBundle\Entity\BaseSroType")
     * @ORM\JoinColumn(name="sro_type_id", referencedColumnName="id", nullable=false)
     */
    private $sroType;

    /**
     * @var BaseSubject
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\BaseCollectionBundle\Entity\BaseSubject")
     * @ORM\JoinColumn(name="subject_id", referencedColumnName="id", nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="membership_fee_info", type="text", nullable=true)
     */
    private $membershipFeeInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="entrance_fee_info", type="text", nullable=true)
     */
    private $entranceFeeInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="insurance_info", type="text", nullable=true)
     */
    private $insuranceInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="scan_info", type="text", nullable=true)
     */
    private $scanInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="original_info", type="text", nullable=true)
     */
    private $originalInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="documents_info", type="text", nullable=true)
     */
    private $documentsInfo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_payout", type="boolean", nullable=true)
     */
    private $hasPayout;

    /**
     * @var string
     * @ORM\Column(name="special_conditions", type="text", nullable=true)
     */
    private $specialConditions;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\UserBundle\Entity\User", mappedBy="sro")
     */
    private $users;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Nss\Bundle\SroBundle\Entity\Contact", mappedBy="sro")
     */
    private $contacts;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * @param string $registrationNumber
     */
    public function setRegistrationNumber(String $registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;
    }

    /**
     * @return BaseSroType
     */
    public function getSroType()
    {
        return $this->sroType;
    }

    /**
     * @param BaseSroType $sroType
     */
    public function setSroType(BaseSroType $sroType)
    {
        $this->sroType = $sroType;
    }

    /**
     * @return BaseSubject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param BaseSubject $subject
     */
    public function setSubject(BaseSubject $subject)
    {
        $this->subject= $subject;
    }

    /**
     * @return string
     */
    public function getMembershipFeeInfo()
    {
        return $this->membershipFeeInfo;
    }

    /**
     * @param string $membershipFeeInfo
     */
    public function setMembershipFeeInfo(string $membershipFeeInfo)
    {
        $this->membershipFeeInfo = $membershipFeeInfo;
    }

    /**
     * @return string
     */
    public function getEntranceFeeInfo()
    {
        return $this->entranceFeeInfo;
    }

    /**
     * @param string $entranceFeeInfo
     */
    public function setEntranceFeeInfo(string $entranceFeeInfo)
    {
        $this->entranceFeeInfo = $entranceFeeInfo;
    }

    /**
     * @return string
     */
    public function getInsuranceInfo()
    {
        return $this->insuranceInfo;
    }

    /**
     * @param string $insuranceInfo
     */
    public function setInsuranceInfo(string $insuranceInfo)
    {
        $this->insuranceInfo = $insuranceInfo;
    }

    /**
     * @return string
     */
    public function getScanInfo()
    {
        return $this->scanInfo;
    }

    /**
     * @param string $scanInfo
     */
    public function setScanInfo(string $scanInfo)
    {
        $this->scanInfo = $scanInfo;
    }

    /**
     * @return string
     */
    public function getOriginalInfo()
    {
        return $this->originalInfo;
    }

    /**
     * @param string $originalInfo
     */
    public function setOriginalInfo(string $originalInfo)
    {
        $this->originalInfo = $originalInfo;
    }

    /**
     * @return string
     */
    public function getDocumentsInfo()
    {
        return $this->documentsInfo;
    }

    /**
     * @param string $documentsInfo
     */
    public function setDocumentsInfo(string $documentsInfo)
    {
        $this->documentsInfo = $documentsInfo;
    }

    /**
     * @return bool
     */
    public function hasPayout()
    {
        return $this->hasPayout;
    }

    /**
     * @param bool $payoutInfo
     */
    public function setPayoutInfo(bool $payoutInfo)
    {
        $this->hasPayout = $payoutInfo;
    }

    /**
     * @return string
     */
    public function getSpecialConditions()
    {
        return $this->specialConditions;
    }

    /**
     * @param string $specialConditions
     */
    public function setSpecialConditions(string $specialConditions)
    {
        $this->specialConditions = $specialConditions;
    }

    /**
     * @return Collection
     */
    public function getUsers() : Collection
    {
        return $this->users;
    }

    /**
     * @param Collection $users
     */
    public function setUsers(Collection $users)
    {
        $this->users = $users;
    }

    /**
     * @return bool
     */
    public function isHasPayout(): bool
    {
        return $this->hasPayout;
    }

    /**
     * @param bool $hasPayout
     */
    public function setHasPayout(bool $hasPayout)
    {
        $this->hasPayout = $hasPayout;
    }

    /**
     * @return Collection
     */
    public function getContacts() : Collection
    {
        return $this->contacts;
    }

    /**
     * @param Collection $contacts
     */
    public function setContacts(Collection $contacts)
    {
        $this->contacts = $contacts;
    }

    public function __toString()
    {
        return $this->name;
    }
}