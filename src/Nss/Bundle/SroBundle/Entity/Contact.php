<?php
namespace Nss\Bundle\SroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_sro_contacts", schema="public")
 */
class Contact
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="text", nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="text", nullable=false)
     */
    private $fio;

    /**
     * @var Sro
     *
     * @ORM\ManyToOne(targetEntity="Nss\Bundle\SroBundle\Entity\Sro", inversedBy="contacts")
     * @ORM\JoinColumn(name="sro_id", referencedColumnName="id")
     */
    private $sro;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFio(): string
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     */
    public function setFio(string $fio)
    {
        $this->fio = $fio;
    }

    /**
     * @return Sro
     */
    public function getSro(): Sro
    {
        return $this->sro;
    }

    /**
     * @param Sro $sro
     */
    public function setSro(Sro $sro)
    {
        $this->sro = $sro;
    }
}