<?php
namespace Nss\Bundle\SroBundle\Twig;

use Doctrine\ORM\EntityManager;
use Nss\Bundle\OrderBundle\Entity\Model\SroOrderModel;
use Nss\Bundle\SroBundle\Entity\Sro;

class SroExtension extends \Twig_Extension
{
    /** @var EntityManager*/
    private $em;

    /**
     * @param EntityManager $em
    */
    public function  __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [ new \Twig_SimpleFunction('sroEnabledSroOrderModelCollection', [$this, 'getEnabledOrderModelCollection'] ) ];
    }

    public function getEnabledOrderModelCollection(Sro $sro) : array
    {
        return $this->em->getRepository(SroOrderModel::class)->findBy([
            'sro' => $sro->getId(),
            'enabled' => true
        ]);
    }

    public function getName()
    {
        return 'sro_extension';
    }
}