<?php

namespace Nss\Bundle\SroBundle\Manager;

use Doctrine\ORM\EntityManager;
use ITSymfony\Bundle\DataTableBundle\Filter\Filter;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Nss\Bundle\SroBundle\Entity\Sro;

class SroManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /** @var Paginator*/
    private $paginator;

    /** @var array*/
    private $filters = [
        'id' => 'int',
        'st.id' => [
            'type' => 'eq',
            'options' => []
        ],
        'ss.id' => [
            'type' => 'eq',
            'options' => []
        ],
        's.name' => 'like',
        's.shortName' => 'like',
        's.registrationNumber' => 'like'
    ];

    public function __construct(EntityManager $em, Paginator $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getFilters()
    {
        $filters = $this->filters;

        return $filters;
    }

    public function getPaginated(int $page, int $limit, Filter $filter) : PaginationInterface
    {
        $qb = $this->em->createQueryBuilder()
            ->select('s')
            ->from(Sro::class,'s')
            ->join('s.sroType',' st')
            ->join('s.subject','ss')
            ;

        $filter->addFiltersToQB($qb);

        return $this->paginator->paginate($qb, $page, $limit,
            ['defaultSortFieldName' => 's.id', 'defaultSortDirection' => 'ASC']
        );
    }

}
