<?php
namespace Nss\Bundle\AppBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var MenuItemCollection*/
    private  $menuItemCollection;

    /** @var FactoryInterface*/
    private $factory;

    /** @var EntityManager $em*/
    private $em;

    /** @var  AuthorizationChecker $checker*/
    private $checker;

    /**
     * @param EntityManager $em
     * @param FactoryInterface $factory
     * @param AuthorizationChecker $checker
    */
    public function __construct(
        EntityManager $em,
        FactoryInterface $factory,
        AuthorizationChecker $checker
    ) {
        $this->factory = $factory;
        $this->em = $em;
        $this->checker = $checker;
    }

    /**
     * @param array $options
     * @return MenuItem
    */
    public function mainMenu(array $options) : MenuItem
    {
        $this->buildMenuItemCollection();

        /** @var MenuItem $rootMenuItem*/
        $rootMenuItem = $this->factory->createItem('root');

        $this->buildMenuLevel($rootMenuItem);

        return $rootMenuItem;
    }

    /**
     * Сборка меню
     *
     * @param MenuItem $currentLevelMenuItem
     * @param int $parentId
     * @throws \Exception
    */
    private function buildMenuLevel(MenuItem $currentLevelMenuItem, $parentId = 0)
    {
        if (!$this->menuItemCollection instanceof MenuItemCollection) {
            throw new \Exception('undefined menu item collection');
        }

        foreach ($this->menuItemCollection->getAll() as $menuItem) {
            if ($parentId == $menuItem['parent_id']) {

                $options = [];
                if (isset($menuItem['options'])){
                    $options['routeParameters'] = $menuItem['options'];
                }

                if (isset($menuItem['route'])) {
                    $options['route'] = $menuItem['route'];
                }

                $nextLevelMenuItem = $currentLevelMenuItem->addChild($menuItem['title'], $options);

                if (isset($menuItem['icon'])) {
                    $nextLevelMenuItem->setExtra('icon', $menuItem['icon']);
                }

                call_user_func_array([$this, 'buildMenuLevel'], [
                    $nextLevelMenuItem, $menuItem['id']
                ]);
            }
        }
    }

    /**
     * Логика отображения пунктов меню
     */
    private function buildMenuItemCollection()
    {

        $items = [
            ['title' => 'Заявки', 'parent_id' => 0, 'route' => 'order_my',  'action' => 'order_view_my', 'icon' => 'fa fa-folder-o'],
            ['title' => 'Все заявки', 'parent_id' => 0, 'route' => 'moderation', 'action' => 'moderation_view_all', 'icon' => 'fa fa-th-large'],
            ['title' => 'СРО', 'parent_id' => 0, 'route'=> 'sro',  'action' => 'sro_view', 'icon' => 'fa fa-university'],
            ['title' => 'Клиенты','route' => 'organization_my', 'parent_id' => 0,'action' => 'contact_my', 'icon' => 'fa fa-address-book-o'],
        ];

        $collection = new MenuItemCollection();

        foreach ($items as $item) {
            if (!$item['action'] || $this->checker->isGranted('allow', 'action:' . $item['action'])) {
                $collection->add($item);
            }
        }

        $this->menuItemCollection = $collection;
    }
}