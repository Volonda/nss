<?php
namespace Nss\Bundle\AppBundle\Menu;

class MenuItemCollection
{
    private $menuItems = [];

    /**
     * @param array $menuItem
     * @return int
    */
    public function add(array $menuItem) : int
    {
        $menuItem['id'] = $this->getNextId();

        $this->menuItems[] = $menuItem;

        return  $menuItem['id'];
    }

    /**
     * @return array
    */
    public function getAll() : array
    {
        return $this->menuItems;
    }

    /**
     * @return int
     */
    private function getNextId() : int
    {
        if (count($this->menuItems)) {
            return $this->menuItems[count($this->menuItems) - 1]['id'] + 1;
        } else {
            return 1;
        }
    }
}