<?php
namespace Nss\Bundle\AppBundle\Entity\Traits;

use Gedmo\Timestampable\Traits\TimestampableEntity;

trait TimestampableEntityString
{
    use TimestampableEntity;

    public function getCreatedAtDateString()
    {
        return $this->getCreatedAt()->format('d.m.Y');
    }

    public function getCreatedAtDateTimeString()
    {
        return $this->getCreatedAt()->format('d.m.Y H:i:s');
    }

    public function getUpdatedAtDateString()
    {
        return $this->getUpdatedAt()->format('d.m.Y');
    }

    public function getUpdatedAtDateTimeString()
    {
        return $this->getUpdatedAt()->format('d.m.Y H:i:s');
    }
}