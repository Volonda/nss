<?php

namespace Nss\Bundle\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="app")
     */
    public function indexAction()
    {
        return $this->render('NssAppBundle:Default:index.html.twig');
    }
}
