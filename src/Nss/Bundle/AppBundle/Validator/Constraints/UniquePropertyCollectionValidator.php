<?php
namespace Nss\Bundle\AppBundle\Validator\Constraints;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniquePropertyCollectionValidator extends ConstraintValidator
{
    /**
     * @param $collection
     * @param Constraint $constraint
     * @throws \Exception
     */
    public function validate($collection, Constraint $constraint)
    {
        if ($collection instanceof Collection) {

            $accessor = new PropertyAccessor();
            $property = $constraint->property;
            $values = [];

            foreach ($collection as $object) {
                if ($accessor->isReadable($object, $property)) {
                    $value = $accessor->getValue($object, $property);
                    if (!in_array($value, $values)) {
                        $values[] = $value;
                    } else {
                        $this->context->buildViolation($constraint->message)
                            ->addViolation();
                    }
                }
            }
        }
    }
}