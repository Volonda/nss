<?php
namespace Nss\Bundle\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniquePropertyCollection extends Constraint
{
    public $message = 'Значение не уникально';

    public $property = null;
}