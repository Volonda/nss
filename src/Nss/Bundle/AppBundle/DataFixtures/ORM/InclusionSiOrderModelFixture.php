<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\DocumentBundle\Entity\TemplateList;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\OrderBundle\Entity\BaseService;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel;
use Nss\Bundle\OrderBundle\Entity\Model\OrderServiceModel;
use Nss\Bundle\OrderBundle\Entity\Model\OrderServiceModelFormType;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModel;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModelFormType;
use Nss\Bundle\OrderBundle\Entity\Model\OptionModel;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow;

class InclusionSiOrderModelFixture extends AbstractFixture implements OrderedFixtureInterface
{
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        /** @var BaseOrderType $orderModelType */
        $orderModelType = $this->getReference('order_model_type_inclusion');
        $order = new TemplateOrderModel();
        $order->setStatus($manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'draft']));
        $order->setType($orderModelType);
        $order->setTitle('Внесение - изыскание');
        $manager->persist($order);

        /** @var TemplateList $templateList*/
        $templateList = $this->getReference('document_inclusion_input');
        $order->setInputDocumentTemplateList($templateList);

        $templateList = $this->getReference('document_inclusion_output');
        $order->setOutputDocumentTemplateList($templateList);

        /** @var WorkflowModel $workflowModel*/
        $workflowModel = $this->getReference('workflow.workflow_model');
        $order->setWorkflowModel($workflowModel);

        //partner participation service
        /**
         * @var BaseService $service
        */
        $service = $this->getReference('service_responsibility_level_vv');
        $this->addService($order, $service, [
            [
                'optionModel' =>  $this->getReference('option_less_than_25'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_50')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_50'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_150')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_300'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_500')
            ],
            [
                'optionModel' =>  $this->getReference('option_more_than_300'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_1000')
            ]
        ]);

        $service = $this->getReference('service_responsibility_level_odo');
        $this->addService($order, $service, [
            [
                'optionModel' =>  $this->getReference('option_no'),
                'rewardModel' => null,
                'conditionModel' => null
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_25'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_150')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_50'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_350')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_300'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_2500')
            ],
            [
                'optionModel' =>  $this->getReference('option_more_than_300'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_3500')
            ]
        ]);

        $service = $this->getReference('service_document_package');
        $this->addService($order, $service, [
            [
                'optionModel' =>  $this->getReference('option_no'),
                'rewardModel' => null,
                'conditionModel' => null
            ],
            [
                'optionModel' =>  $this->getReference('option_yes'),
                'rewardModel' => $this->getReference('reward_5'),
                'conditionModel' => null
            ]
        ]);

        $manager->persist($order);
        $manager->flush();
    }


    private function addService(OrderModel $orderModel, BaseService $service, array $rows)
    {
        /** @var ServiceModelFormType $serviceListFormType*/
        $serviceListFormType = $this->getReference('service_list_form_type_simple_select');

        /** @var ObjectManager $manager*/
        $manager = $this->manager;

        $serviceModel = new ServiceModel();
        $serviceModel->setService($service);
        $serviceModel->setOrder($orderModel);
        $serviceModel->setFormType($serviceListFormType);

        foreach ($rows as $row) {

            $optionModel = $row['optionModel'];
            $conditionModel = $row['conditionModel'];
            $rewardModel = $row['rewardModel'];

            $serviceOption = new OptionModel();
            $serviceOption->setOption($optionModel);
            $serviceOption->setService($serviceModel);
            $serviceOption->setCondition($conditionModel);
            $serviceOption->setRewardModel($rewardModel);
            $manager->persist($serviceOption);
        }

        $manager->persist($serviceModel);
    }


    public function getOrder()
    {
        return 100;
    }
}