<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\ContactBundle\Entity\Contact;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\UserBundle\Entity\OrganizationContact;
use Nss\Bundle\UserBundle\Entity\User;

class ContactFixture extends AbstractFixture implements OrderedFixtureInterface
{
    private $name = ['Иван','Петр','Арсен','Федор','Емеля'];

    private $surName = ['Иванович','Арсенович','Петрович','Федорович','Емельянович'];

    private $lastName = ['Иванов', 'Арсенов', 'Петров', 'Федоров', 'Емельянов'];

    private $operatorCode = ['968', '911', '904', '912', '965', '914'];

    private $emails = ['gmail.dev', 'yandex.dev', 'mail.dev'];

    public function load(ObjectManager $manager)
    {
        /** @var User $agent*/
        $agent = $manager->getRepository(User::class)->findOneBy(['username' => 'agent']);

        for ($i=0; $i<=20; $i++) {

            /** @var Organization $organization*/
            $organization = $this->getReference('organization_' . $i);
            $contact = $this->randomGenerate($agent, $organization);

            $manager->persist($contact);
        }

        $manager->flush();
    }

    private function randomGenerate(User $agent, Organization $organization)
    {
        $contact = new Contact();
        $contact->setOrganization($organization);
        $contact->setOwner($agent);
        $contact->setPhone(
            '+7' . $this->operatorCode[array_rand($this->operatorCode)] . sprintf('%03d', rand(0, 999)) . sprintf('%02d', rand(0, 99)) . sprintf('%02d', rand(0, 99))
        );
        $contact->setEmail('user' . sprintf('%03d', rand(0, 999)) . '@' . $this->emails[array_rand($this->emails)]);
        $name = $this->name[array_rand($this->name)];
        $surName = $this->surName[array_rand($this->surName)];
        $lastName = $this->lastName[array_rand($this->lastName)];
        $contact->setFio($lastName . ' ' . $name . ' ' . $surName);

        return $contact;
    }


    public function getOrder()
    {
        return 200;
    }
}