<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\OrderBundle\Entity\BaseOption;

class OptionFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $option = new BaseOption();
        $option->setName('до 60 млн руб.');
        $option->setDescription('до 60 млн руб.');
        $option->setCode('less_than_60');
        $this->addReference('option_less_than_60', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('до 500 млн руб.');
        $option->setDescription('до 500 млн руб.');
        $option->setCode('less_than_500');
        $this->addReference('option_less_than_500', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('до 3 млрд руб.');
        $option->setDescription('до 3 млрд руб.');
        $option->setCode('less_than_3000');
        $this->addReference('option_less_than_3000', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('до 10 млрд руб.');
        $option->setDescription('до 10 млрд руб.');
        $option->setCode('less_than_10000');
        $this->addReference('option_less_than_10000', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('свыше 10 млрд руб.');
        $option->setDescription('свыше 10 млрд руб.');
        $option->setCode('more_than_10000');
        $this->addReference('option_more_than_10000', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('до 25 млн руб.');
        $option->setDescription('до 25 млн руб.');
        $option->setCode('less_than_25');
        $this->addReference('option_less_than_25', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('до 50 млн руб.');
        $option->setDescription('до 50 млн руб.');
        $option->setCode('less_than_50');
        $this->addReference('option_less_than_50', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('до 300 млн руб.');
        $option->setDescription('до 300 млн руб.');
        $option->setCode('less_than_300');
        $this->addReference('option_less_than_300', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('свыше 300 млн руб.');
        $option->setDescription('свыше 300 млн руб.');
        $option->setCode('more_than_300');
        $this->addReference('option_more_than_300', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('Да');
        $option->setDescription('Да');
        $option->setCode('yes');
        $this->addReference('option_yes', $option);
        $manager->persist($option);

        $option = new BaseOption();
        $option->setName('Нет');
        $option->setDescription('Нет');
        $option->setCode('no');
        $this->addReference('option_no', $option);
        $manager->persist($option);

        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}