<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\OrderBundle\Entity\BaseService;
use Nss\Bundle\OrderBundle\Entity\BaseServiceType;

class ServiceFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $mainServiceType = new BaseServiceType();
        $mainServiceType->setName('Основные');
        $mainServiceType->setCode('main');
        $manager->persist($mainServiceType);

        $service = new BaseService();
        $service->setName('Уровень ответственности ВВ');
        $service->setType($mainServiceType);
        $service->setCode('level_of_responsibility_vv');
        $manager->persist($service);
        $this->addReference('service_responsibility_level_vv', $service);

        $service = new BaseService();
        $service->setName('Уровень ответственности ОДО');
        $service->setType($mainServiceType);
        $service->setCode('level_of_responsibility_odo');
        $manager->persist($service);
        $this->addReference('service_responsibility_level_odo', $service);

        $service = new BaseService();
        $service->setName('Пакет документов');
        $service->setType($mainServiceType);
        $service->setCode('document_package');
        $manager->persist($service);
        $this->addReference('service_document_package', $service);

        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}