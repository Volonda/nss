<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\UserBundle\Entity\Action;
use Nss\Bundle\UserBundle\Entity\Group;
use Nss\Bundle\UserBundle\Entity\OrganizationContact;
use Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow;
use Nss\Bundle\WorkflowBundle\Entity\Model\TransitionModel;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowActionModel;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel;

class WorkflowFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var BaseWorkflow $baseWorkflow*/
        $baseWorkflow = new BaseWorkflow();
        $baseWorkflow->setCode('default');
        $baseWorkflow->setName('Стандартный рабочий процесс');
        $manager->persist($baseWorkflow);

        $workflowModel = new WorkflowModel();
        $workflowModel->setBaseWorkflow($baseWorkflow);
        $manager->persist($workflowModel);

        $this->addReference('workflow.workflow_model', $workflowModel);

        /**
         * @var BaseOrderStatus $orderStatusDraft
         * @var BaseOrderStatus $orderStatusQueue
         * @var BaseOrderStatus$orderStatusProcessing
         * @var BaseOrderStatus $orderStatusProcessed
         */
        $orderStatusDraft = $manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'draft']);
        $orderStatusQueue = $manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'queue']);
        $orderStatusProcessing = $manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'processing']);
        $orderStatusProcessed = $manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'processed']);

        /**
         * @var Group $operatorGroup
         * @var Group $agentGroup
        */
        $agentGroup = $manager->getRepository(Group::class)->findOneBy(['name' => 'agent']);
        $operatorGroup = $manager->getRepository(Group::class)->findOneBy(['name' => 'operator']);

        /**
         * Draft -> Queue
        */
        $transition = new TransitionModel();
        $transition->setCurrentStatus($orderStatusDraft);
        $transition->setTransitionStatus($orderStatusQueue);
        $transition->setWorkflowModel($workflowModel);
        $transition->addGroup($agentGroup);
        $manager->persist($transition);

        /**
         * Queue -> Processing
         */
        $transition = new TransitionModel();
        $transition->setCurrentStatus($orderStatusQueue);
        $transition->setTransitionStatus($orderStatusProcessing);
        $transition->setWorkflowModel($workflowModel);
        $transition->addGroup($operatorGroup);
        $manager->persist($transition);

        /**
         * Processing -> Processed
         */
        $transition = new TransitionModel();
        $transition->setCurrentStatus($orderStatusProcessing);
        $transition->setTransitionStatus($orderStatusProcessed);
        $transition->setWorkflowModel($workflowModel);
        $transition->addGroup($operatorGroup);
        $manager->persist($transition);


        /**
         * @var BaseOrderStatus $orderStatusDraft
         * @var BaseOrderStatus $orderStatusQueue
         * @var BaseOrderStatus$orderStatusProcessing
         * @var BaseOrderStatus $orderStatusProcessed
         */
        $orderStatusDraft = $manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'draft']);
        $orderStatusQueue = $manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'queue']);
        $orderStatusProcessing = $manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'processing']);
        $orderStatusProcessed = $manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'processed']);

        /**
         * @var Action $orderEditMyAction
         * @var Action $moderationViewAll
         * @var Action $moderation
         * @var Action $orderViewMy
         */
        $orderEditMyAction = $manager->getRepository(Action::class)->findOneBy(['code' => 'order_edit_my']);
        $orderViewMy = $manager->getRepository(Action::class)->findOneBy(['code' => 'order_view_my']);
        $moderationViewAll = $manager->getRepository(Action::class)->findOneBy(['code' => 'moderation_view_all']);
        $moderation = $manager->getRepository(Action::class)->findOneBy(['code' => 'moderation']);

        //Draft
        $workFlowAction = new WorkflowActionModel();
        $workFlowAction->addAction($orderEditMyAction);
        $workFlowAction->addAction($orderViewMy);
        $workFlowAction->setWorkflowModel($workflowModel);
        $workFlowAction->setCurrentStatus($orderStatusDraft);
        $manager->persist($workFlowAction);

        //Queue
        $workFlowAction = new WorkflowActionModel();
        $workFlowAction->addAction($moderationViewAll);
        $workFlowAction->addAction($moderation);
        $workFlowAction->addAction($orderViewMy);
        $workFlowAction->setWorkflowModel($workflowModel);
        $workFlowAction->setCurrentStatus($orderStatusQueue);
        $manager->persist($workFlowAction);

        //Processing
        $workFlowAction = new WorkflowActionModel();
        $workFlowAction->addAction($moderationViewAll);
        $workFlowAction->addAction($moderation);
        $workFlowAction->addAction($orderViewMy);
        $workFlowAction->setWorkflowModel($workflowModel);
        $workFlowAction->setCurrentStatus($orderStatusProcessing);
        $manager->persist($workFlowAction);

        $workFlowAction = new WorkflowActionModel();
        $workFlowAction->addAction($moderationViewAll);
        $workFlowAction->addAction($orderViewMy);
        $workFlowAction->setWorkflowModel($workflowModel);
        $workFlowAction->setCurrentStatus($orderStatusProcessed);
        $manager->persist($workFlowAction);

        $manager->flush();
    }

    public function getOrder()
    {
        return 80;
    }
}