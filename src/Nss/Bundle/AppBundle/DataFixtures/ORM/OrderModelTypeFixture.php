<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;

class OrderModelTypeFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $orderModelType = new BaseOrderType();
        $orderModelType->setCode('inclusion');
        $orderModelType->setName('Внесение');
        $manager->persist($orderModelType);
        $this->addReference('order_model_type_inclusion', $orderModelType);

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}