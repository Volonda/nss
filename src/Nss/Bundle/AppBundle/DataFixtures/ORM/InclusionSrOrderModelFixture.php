<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\DocumentBundle\Entity\TemplateList;
use Nss\Bundle\OrderBundle\Entity\BaseOrderStatus;
use Nss\Bundle\OrderBundle\Entity\BaseOrderType;
use Nss\Bundle\OrderBundle\Entity\BaseService;
use Nss\Bundle\OrderBundle\Entity\Model\OrderModel;
use Nss\Bundle\OrderBundle\Entity\Model\TemplateOrderModel;
use Nss\Bundle\UserBundle\Entity\Action;
use Nss\Bundle\WorkflowBundle\Entity\Model\WorkflowModel;
use Nss\Bundle\OrderBundle\Entity\Model\OrderServiceModel;
use Nss\Bundle\OrderBundle\Entity\Model\OrderServiceModelFormType;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModel;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModelFormType;
use Nss\Bundle\OrderBundle\Entity\Model\OptionModel;
use Nss\Bundle\SroBundle\Entity\Sro;
use Nss\Bundle\WorkflowBundle\Entity\BaseWorkflow;
use Nss\Bundle\WorkflowBundle\Entity\Model\ActionModel;


class InclusionSrOrderModelFixture extends AbstractFixture implements OrderedFixtureInterface
{
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        /** @var BaseOrderType $orderModelType */
        $orderModelType = $this->getReference('order_model_type_inclusion');
        $order = new TemplateOrderModel();
        $order->setStatus($manager->getRepository(BaseOrderStatus::class)->findOneBy(['code' => 'draft']));
        $order->setType($orderModelType);
        $order->setTitle('Внесение - стройка');
        $manager->persist($order);

        /** @var TemplateList $templateList*/
        $templateList = $this->getReference('document_inclusion_input');
        $order->setInputDocumentTemplateList($templateList);

        $templateList = $this->getReference('document_inclusion_output');
        $order->setOutputDocumentTemplateList($templateList);

        /** @var WorkflowModel $workflowModel*/
        $workflowModel = $this->getReference('workflow.workflow_model');
        $order->setWorkflowModel($workflowModel);

        //partner participation service
        /**
         * @var BaseService $service
        */
        $service = $this->getReference('service_responsibility_level_vv');
        $this->addService($order, $service, [
            [
                'optionModel' =>  $this->getReference('option_less_than_60'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_100')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_500'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_500')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_3000'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_1500')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_10000'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_2000')
            ],
            [
                'optionModel' =>  $this->getReference('option_more_than_10000'),
                'rewardModel' => $this->getReference('reward_10'),
                'conditionModel' => $this->getReference('condition_5000')
            ]
        ]);

        $service = $this->getReference('service_responsibility_level_odo');
        $this->addService($order, $service, [
            [
                'optionModel' =>  $this->getReference('option_no'),
                'rewardModel' => null,
                'conditionModel' => null
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_60'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_200')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_500'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_2500')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_3000'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_4500')
            ],
            [
                'optionModel' =>  $this->getReference('option_less_than_10000'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_7000')
            ],
            [
                'optionModel' =>  $this->getReference('option_more_than_10000'),
                'rewardModel' => null,
                'conditionModel' => $this->getReference('condition_25000')
            ]
        ]);

        $service = $this->getReference('service_document_package');
        $this->addService($order, $service, [
            [
                'optionModel' =>  $this->getReference('option_no'),
                'rewardModel' => null,
                'conditionModel' => null
            ],
            [
                'optionModel' =>  $this->getReference('option_yes'),
                'rewardModel' => $this->getReference('reward_5'),
                'conditionModel' => null
            ]
        ]);

        $manager->persist($order);
        $manager->flush();
    }

    private function addService(OrderModel $orderModel, BaseService $service, array $rows)
    {
        /** @var ServiceModelFormType $serviceListFormType*/
        $serviceListFormType = $this->getReference('service_list_form_type_simple_select');

        /** @var ObjectManager $manager*/
        $manager = $this->manager;

        $serviceModel = new ServiceModel();
        $serviceModel->setService($service);
        $serviceModel->setOrder($orderModel);
        $serviceModel->setFormType($serviceListFormType);

        foreach ($rows as $row) {

            $optionModel = $row['optionModel'];
            $conditionModel = $row['conditionModel'];
            $rewardModel = $row['rewardModel'];

            $serviceOption = new OptionModel();
            $serviceOption->setOption($optionModel);
            $serviceOption->setService($serviceModel);
            $serviceOption->setCondition($conditionModel);
            $serviceOption->setRewardModel($rewardModel);
            $manager->persist($serviceOption);
        }

        $manager->persist($serviceModel);
    }


    public function getOrder()
    {
        return 100;
    }
}