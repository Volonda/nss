<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\NotificationBundle\Entity\Notification;
use Nss\Bundle\UserBundle\Entity\User;

class UnreadNotificationFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*$agent = $manager->getRepository(User::class)
            ->findOneBy(['usernameCanonical' => 'agent']);

        foreach ($this->getNotification() as $title => $notification) {
            $notification = new Notification($agent, $title, $notification);
            $notification->setSubjectId(1);
            $manager->persist($notification);
        }

        $manager->flush();*/
    }

    public function getOrder()
    {
        return 200;
    }

    private function getNotification()
    {
        return [
            'Создан новая заявка' => 'Order - создание',
            'Изменение статуса заявки' => 'Workflow - переход',
        ];
    }

}