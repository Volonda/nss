<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\OrderBundle\Entity\BaseFormType;
use Nss\Bundle\OrderBundle\Entity\Model\ServiceModelFormType;


class OrderServiceFormTypeFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $serviceListFormType = new ServiceModelFormType();
        /** @var BaseFormType $formType*/
        $formType=$manager->getRepository(BaseFormType::class)->find(2);
        $serviceListFormType->setFormType($formType);
        $serviceListFormType->setCode('simple_select');
        $serviceListFormType->setTitle('Простой выпадающий список');
        $manager->persist($serviceListFormType);
        $this->addReference('service_list_form_type_simple_select', $serviceListFormType);

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}