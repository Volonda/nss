<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\OrderBundle\Entity\BaseRewardType;
use Nss\Bundle\OrderBundle\Entity\Model\RewardModel;

class RewardFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $rewardType = new BaseRewardType();
        $rewardType->setName('Рубли');
        $rewardType->setShortName('руб');
        $rewardType->setCode('rur');
        $manager->persist($rewardType);

        $reward = new RewardModel();
        $reward->setType($rewardType);
        $reward->setValue(0);
        $reward->setName('Нет');
        $manager->persist($reward);

        $this->addReference('reward_0', $reward);

        $reward = new RewardModel();
        $reward->setType($rewardType);
        $reward->setValue(10000);
        $reward->setName('10 000');
        $manager->persist($reward);

        $this->addReference('reward_10', $reward);

        $reward = new RewardModel();
        $reward->setType($rewardType);
        $reward->setValue(5000);
        $reward->setName('5 000');
        $manager->persist($reward);

        $this->addReference('reward_5', $reward);

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}