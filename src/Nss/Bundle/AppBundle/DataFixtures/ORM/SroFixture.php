<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\BaseCollectionBundle\Entity\BaseSubject;
use Nss\Bundle\OrganizationBundle\Entity\Organization;
use Nss\Bundle\SroBundle\Entity\BaseSroType;
use Nss\Bundle\SroBundle\Entity\Contact;
use Nss\Bundle\SroBundle\Entity\Sro;

class SroFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var BaseSubject $subject*/
        $subject = $manager->getRepository(BaseSubject::class)->find(4);

        $contact = new Contact();
        $contact->setFio('Иванов Иван Иванович');
        $contact->setEmail('sr@sro.dev');
        $contact->setPhone('+7 (812) 777 77 77');

        //СР
        $sroType = new BaseSroType();
        $sroType->setName('Строители');
        $sroType->setCode('building');
        $manager->persist($sroType);

        $sro = new Sro();
        /** @var BaseSroType $sroTypeBuilding*/
        $sro->setSroType($sroType);
        $sro->setSubject($subject);
        $sro->setName('СР');
        $sro->setShortName('СР');
        $sro->setRegistrationNumber('СРО-С-236-22042011');
        $manager->persist($sro);
        $contact = clone $contact;
        $contact->setSro($sro);
        $manager->persist($contact);
        $this->addReference('sro_sr', $sro);

        //СИ
        $sroType = new BaseSroType();
        $sroType->setName('Изыскатели');
        $sroType->setCode('research');
        $manager->persist($sroType);

        $sro = new Sro();
        /** @var BaseSroType $sroTypeBuilding*/
        $sro->setSroType($sroType);
        $sro->setSubject($subject);
        $sro->setName('СИ');
        $sro->setShortName('СИ');
        $sro->setRegistrationNumber('СРО-И-029-25102011');
        $manager->persist($sro);
        $contact = clone $contact;
        $contact->setSro($sro);
        $manager->persist($contact);
        $this->addReference('sro_si', $sro);

        //СП
        $sroType = new BaseSroType();
        $sroType->setName('Проектирование');
        $sroType->setCode('project');
        $manager->persist($sroType);

        $sro = new Sro();
        /** @var BaseSroType $sroTypeBuilding*/
        $sro->setSroType($sroType);
        $sro->setSubject($subject);
        $sro->setName('СП');
        $sro->setShortName('СП');
        $sro->setRegistrationNumber('СРО-П-167-25102011');
        $manager->persist($sro);
        $contact = clone $contact;
        $contact->setSro($sro);
        $manager->persist($contact);
        $this->addReference('sro_sp', $sro);

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}