<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\DocumentBundle\Entity\Template;
use Nss\Bundle\DocumentBundle\Entity\TemplateInTemplateList;
use Nss\Bundle\DocumentBundle\Entity\TemplateList;

class DocumentFixture extends AbstractFixture implements OrderedFixtureInterface
{
    /** @var ObjectManager*/
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        //inclusion input documents
        $templateList = new TemplateList();
        $templateList->setCode('inclusion_input');
        $templateList->setName('Вступление. Входящие');
        $manager->persist($templateList);
        $this->addDocument('insurance_contract', 'Договор страхования', $templateList);
        $this->addDocument('work_type_statement', 'Заявление с видами работ', $templateList);
        $this->addDocument('additional_documents', 'Дополнительные документы', $templateList);
        $this->addDocument('info_mail', 'Информационное письмо', $templateList);
        $this->addDocument('loan_agreement', 'График платежей, договор займа', $templateList);
        $this->addDocument('charter', 'Устав, изменения в устав', $templateList);
        $this->addDocument('cost_work', 'Сведения о стоимости работ', $templateList);
        $this->addDocument('grn', 'Лист записи, свидетельство ГРН', $templateList);
        $this->addDocument('inn', 'Свидетельство ИНН', $templateList);
        $this->addDocument('ogrn', 'Свидетельство ОГРН', $templateList);
        $this->addDocument('info', 'Информационный лист', $templateList);
        $this->addReference('document_inclusion_input', $templateList);

        //output inclusion documents
        $templateList = new TemplateList();
        $templateList->setName('Вступление. Исходящие');
        $templateList->setCode('inclusion_output');
        $manager->persist($templateList);
        $this->addDocument('negative_act', 'Отрицательный акт', $templateList);
        $this->addDocument('admission', 'Допуск', $templateList);
        $this->addReference('document_inclusion_output', $templateList);

        $manager->flush();
    }

    private function addDocument(string $code, string $title, TemplateList $templateList)
    {
        /** @var ObjectManager $manager*/
        $manager = $this->manager;

        $template = new Template();
        $template->setName($title);
        $template->setCode($code);
        $manager->persist($template);

        $templateInTemplateList = new TemplateInTemplateList();
        $templateInTemplateList->setTemplate($template);
        $templateInTemplateList->setTemplateList($templateList);

        $manager->persist($templateInTemplateList);
    }

    public function getOrder()
    {
        return 5;
    }
}