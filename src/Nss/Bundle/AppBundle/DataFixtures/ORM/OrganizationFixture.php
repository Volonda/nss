<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\BaseCollectionBundle\Entity\BaseOrganizationType;
use Nss\Bundle\OrganizationBundle\Entity\Organization;

class OrganizationFixture extends AbstractFixture implements OrderedFixtureInterface
{

    private $names = [
        'Российский алюминий',
        'УГМК',
        'Металлоинвест',
        'ГК «ТАИФ»',
        'Группа ГАЗ',
        'Русснефть',
        'МегаФон',
        'Эльдорадо',
        'Евросеть',
        'Объединенная металлургическая компания',
        'Группа «СОК»',
        'Рольф',
        'СИА Интернейшнл',
        'Мегаполис (компания)',
        'СУЭК',
        'Группа компаний «Протек»',
        'Группа ЧТПЗ',
        'SNS',
        'Русская медная компания',
        'Трансмашхолдинг',
        'Еврохим',
        'Группа «Альянс»',
        'Евроцемент'
    ];

    private $types = [];

    public function load(ObjectManager $manager)
    {
        $this->types = $manager->getRepository(BaseOrganizationType::class)->findAll();

        for ($i=0; $i<=20; $i++) {
            $organization = $this->randomGenerate($i);
            $this->addReference('organization_' . $i, $organization);

            $manager->persist($organization);
        }

        $manager->flush();
    }

    private function randomGenerate(int $index)
    {
        $organization = new Organization();
        $organization->setInn(rand(1000000000, 9999999999));
        $organization->setFullName($this->names[$index]);
        $organization->setShortName($this->names[$index]);
        $organization->setType($this->types[array_rand($this->types)]);

        return $organization;
    }


    public function getOrder()
    {
        return 100;
    }
}