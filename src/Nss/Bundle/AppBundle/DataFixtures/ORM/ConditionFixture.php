<?php
namespace Nss\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nss\Bundle\OrderBundle\Entity\BaseConditionType;
use Nss\Bundle\OrderBundle\Entity\Model\ConditionModel;

class ConditionFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $conditionType = new BaseConditionType();
        $conditionType->setName('Рубли');
        $conditionType->setShortName('руб');
        $conditionType->setCode('rur');
        $manager->persist($conditionType);
        $this->addReference('condition_type_rur', $conditionType);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(100000);
        $condition->setName('100 000');
        $manager->persist($condition);
        $this->addReference('condition_100', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(200000);
        $condition->setName('200 000');
        $manager->persist($condition);
        $this->addReference('condition_200', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(500000);
        $condition->setName('500 000');
        $manager->persist($condition);
        $this->addReference('condition_500', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(1500000);
        $condition->setName('1 500 000');
        $manager->persist($condition);
        $this->addReference('condition_1500', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(2500000);
        $condition->setName('2 500 000');
        $manager->persist($condition);
        $this->addReference('condition_2500', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(4500000);
        $condition->setName('4 500 000');
        $manager->persist($condition);
        $this->addReference('condition_4500', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(2000000);
        $condition->setName('2 000 000');
        $manager->persist($condition);
        $this->addReference('condition_2000', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(7000000);
        $condition->setName('7 000 000');
        $manager->persist($condition);
        $this->addReference('condition_7000', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(5000000);
        $condition->setName('5 000 000');
        $manager->persist($condition);
        $this->addReference('condition_5000', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(25000000);
        $condition->setName('25 000 000');
        $manager->persist($condition);
        $this->addReference('condition_25000', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(50000);
        $condition->setName('50 000');
        $manager->persist($condition);
        $this->addReference('condition_50', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(150000);
        $condition->setName('150 000');
        $manager->persist($condition);
        $this->addReference('condition_150', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(350000);
        $condition->setName('350 000');
        $manager->persist($condition);
        $this->addReference('condition_350', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(1000000);
        $condition->setName('1 000 000');
        $manager->persist($condition);
        $this->addReference('condition_1000', $condition);

        $condition = new ConditionModel();
        $condition->setType($conditionType);
        $condition->setValue(3500000);
        $condition->setName('3 500 000');
        $manager->persist($condition);
        $this->addReference('condition_3500', $condition);

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}