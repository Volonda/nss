<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Nss\Bundle\AdminBundle\NssAdminBundle(),
            new Nss\Bundle\UserBundle\NssUserBundle(),
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),
            new Nss\Bundle\OrderBundle\NssOrderBundle(),
            new Nss\Bundle\AppBundle\NssAppBundle(),
            new ITSymfony\Bundle\DataTableBundle\ITSymfonyDataTableBundle(),
            new ITSymfony\Bundle\BaseCollectionBundle\ITSymfonyBaseCollectionBundle(),
            new ITSymfony\Bundle\RequiredFieldsFormBundle\RequiredFieldsFormBundle(),
            new Nss\Bundle\BaseCollectionBundle\NssBaseCollectionBundle(),
            new Nss\Bundle\DocumentBundle\NssDocumentBundle(),
            new Nss\Bundle\OrganizationBundle\NssOrganizationBundle(),
            new Nss\Bundle\SearchBundle\NssSearchBundle(),
            new Nss\Bundle\ModerationBundle\NssModerationBundle(),
            new ITSymfony\Bundle\ActionRolePermissionBundle\ITSymfonyActionRolePermissionBundle(),
            new ITSymfony\Bundle\SeldonBundle\ITSymfonySeldonBundle(),
            new Nss\Bundle\WorkflowBundle\NssWorkflowBundle(),
            new CheckerBundle\CheckerBundle(),
            new Nss\Bundle\ContactBundle\NssContactBundle(),
            new Nss\Bundle\SroBundle\NssSroBundle(),
            new WhiteOctober\BreadcrumbsBundle\WhiteOctoberBreadcrumbsBundle(),
            new Nss\Bundle\HistoryBundle\NssHistoryBundle(),
            new Nss\Bundle\MessageBundle\NssMessageBundle(),
            new Nss\Bundle\NotificationBundle\NssNotificationBundle(),
            new Nss\Bundle\CommentBundle\NssCommentBundle(),
            new Nss\Bundle\FileBundle\NssFileBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
