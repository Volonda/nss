 ALTER TABLE public.app_orders
  ADD FOREIGN KEY (contact_id) REFERENCES nss_user.contacts(id),
  ADD FOREIGN KEY (owner_id) REFERENCES nss_user.users(id)
 ;

ALTER TABLE public.app_messages
  ADD FOREIGN KEY (sender_id) REFERENCES nss_user.users(id)
  ;

ALTER TABLE public.app_comments
  ADD FOREIGN KEY (owner_id) REFERENCES nss_user.users(id),
  ADD FOREIGN KEY (contact_id) REFERENCES nss_user.contacts(id)
  ;

ALTER TABLE public.app_history
  ADD FOREIGN KEY (transition_id) REFERENCES workflow.app_transitions(id)
;

ALTER TABLE public.app_notification
  ADD FOREIGN KEY (user_id) REFERENCES nss_user.users(id);

ALTER TABLE public.model_template_orders
  ADD FOREIGN KEY (workflow_model_id) REFERENCES workflow.model_workflows(id)
;

ALTER TABLE public.model_orders
  ADD FOREIGN KEY (input_document_template_list_id) REFERENCES doc.template_lists(id),
  ADD FOREIGN KEY (output_document_template_list_id) REFERENCES doc.template_lists(id)
  ;

ALTER TABLE public.app_orders
  ADD FOREIGN KEY (input_document_bundle_id) REFERENCES doc.bundles(id),
  ADD FOREIGN KEY (output_document_bundle_id) REFERENCES doc.bundles(id)
;

CREATE VIEW public.vw_order_workflow_actions AS (
  SELECT
    row_number() over() id,
    waa.action_id,
    o.id order_id,
    w.id workflow_id
  FROM app_orders o
    JOIN workflow.app_workflows w ON w.order_id = o.id
    JOIN workflow.app_workflow_actions wa ON wa.workflow_id = w.id
    JOIN workflow.app_workflow_action_actions waa ON waa.workflow_action_id = wa.id
    JOIN nss_user.actions a ON a.id = waa.action_id
  WHERE wa.current_status_id = o.status_id
        AND a.code IN ('order_view_my', 'moderation_view_all')
);

ALTER TABLE public.model_user_rewards
  ADD FOREIGN KEY (user_id) REFERENCES nss_user.users(id);

