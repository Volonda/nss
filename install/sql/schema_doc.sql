CREATE SCHEMA doc;

CREATE TABLE doc.templates (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  code VARCHAR(30) NOT NULL UNIQUE
);

CREATE TABLE doc.bundles (
  id SERIAL NOT NULL PRIMARY KEY
);

CREATE TABLE doc.documents (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  template_id INT DEFAULT NULL,
  bundle_id INT NOT NULL,
  modified TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  required BOOLEAN DEFAULT NULL,
  is_original BOOLEAN DEFAULT NULL,
  FOREIGN KEY (template_id) REFERENCES doc.templates (id),
  FOREIGN KEY (bundle_id) REFERENCES doc.bundles (id)
);

CREATE TABLE doc.versions (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  document_id INT NOT NULL,
  comment TEXT DEFAULT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  locked BOOLEAN DEFAULT FALSE NOT NULL,
  FOREIGN KEY (document_id) REFERENCES doc.documents (id)
);

CREATE TABLE doc.template_lists (
  id SERIAL NOT NULL  PRIMARY KEY,
  name VARCHAR(255) DEFAULT NULL,
  code VARCHAR(255) DEFAULT NULL,
  modified TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
 );

CREATE TABLE doc.template_in_template_list (
  id SERIAL PRIMARY KEY NOT NULL,
  template_id INT NOT NULL,
  template_list_id INT NOT NULL,
  required BOOLEAN,
  FOREIGN KEY (template_id) REFERENCES doc.templates (id),
  FOREIGN KEY (template_list_id) REFERENCES doc.template_lists (id)
);

CREATE UNIQUE INDEX template_id_template_list_id_uix ON doc.template_in_template_list(template_id, template_list_id);

CREATE TABLE doc.version_files(
  id BIGSERIAL PRIMARY KEY NOT NULL,
  version_id INT NOT NULL,
  file_id INT NOT NULL UNIQUE,
  FOREIGN KEY (version_id) REFERENCES doc.versions(id),
  FOREIGN KEY (file_id) REFERENCES file.files(id)
);