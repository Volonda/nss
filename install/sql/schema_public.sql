CREATE TABLE public.base_order_statuses (
  id SERIAL NOT NULL,
  name VARCHAR(100) NOT NULL,
  action_name VARCHAR(100) NOT NULL,
  code TEXT NOT NULL UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE public.base_reward_types (
  id SERIAL NOT NULL,
  name VARCHAR(100) NOT NULL,
  short_name VARCHAR(100) NOT NULL,
  code TEXT NOT NULL UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE public.base_condition_types (
  id SERIAL NOT NULL,
  name VARCHAR(100) NOT NULL,
  short_name VARCHAR(100) NOT NULL,
  code TEXT NOT NULL UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE public.base_service_types (
  id SERIAL NOT NULL,
  name VARCHAR(100) NOT NULL,
  code TEXT NOT NULL UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE public.base_order_types (
  id SERIAL NOT NULL,
  name VARCHAR(100) NOT NULL,
  code TEXT NOT NULL UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE public.base_form_types(
  id SERIAL NOT NULL,
  name VARCHAR(100) NOT NULL,
  code TEXT NOT NULL UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE public.model_service_form_types(
  id SERIAL NOT NULL PRIMARY KEY,
  options TEXT,
  form_type_id INT NOT NULL,
  title TEXT NOT NULL,
  code TEXT NOT NULL UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (form_type_id) REFERENCES public.base_form_types(id)
);

CREATE TABLE public.model_rewards (
  id SERIAL NOT NULL,
  type_id INT NOT NULL,
  value DECIMAL(12,2) DEFAULT 0,
  name TEXT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (type_id) REFERENCES public.base_reward_types (id)
);

CREATE TABLE public.app_sro (
  id SERIAL NOT NULL,
  sro_type_id INT NOT NULL,
  subject_id INT NOT NULL,
  name VARCHAR(255) NOT NULL,
  short_name TEXT NOT NULL,
  registration_number VARCHAR(100) NOT NULL UNIQUE,
  membership_fee_info TEXT DEFAULT NULL,
  entrance_fee_info TEXT DEFAULT NULL,
  insurance_info TEXT DEFAULT NULL,
  scan_info TEXT DEFAULT NULL,
  original_info TEXT DEFAULT NULL,
  documents_info TEXT DEFAULT NULL,
  has_payout BOOLEAN DEFAULT NULL,
  special_conditions TEXT DEFAULT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id),
  FOREIGN  KEY (sro_type_id) REFERENCES base.sro_types (id),
  FOREIGN KEY (subject_id) REFERENCES base.subjects (id)
);

CREATE TABLE public.model_conditions (
  id SERIAL NOT NULL,
  type_id INT NOT NULL,
  value DECIMAL(12,2) DEFAULT 0,
  name TEXT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (type_id) REFERENCES public.base_condition_types (id)
);

CREATE TABLE public.base_services (
  id SERIAL NOT NULL,
  type_id INT NOT NULL,
  name VARCHAR(255) NOT NULL,
  code VARCHAR(100) NOT NULL UNIQUE,
  description VARCHAR(255) DEFAULT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (type_id) REFERENCES public.base_service_types (id)
);

CREATE TABLE public.model_orders (
  id SERIAL NOT NULL PRIMARY KEY,
  input_document_template_list_id INT,
  output_document_template_list_id INT,
  enabled BOOLEAN NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  model_type VARCHAR (20) NOT NULL
);

CREATE TABLE public.model_sro_orders (
  id SERIAL NOT NULL PRIMARY KEY,
  sro_id INT NOT NULL,
  template_id INT NOT NULL,
  is_rewrite BOOLEAN NOT NULL,
  FOREIGN KEY (template_id) REFERENCES public.model_orders(id),
  FOREIGN KEY (id) REFERENCES public.model_orders(id),
  FOREIGN KEY (sro_id) REFERENCES public.app_sro (id)
);

CREATE TABLE public.model_template_orders (
  id SERIAL NOT NULL PRIMARY KEY,
  type_id INT NOT NULL,
  title VARCHAR(255) NOT NULL,
  status_id INT NOT NULL,
  workflow_model_id INT NOT NULL,
  FOREIGN KEY (type_id) REFERENCES public.base_order_types (id),
  FOREIGN KEY (id) REFERENCES public.model_orders(id),
  FOREIGN KEY (status_id) REFERENCES public.base_order_statuses(id)
);

CREATE TABLE public.model_services (
  id SERIAL NOT NULL,
  service_id INT NOT NULL,
  order_model_id INT NOT NULL,
  service_form_type_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (service_id) REFERENCES public.base_services (id),
  FOREIGN KEY (order_model_id) REFERENCES public.model_orders (id),
  FOREIGN KEY (service_form_type_id) REFERENCES public.model_service_form_types(id)
);

CREATE UNIQUE INDEX service_id_order_model_id_uix ON public.model_services(service_id, order_model_id);

CREATE TABLE public.base_options (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  code TEXT NOT NULL UNIQUE,
  description VARCHAR(255) DEFAULT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL
);

CREATE TABLE public.model_options (
  id SERIAL NOT NULL PRIMARY KEY,
  service_model_id INT NOT NULL,
  option_id INT NOT NULL,
  condition_model_id INT,
  reward_model_id INT,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (service_model_id) REFERENCES public.model_services (id),
  FOREIGN KEY (option_id) REFERENCES public.base_options (id),
  FOREIGN KEY (condition_model_id) REFERENCES public.model_conditions (id),
  FOREIGN KEY (reward_model_id) REFERENCES public.model_rewards (id)
);

CREATE UNIQUE INDEX service_model_id_option_id_uix ON public.model_options(service_model_id, option_id);

CREATE TABLE public.model_user_rewards (
  id SERIAL NOT NULL PRIMARY KEY,
  user_id INT NOT NULL,
  reward_model_id INT NOT NULL,
  option_model_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (reward_model_id) REFERENCES public.model_rewards(id),
  FOREIGN KEY (option_model_id) REFERENCES public.model_options (id)
);
CREATE UNIQUE INDEX user_id_reward_model_id_option_model_id_uix ON  public.model_user_rewards(user_id,reward_model_id,option_model_id);

--Данные
CREATE TABLE public.app_orders (
  id BIGSERIAL PRIMARY KEY,
  order_model_id INT NOT NULL,
  sro_id INT NOT NULL,
  type_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  contact_id INT NOT NULL,
  owner_id INT NOT NULL,
  status_id INT NOT NULL,
  reward_total DECIMAL(12,2) DEFAULT 0,
  condition_total DECIMAL(12,2) DEFAULT 0,
  input_document_bundle_id INT,
  output_document_bundle_id INT,
  FOREIGN KEY (sro_id) REFERENCES public.app_sro(id),
  FOREIGN KEY (type_id) REFERENCES public.base_order_types(id),
  FOREIGN KEY (status_id) REFERENCES public.base_order_statuses(id),
  FOREIGN KEY (order_model_id) REFERENCES public.model_orders(id)
);

CREATE TABLE public.app_services (
  id BIGSERIAL PRIMARY KEY,
  order_id INT NOT NULL,
  title TEXT NOT NULL,
  base_service_id INT NOT NULL,
  service_model_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (order_id) REFERENCES public.app_orders(id),
  FOREIGN KEY (base_service_id) REFERENCES public.base_services(id),
  FOREIGN KEY (service_model_id) REFERENCES public.model_services(id)
);

CREATE UNIQUE INDEX order_id_base_service_id_uix ON public.app_services(order_id, base_service_id);

CREATE TABLE public.app_conditions(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  type_id INT NOT NULL,
  value DECIMAL(12,2) DEFAULT 0,
  name TEXT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (type_id) REFERENCES public.base_condition_types (id)
);

CREATE  TABLE app_rewards(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  type_id INT NOT NULL,
  name TEXT NOT NULL,
  value DECIMAL(12,2) DEFAULT 0,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (type_id) REFERENCES public.base_reward_types (id)
);

CREATE TABLE public.app_options (
  id BIGSERIAL PRIMARY KEY,
  service_id INT NOT NULL UNIQUE,
  base_option_id INT NOT NULL,
  reward_id INT,
  condition_id INT,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (service_id) REFERENCES public.app_services(id),
  FOREIGN KEY (base_option_id) REFERENCES public.base_options(id),
  FOREIGN KEY (reward_id) REFERENCES public.app_rewards,
  FOREIGN KEY (condition_id) REFERENCES public.app_conditions
);

CREATE UNIQUE INDEX service_id_base_option_id_uix ON public.app_options(service_id, base_option_id);

CREATE TABLE public.app_organizations(
  id BIGSERIAL PRIMARY KEY,
  inn VARCHAR (20) NOT NULL,
  short_name TEXT,
  full_name TEXT,
  type_id INT NOT NULL,
  seldon_data JSONB,
  seldon_data_updated_at TIMESTAMP(0) WITHOUT TIME ZONE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (type_id) REFERENCES base.organization_types(id)
);

CREATE TABLE public.app_history(
  id BIGSERIAL PRIMARY KEY,
  order_id INT,
  transition_id INT,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  type VARCHAR (30),
  FOREIGN KEY (order_id) REFERENCES app_orders(id)
);

CREATE TABLE public.app_messages(
  id BIGSERIAL PRIMARY KEY,
  sender_id INT NOT NULL,
  message TEXT NOT NULL,
  order_id INT,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  type VARCHAR (30),
  FOREIGN KEY (order_id) REFERENCES app_orders(id)
);

CREATE TABLE public.app_sro_contacts(
  id BIGSERIAL PRIMARY KEY,
  phone TEXT NOT NULL,
  email TEXT NOT NULL,
  fio TEXT NOT NULL,
  sro_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY(sro_id) REFERENCES public.app_sro(id)
);

CREATE TABLE public.app_comments(
  id BIGSERIAL PRIMARY KEY,
  owner_id INT NOT NULL,
  contact_id INT,
  order_id INT,
  message TEXT NOT NULL,
  type VARCHAR(20) NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY(order_id) REFERENCES public.app_orders(id)
);

CREATE TABLE public.app_notification(
  id BIGSERIAL PRIMARY KEY,
  user_id INTEGER NOT NULL,
  title VARCHAR(255) NOT NULL,
  text TEXT NOT NULL,
  subject_id INTEGER,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE,
  read_at TIMESTAMP(0) WITHOUT TIME ZONE
);