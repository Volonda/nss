CREATE schema workflow;

CREATE TABLE workflow.base_workflows(
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  code VARCHAR(50) UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL
);

CREATE TABLE workflow.app_workflows(
  id BIGSERIAL PRIMARY KEY,
  base_workflow_id INT NOT NULL,
  order_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  FOREIGN KEY (order_id) REFERENCES public.app_orders (id),
  FOREIGN KEY (base_workflow_id) REFERENCES workflow.base_workflows (id)
);

CREATE TABLE workflow.model_workflows(
  id BIGSERIAL PRIMARY KEY,
  base_workflow_id INT NOT NULL,
  FOREIGN KEY (base_workflow_id) REFERENCES workflow.base_workflows(id)
);

CREATE TABLE workflow.model_transitions(
  id BIGSERIAL PRIMARY KEY,
  current_status_id INT NOT NULL,
  transition_status_id INT NOT NULL,
  workflow_model_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY (workflow_model_id) REFERENCES workflow.model_workflows (id),
  FOREIGN KEY (current_status_id) REFERENCES public.base_order_statuses(id),
  FOREIGN KEY (transition_status_id) REFERENCES public.base_order_statuses(id)
);

CREATE TABLE workflow.app_transitions(
  id BIGSERIAL PRIMARY KEY,
  current_status_id INT NOT NULL,
  transition_status_id INT NOT NULL,
  order_workflow_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  FOREIGN KEY (order_workflow_id) REFERENCES workflow.app_workflows (id),
  FOREIGN KEY (current_status_id) REFERENCES public.base_order_statuses(id),
  FOREIGN KEY (transition_status_id) REFERENCES public.base_order_statuses(id)
);


CREATE TABLE workflow.model_transition_groups(
  id BIGSERIAL PRIMARY KEY,
  group_id INT NOT NULL,
  transition_model_id INT NOT NULL,
  FOREIGN KEY (transition_model_id) REFERENCES workflow.model_transitions(id),
  FOREIGN KEY (group_id) REFERENCES nss_user.groups(id)
);

CREATE TABLE workflow.app_transition_groups(
  id BIGSERIAL PRIMARY KEY,
  group_id INT NOT NULL,
  transition_id INT NOT NULL,
  FOREIGN KEY (transition_id) REFERENCES workflow.app_transitions(id),
  FOREIGN KEY (group_id) REFERENCES nss_user.groups(id)
);

CREATE TABLE workflow.model_workflow_actions(
  id BIGSERIAL PRIMARY KEY,
  current_status_id INT NOT NULL,
  workflow_model_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  FOREIGN KEY (current_status_id) REFERENCES public.base_order_statuses(id),
  FOREIGN KEY (workflow_model_id) REFERENCES workflow.model_workflows(id)
);

CREATE TABLE workflow.app_workflow_actions(
  id BIGSERIAL PRIMARY KEY,
  current_status_id INT NOT NULL,
  workflow_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  FOREIGN KEY (current_status_id) REFERENCES public.base_order_statuses(id),
  FOREIGN KEY (workflow_id) REFERENCES workflow.app_workflows(id)
);

CREATE TABLE workflow.model_workflow_action_actions(
  id BIGSERIAL PRIMARY KEY,
  workflow_model_action_id INT NOT NULL,
  action_id INT NOT NULL,
  FOREIGN KEY (workflow_model_action_id) REFERENCES workflow.model_workflow_actions(id),
  FOREIGN KEY (action_id) REFERENCES nss_user.actions(id)
);

CREATE TABLE workflow.app_workflow_action_actions(
  id BIGSERIAL PRIMARY KEY,
  workflow_action_id INT NOT NULL,
  action_id INT NOT NULL,
  FOREIGN KEY (workflow_action_id) REFERENCES workflow.app_workflow_actions(id),
  FOREIGN KEY (action_id) REFERENCES nss_user.actions(id)
);


CREATE UNIQUE INDEX model_transitions_workflow_model_id_transition_status_id_current_status_id_uix ON workflow.model_transitions (current_status_id, transition_status_id,workflow_model_id);
CREATE UNIQUE INDEX model_workflow_actions_current_status_id_base_workflow_id_uix ON workflow.model_workflow_actions (current_status_id, workflow_model_id);
