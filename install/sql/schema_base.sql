START TRANSACTION;

CREATE SCHEMA base;

-- base
CREATE TABLE base.federal_districts (
  id SERIAL NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  code TEXT NOT NULL UNIQUE,
  is_foreign BOOLEAN NOT NULL
);
CREATE TABLE base.subjects (
  id SERIAL NOT NULL  PRIMARY KEY,
  federal_district_id INT DEFAULT NULL,
  name TEXT NOT NULL,
  code TEXT NOT NULL UNIQUE,
  is_foreign BOOLEAN NOT NULL,
  FOREIGN KEY (federal_district_id) REFERENCES base.federal_districts (id)
);
CREATE TABLE base.organization_types (
  id SERIAL NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  code TEXT NOT NULL UNIQUE
);

CREATE TABLE base.sro_types (
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(100) NOT NULL,
  code TEXT NOT NULL UNIQUE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL
);

