CREATE SCHEMA nss_user;

CREATE TABLE nss_user.users (
  id SERIAL NOT NULL PRIMARY KEY,
  username VARCHAR(180) NOT NULL,
  username_canonical VARCHAR(180) NOT NULL UNIQUE,
  email VARCHAR(180) NOT NULL,
  email_canonical VARCHAR(180) NOT NULL UNIQUE,
  enabled BOOLEAN NOT NULL,
  salt VARCHAR(255) DEFAULT NULL,
  password VARCHAR(255) NOT NULL,
  last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  confirmation_token VARCHAR(180) DEFAULT NULL UNIQUE,
  password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  roles TEXT NOT NULL,
  sro_id INT,
  FOREIGN KEY (sro_id) REFERENCES public.app_sro(id)
);

CREATE TABLE nss_user.contacts(
  id BIGSERIAL PRIMARY KEY,
  owner_id INT NOT NULL,
  phone TEXT,
  email TEXT,
  fio TEXT NOT NULL,
  position TEXT,
  description TEXT,
  organization_id INT NOT NULL,
  is_agent BOOLEAN NOT NULL DEFAULT FALSE,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY(organization_id) REFERENCES public.app_organizations(id),
  FOREIGN KEY(owner_id) REFERENCES nss_user.users(id)
);

CREATE TABLE nss_user.bank_details(
  id BIGSERIAL PRIMARY KEY,
  owner_id INT NOT NULL,
  title TEXT NOT NULL,
  bic TEXT NOT NULL,
  correspondent_account TEXT NOT NULL,
  checking_account TEXT NOT NULL,
  organization_id INT NOT NULL,
  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  FOREIGN KEY(organization_id) REFERENCES public.app_organizations(id),
  FOREIGN KEY(owner_id) REFERENCES nss_user.users(id)
);

CREATE TABLE nss_user.groups (
  id SERIAL PRIMARY KEY,
  name CHARACTER VARYING(100) NOT NULL UNIQUE,
  title TEXT NOT NULL,
  roles TEXT NOT NULL
);

CREATE TABLE nss_user.user_groups (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL ,
  group_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES nss_user.users(id),
  FOREIGN KEY (group_id) REFERENCES nss_user.groups(id)
);

CREATE TABLE nss_user.actions(
  id SERIAL PRIMARY KEY,
  code CHARACTER VARYING(100) NOT NULL UNIQUE,
  title TEXT NOT NULL,
  roles JSONB NOT NULL,
  is_workflow_list BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE nss_user.action_groups(
  id SERIAL PRIMARY KEY,
  action_id INTEGER NOT NULL,
  group_id INTEGER NOT NULL
);