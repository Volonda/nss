/** users*/
INSERT INTO nss_user.users (username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) VALUES ('agent', 'agent', 'agent@nss.ru', 'agent@nss.ru', true, NULL, '$2y$13$lRY8AxmDa6TEEwEju8PlO.q5X.skBqIDfPW2ahtbBmfCyyyiv4ocO', NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_AGENT";}');
INSERT INTO nss_user.users (username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) VALUES ('operator', 'operator', 'operator@nss.ru', 'operator@nss.ru', true, NULL, '$2y$13$gIbMIm3NUyZ9wLchQ8mZn.yn81ItxGWei3xAFTmj3TWbcIMXgrXJi', '2017-06-13 16:28:39', NULL, NULL, 'a:1:{i:0;s:13:"ROLE_OPERATOR";}');
INSERT INTO nss_user.users (username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) VALUES ('admin', 'admin', 'admin@nss.ru', 'admin@nss.ru', true, NULL, '$2y$13$gIbMIm3NUyZ9wLchQ8mZn.yn81ItxGWei3xAFTmj3TWbcIMXgrXJi', NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}');
INSERT INTO nss_user.users (username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) VALUES ('moderator', 'moderator', 'moderator@nss.ru', 'moderator@nss.ru', true, NULL, '$2y$13$gIbMIm3NUyZ9wLchQ8mZn.yn81ItxGWei3xAFTmj3TWbcIMXgrXJi', NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_MODERATOR";}');

/** base.condition_types*/
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Москва', '1');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Санкт-Петербург', '2');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Центральный ФО', '3');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Южный ФО', '4');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Северо-Западный ФО', '5');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Дальневосточный ФО', '6');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Сибирский ФО', '7');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Уральский ФО', '8');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Приволжский ФО', '9');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (false, 'Северо-Кавказский ФО', '10');
INSERT INTO base.federal_districts (is_foreign, name, code) VALUES (true, 'Иные территории', '11');

/** base.subjects */
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (4, false, 'Севастополь', '92');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (11, true, 'Иные территории', '00');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (1, false, 'Москва', '77');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (2, false, 'Санкт-Петербург', '78');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Белгородская область', '31');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Брянская область', '32');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Владимирская область', '33');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Воронежская область', '36');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Ивановская область', '37');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Калужская область', '40');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Курская область', '46');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Липецкая область', '48');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Московская область', '50');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Орловская область', '57');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Рязанская область', '62');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Смоленская область', '67');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Тамбовская область', '68');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Тверская область', '69');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Тульская область', '71');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (3, false, 'Ярославская область', '76');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (4, false, 'Республика Адыгея', '01');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (4, false, 'Республика Калмыкия', '08');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (4, false, 'Краснодарский край', '23');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (4, false, 'Астраханская область', '30');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (4, false, 'Волгоградская область', '34');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (4, false, 'Ростовская область', '61');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Республика Карелия', '10');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Республика Коми', '11');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Архангельская область', '29');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Вологодская область', '35');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Калининградская область', '39');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Ленинградская область', '47');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Мурманская область', '51');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Новгородская область', '53');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Псковская область', '60');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (5, false, 'Ненецкий автономный округ', '83');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Республика Саха (Якутия)', '14');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Камчатский край', '41');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Приморский край', '25');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Хабаровский край', '27');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Амурская область', '28');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Магаданская область', '49');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Сахалинская область', '65');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Еврейская автономная область', '79');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (6, false, 'Чукотский автономный округ', '87');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Республика Алтай', '04');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Республика Бурятия', '03');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Республика Тыва', '17');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Республика Хакасия', '19');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Алтайский край', '22');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Забайкальский край', '75');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Красноярский край', '24');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Иркутская область', '38');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Кемеровская область', '42');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Новосибирская область', '54');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Омская область', '55');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (7, false, 'Томская область', '70');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (8, false, 'Курганская область', '45');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (8, false, 'Свердловская область', '66');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (8, false, 'Тюменская область', '72');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (8, false, 'Челябинская область', '74');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (8, false, 'Ханты-Мансийский автономный округ - Югра', '86');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (8, false, 'Ямало-Ненецкий автономный округ', '89');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Республика Башкортостан', '02');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Республика Марий Эл', '12');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Республика Мордовия', '13');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Республика Татарстан', '16');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Удмуртская республика', '18');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Чувашская республика', '21');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Пермский край', '59');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Кировская область', '43');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Нижегородская область', '52');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Оренбургская область', '56');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Пензенская область', '58');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Самарская область', '63');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Саратовская область', '64');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (9, false, 'Ульяновская область', '73');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (10, false, 'Республика Дагестан', '05');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (10, false, 'Республика Ингушетия', '06');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (10, false, 'Кабардино-Балкарская республика', '07');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (10, false, 'Карачаево-Черкесская республика', '09');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (10, false, 'Республика Северная Осетия — Алания', '15');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (10, false, 'Чеченская республика', '20');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (10, false, 'Ставропольский край', '26');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (11, true, 'Иностранное юридическое лицо', '99');
INSERT INTO base.subjects (federal_district_id, is_foreign, name, code) VALUES (4, false, 'Республика Крым', '91');

/** document_templates*/
INSERT INTO public.base_form_types(name,code,created_at, updated_at) VALUES
  ('Текстовое поле (TextType)', 'Symfony\Component\Form\Extension\Core\Type\TextType', CURRENT_DATE, CURRENT_DATE),
  ('Поле выбора (ChoiceType)', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', CURRENT_DATE, CURRENT_DATE)
  ;

/** actions*/
INSERT INTO nss_user.actions (code,title,roles, is_workflow_list) VALUES ('order_create','Заявления. Создание заявлений', '["ROLE_OPERATOR","ROLE_AGENT","ROLE_ADMIN"]', false);
INSERT INTO nss_user.actions (code,title,roles, is_workflow_list) VALUES ('order_edit_my','Заявления. Редактирование моих заявлений', '["ROLE_OPERATOR","ROLE_AGENT","ROLE_ADMIN"]', true);
INSERT INTO nss_user.actions (code,title,roles, is_workflow_list) VALUES ('order_view_my','Заявления. Просмотр моих заявлений', '["ROLE_AGENT"]', true);
INSERT INTO nss_user.actions (code,title,roles, is_workflow_list) VALUES ('moderation','Модерация. Обработка заявлений', '["ROLE_OPERATOR","ROLE_ADMIN"]', true);
INSERT INTO nss_user.actions (code,title,roles, is_workflow_list) VALUES ('moderation_view_all','Модерация. Просмотр всех заявлений', '["ROLE_OPERATOR","ROLE_ADMIN"]', true);
INSERT INTO nss_user.actions (code,title,roles, is_workflow_list) VALUES ('contact_my','Контакты. Просмотр и добавление моих контактов', '["ROLE_AGENT"]', false);
INSERT INTO nss_user.actions (code,title,roles, is_workflow_list) VALUES ('sro_view','СРО. Просмотр всех СРО', '["ROLE_OPERATOR","ROLE_AGENT","ROLE_ADMIN"]', false);

/** groups*/
INSERT INTO nss_user.groups (name,title,roles) VALUES ('admin','Админ','a:1:{i:0;s:10:"ROLE_ADMIN";}');
INSERT INTO nss_user.groups (name,title,roles) VALUES ('operator','Оператор','a:1:{i:0;s:13:"ROLE_OPERATOR";}');
INSERT INTO nss_user.groups (name,title,roles) VALUES ('agent','Агент','a:1:{i:0;s:10:"ROLE_AGENT";}');
INSERT INTO nss_user.groups (name,title,roles) VALUES ('moderator','Модератор','a:1:{i:0;s:14:"ROLE_MODERATOR";}');

/** user_groups*/
INSERT INTO nss_user.user_groups (user_id,group_id) VALUES ((SELECT id FROM nss_user.users WHERE username = 'agent'),(SELECT id FROM nss_user.groups WHERE name = 'agent'));
INSERT INTO nss_user.user_groups (user_id,group_id) VALUES ((SELECT id FROM nss_user.users WHERE username = 'admin'),(SELECT id FROM nss_user.groups WHERE name = 'admin'));
INSERT INTO nss_user.user_groups (user_id,group_id) VALUES ((SELECT id FROM nss_user.users WHERE username = 'operator'),(SELECT id FROM nss_user.groups WHERE name = 'operator'));
INSERT INTO nss_user.user_groups (user_id,group_id) VALUES ((SELECT id FROM nss_user.users WHERE username = 'moderator'),(SELECT id FROM nss_user.groups WHERE name = 'moderator'));


/** action_groups*/
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'order_create'),(SELECT id FROM nss_user.groups WHERE name = 'agent'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'order_edit_my'),(SELECT id FROM nss_user.groups WHERE name = 'agent'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'order_view_my'),(SELECT id FROM nss_user.groups WHERE name = 'agent'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'contact_my'),(SELECT id FROM nss_user.groups WHERE name = 'agent'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'moderation'),(SELECT id FROM nss_user.groups WHERE name = 'operator'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'moderation_view_all'),(SELECT id FROM nss_user.groups WHERE name = 'operator'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'sro_view'),(SELECT id FROM nss_user.groups WHERE name = 'agent'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'sro_view'),(SELECT id FROM nss_user.groups WHERE name = 'operator'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'sro_view'),(SELECT id FROM nss_user.groups WHERE name = 'admin'));
INSERT INTO nss_user.action_groups (action_id, group_id) VALUES ((SELECT id FROM nss_user.actions WHERE code = 'sro_view'),(SELECT id FROM nss_user.groups WHERE name = 'moderator'));

INSERT INTO public.base_order_statuses(name, action_name, code, created_at, updated_at) VALUES ('Черновик', 'Черновик' ,'draft', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO public.base_order_statuses(name, action_name, code, created_at, updated_at) VALUES ('Ожидает обработки', 'Отправить' ,'queue', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO public.base_order_statuses(name, action_name, code, created_at, updated_at) VALUES ('В работе', 'Взять в работу','processing', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO public.base_order_statuses(name, action_name, code, created_at, updated_at) VALUES ('Обработано', 'Обработано', 'processed',  CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

/*base_organization_types*/
INSERT INTO base.organization_types(name, code) VALUES ('ИП','ip'), ('ЮЛ','ul');

COMMIT;