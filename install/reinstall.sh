#!/bin/bash
set -e

dbName=$(grep "database_name:" app/config/parameters.yml |awk '{print $2}')
dbUser=$(grep "database_user:" app/config/parameters.yml |awk '{print $2}')

sudo -u postgres -i psql -c "DROP DATABASE IF EXISTS ${dbName};"
sudo -u postgres -i psql -c "CREATE DATABASE ${dbName} OWNER ${dbUser}"
sudo -u postgres -i psql -c "GRANT CREATE ON DATABASE ${dbName} TO ${dbUser}"

install/install.sh
