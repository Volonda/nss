#!/bin/bash
set -e

sudo chmod 777 var -R

dbPasswrd=$(grep -e "database_password:" app/config/parameters.yml |awk '{print $2}')
dbUser=$(grep "database_user:" app/config/parameters.yml |awk '{print $2}')
dbName=$(grep "database_name:" app/config/parameters.yml |awk '{print $2}')

#set PGPASSWORD=${dbPasswrd}
cat install/sql/schema_base.sql install/sql/schema_file.sql install/sql/schema_public.sql install/sql/schema_nss_user.sql install/sql/schema_workflow.sql install/sql/schema_doc.sql install/sql/after_schema_create.sql  install/sql/data.sql| PGPASSWORD=${dbPasswrd} psql -U ${dbUser} -d ${dbName}

bin/console d:f:l --append
bin/console cache:clear