# config valid only for current version of Capistrano
lock ">=3.6"

set :application, "nss-app"
set :repo_url, "git@77.244.22.226:nss/nss-app.git"

set :tmp_dir, "/tmp/nss-app"

# Branch
set :branch, ENV['BRANCH'] if ENV['BRANCH']
 
# Default value for keep_releases is 5
set :keep_releases, 2

# Set this to 2 for the old directory structure
set :symfony_directory_structure, 3
# Set this to 4 if using the older SensioDistributionBundle
set :sensio_distribution_version, 5

# symfony-standard edition directories
set :app_path, "app"
set :web_path, "web"
set :var_path, "var"
set :bin_path, "bin"

set :symfony_console_path, "bin/console"
set :log_path,              fetch(:var_path) + "/logs"
set :cache_path,            fetch(:var_path) + "/cache"
set :app_config_path,       fetch(:app_path) + "/config"

# Remove app_dev.php during deployment, other files in web/ can be specified here
set :controllers_to_clear, ["app_*.php"]

# Share files/directories between releases
set :linked_files, ["app/config/parameters.yml"]
set :linked_dirs, ["var/logs","uploads"]

set :composer_install_flags, '--optimize-autoloader'

after 'deploy:updated', 'symfony:assets:install'
before 'deploy', 'deploy:permissions'


set :use_set_permissions, true
set :permission_method, :chmod
set :file_permissions_users, ["www"]
set :file_permissions_paths, ["var"]

set :webserver_user, "www"

namespace :deploy do
    task :permissions do
        on roles(:all) do
            if test("[ -d #{release_path} ]")
                execute "cd '#{release_path}'; sudo chmod 777 var -R ;"
                puts "set permissions for #{release_path}"
            end
        end
    end
end
