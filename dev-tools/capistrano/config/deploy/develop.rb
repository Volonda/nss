set :deploy_to, "/var/www/nss-app"
set :symfony_env,  "prod"

server "151.248.115.71", user: "deploy", roles: %w{db}

set :ssh_options, {
    keys: %w(/home/user/.ssh/id_rsa),
    forward_agent: true,
    auth_methods: %w(publickey password)
}