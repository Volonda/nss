class Build
  def Build.configure(config, settings)

    # Configure The Box
    config.vm.box = "ubuntu/xenial64"

    # Configure A Private Network IP
    config.vm.network :private_network, ip: settings["ip"] ||= "10.2.2.11"

    if settings['networking'][0]['public']
      config.vm.network "public_network", type: "dhcp"
    end

    # Configure A Few VirtualBox Settings
    config.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", settings["memory"] ||= "2048"]
      vb.customize ["modifyvm", :id, "--cpus", settings["cpus"] ||= "1"]
      # For NAT adapter
      vb.customize ["modifyvm", :id, "--nictype1", settings["nictype1"] ||= "Am79C973"]
      # For host-only adapter
      vb.customize ["modifyvm", :id, "--nictype2", settings["nictype2"] ||= "Am79C973"]
    end

    # Configure Port Forwarding To The Box
    config.vm.network "forwarded_port", guest: 80, host: 8000
    config.vm.network "forwarded_port", guest: 443, host: 44300
    config.vm.network "forwarded_port", guest: 5432, host: 54320

    # Add Custom Ports From Configuration
    if settings.has_key?("ports")
      settings["ports"].each do |port|
        config.vm.network "forwarded_port", guest: port["guest"], host: port["host"], protocol: port["protocol"] ||= "tcp"
      end
    end

    # Register All Of The Configured Shared Folders
    if settings['folders'].kind_of?(Array)
      settings["folders"].each do |folder|
        config.vm.synced_folder folder["map"], folder["to"], type: folder["type"] ||= nil
      end
    end

    config.vm.provision :shell, path: "dev-tools/vagrant/provision.sh"

  end
end