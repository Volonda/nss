#!/usr/bin/env bash

#install composer
sudo apt-get install -y composer

#set time zone
sudo timedatectl set-timezone Europe/Moscow

# add php 7.1
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update
sudo apt-get  -y --no-install-recommends install  php7.1-pgsql php7.1-intl php7.1-mbstring php7.1-xsl php-yaml php7.1-xml php7.1-cli php7.1-fpm php7.1-soap php7.1-curl php-xdebug php-yaml php7.1-gd php7.1-zip
sudo apt-get clean;

# install nginx
sudo apt-get install -y nginx

#install postgres
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y postgresql-9.6

#config postgres
sudo -u postgres psql -c "alter user postgres encrypted password 'postgres';"
sudo sed -i "s/\#listen_addresses = 'localhost'/listen_addresses = \'*\'/" /etc/postgresql/9.6/main/postgresql.conf
sudo echo "host    all    all    0.0.0.0/0    md5" >> /etc/postgresql/9.6/main/pg_hba.conf
sudo service postgresql restart

#create db
sudo -u postgres psql -c "create database nss"
sudo -u postgres psql -c "create user dev with password 'dev'"
sudo -u postgres psql -c "grant all privileges on database nss to dev"

#create developer and grant ssh access
sudo addgroup www
sudo adduser www-data www

sudo adduser dev --quiet --disabled-password --gecos "dev"
sudo passwd -l dev
sudo echo "dev ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/dev
sudo adduser dev www

sudo chown -R :www  /var/www
sudo chmod -R g+rwX /var/www
sudo chmod g+s /var/www

sudo mkdir /home/dev/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDa6BO9e4gKFDxDQ9LBL5n/gGPHMiUCf1ZBC8hEeXewOjERlhQTQpZoyJZtMl4tAlYUlGZ4+3SDNGjmOUs/yKKBVAXbo1DjY2l6BRS3lQ596/UVMqCoMTWxC6eJ7nzaY1zal75mCrcBKsAGf1SpjGyA7mL9XbDkAlAtkdHIu0JtaUWQx/7q0DuqZN/5ZCWlfIUb7clJc7n2LHgdLUpafYBzGPPXwJ58I6lExJs5zOzW9dNi8UYQxTAd5TUKprj+fsrq46trCUjaKy/MicRqRUDXdo+P3VzdR+Ej7bAarM+YW8k5DWIBdB9DiGtjDqXnuPHODi8tfU2zbbHBRpWfy3bj me@pc3871" | sudo tee --append  /home/dev/.ssh/authorized_keys
sudo chown dev:dev /home/dev/.ssh
sudo chmod 700 /home/dev/.ssh
sudo chown dev:dev /home/dev/.ssh/authorized_keys
sudo chmod 600 /home/dev/.ssh/authorized_keys

sudo tee /home/dev/.ssh/id_rsa <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEA7KEbMcqOiN0vPZ7WYUh9mNjyA4Lf7YzYBU6PBgiqFrKDbIaS
gkEwabsBReYq8Ffoli37nTAD4CE/1AtzS/PYy4wb1RiNj+ZBFp2rx6AJRMi7Vi7c
BIo+5T4ZhL2gvNDEGCM/N8Xar1a98qfJ1uLoQ3fQV2Zx1B/g5SzqJ6rJhd5kPE6i
YWTMM+c6f4wOg7nNjzDgkXOVZhtaYJhPlYTLwurKFk8AwsqZmfC9gOgB8eRcGSr9
nEAgFSlyk5HrswTvQ+28wi1JLw5gxx3x9CerEDTnJsBIOpVIG4RxeYx4LO9vAf48
0MseKDQp4RHXyJ1jIVILpFyVgjBYujxNaHvx3wIDAQABAoIBAGTjU5rpB9hmpYua
O0EDDTmr1ZOVqy+3CytIEUa7TvmaZtpQjgqnDKMnriVFOy157ZqKRGsCW8SlqHcZ
4rCRaO9v7zXt3/1oaHviAal9HDaNdwIJvJja8HIBBhRIXU3jnJfF/2AiJVj4nMfm
rfHGkMyjOtrovMN6TAKqIbOD56QBGcyGw7OQ7N/KSguvOO1tEzu+PkgaDeUxt3SM
nNO7kKP2USTD6ND5pMhHWCTDdXRcTKU6C8Ac6yztHILYevHPSkiOyNRV/GayxRUI
sCGgBW5BF3rWlWcX+o9/uLifpc2RTcrROYziCJTxRUOfdojh5GVKz2WNjHRBD1Yl
deL2gRECgYEA+KV0XIQx80YtVoRRo7O5SFAZB+g/s68dG28Ezmkm7tueBOHkgK8w
tgEvOiQBLVMm5Q4yVc1kDe22ogCrWeaqV+OsAymqLp7c83hfcpAJKWP/o+XSq81D
eD/3U+OTDgkmh1itrSuYsKJOhaE96irKH3lPhcUZSXhxPH9BTJNR4b0CgYEA86Cr
QJV0qXa4uCbA7PpZF1C0/WpB/a8B8PlYRRY6YoYoRxwSkZ9er/96LNsZtqA6AHTW
0jByBSCcc4bQ98ROwNy4OtKEc/VK2At2Mzd7NvI8gMRO7QR6KfEK1Q6+AV9KOI8t
AoPxfl8y5G3QR8QLZXPeMvj2WxoYfzQs0zPuRcsCgYAW8yaPV4BIxQRwRNxIMNF8
NDBrQ2uFyTb1jkSyTJBUYZ7vMm5l1kW3ttpGEoatSUyAvdO1ibSfeH5v64HlVV4J
qLD6xcC4dt9L9aNRrqu5z0oMNa1BQDvnAHhynMG4/O06dtvo7Oo1+3Ul7mEJmRN4
3jiRz0RrPJuvCsvbJonbPQKBgASx8PrypmfpqM+rt4YZoQPpA0Pq+1CXQdNSCthy
ADgL29Ta/qSdzKZZpxIcOtehKNz/Y3BzjI76vOkhDP/pYZcn6ckAa5sMfB/DwMhl
5lL9whq9LcQ1lOEgCxRmD1VZb8W9RKHKxymnTgTG9P+dEJco73xN/fltfFFfQQnk
80+XAoGALZmE2pOfz/BvvFeZVJQ2L2iT/lVVmArCyMsFmbabXSC/W1rQjD3NDYXD
tDEzW2w1bb99h9jB0II30ojKBXfYB54YPdi+YG+Ma1jfISPzcMaUqdYKutmhE1lH
h3syJIG+NPniVPG7/5dq9CofkSJtadPYDuMvgHxaAfo+N3CRL+U=
-----END RSA PRIVATE KEY-----
EOF
sudo chown dev:dev /home/dev/.ssh/id_rsa
sudo chmod 600 /home/dev/.ssh/id_rsa

sudo tee /home/dev/.ssh/id_rsa.pub <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDsoRsxyo6I3S89ntZhSH2Y2PIDgt/tjNgFTo8GCKoWsoNshpKCQTBpuwFF5irwV+iWLfudMAPgIT/UC3NL89jLjBvVGI2P5kEWnavHoAlEyLtWLtwEij7lPhmEvaC80MQYIz83xdqvVr3yp8nW4uhDd9BXZnHUH+DlLOonqsmF3mQ8TqJhZMwz5zp/jA6Duc2PMOCRc5VmG1pgmE+VhMvC6soWTwDCypmZ8L2A6AHx5FwZKv2cQCAVKXKTkeuzBO9D7bzCLUkvDmDHHfH0J6sQNOcmwEg6lUgbhHF5jHgs728B/jzQyx4oNCnhEdfInWMhUgukXJWCMFi6PE1oe/Hf dev@ubuntu-xenial
EOF
sudo chown dev:dev /home/dev/.ssh/id_rsa.pub
sudo chmod 600 /home/dev/.ssh/id_rsa.pub
sudo -u dev ssh-keyscan -H 172.16.40.104 >> /home/dev/.ssh/known_hosts

#create vhost
#create vhost
sudo tee /etc/nginx/sites-available/nss.local <<EOF
server {
    server_name nss.dev www.nss.dev;
    root /var/www/nss/web;

    location / {
        # try to serve file directly, fallback to app.php
        try_files \$uri /app.php\$is_args\$args;
    }
    # DEV
    # This rule should only be placed on your development environment
    # In production, don't include this and don't deploy app_dev.php or config.php
    location ~ ^/(app_dev|config)\.php(/|\$) {
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)\$;
        include fastcgi_params;
        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT \$realpath_root;
    }
    # PROD
    location ~ ^/app\.php(/|\$) {
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)\$;
        include fastcgi_params;
        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT \$realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/app.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php\$ {
      return 404;
    }

    error_log /var/log/nginx/nss-error.log;
    access_log /var/log/nginx/nss-access.log;
}
EOF

sudo ln -s  /etc/nginx/sites-available/nss.local /etc/nginx/sites-enabled/nss.local

sudo service nginx restart
